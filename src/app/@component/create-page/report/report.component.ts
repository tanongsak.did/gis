import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ServiceCenter } from 'src/app/@common/service-center/service-center';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastService } from '../../toast/toast.service';
import { PageService } from 'src/app/page/page.service';
import { PageLayerEnum, PageReportEnum } from '../../../@shares/enum/app-enum';
import { FileTypeEnum } from '../../../@shares/enum/app-enum';
import { IncidentModel, ReportModel } from 'src/app/@shares/model/app-model';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {
  @Input() incident: IncidentModel = new IncidentModel();

  pageReportEnum = PageReportEnum
  page = PageReportEnum.list

  reportList: ReportModel[] = []
  delete = ""


  reportDetailModal: any;
  reportSelected: ReportModel = new ReportModel()
  @ViewChild("reportDetail") reportDetail: any
  reportDeleteModal: any;
  @ViewChild("reportDelete") reportDelete: any
  constructor(
    private serviceCenter: ServiceCenter,
    private ngbModal: NgbModal,
    private toastService: ToastService,
    private pageService: PageService
  ) {

  }

  ngOnInit() {
    this.getReport()


  }

  onCreateClose() {
    this.page = this.pageReportEnum.list
    this.getReport()
  }

  onCreateReport() {
    this.reportSelected = new ReportModel()
    this.page = this.pageReportEnum.create
  }

  getReport() {
    this.serviceCenter.getReportListById(this.incident.id).then((res: any[]) => {
      res.forEach(element => {
        element.reportDateTime = element.reportDateTime + ".00Z"
        this.mapFile(element.allAttachedFiles, element.id).then(data => {
          element.imageDisplay = data.file
          element.videosUrl = data.link
        })

      });

      this.reportList = res.sort((a: ReportModel, b: ReportModel) => {
        return new Date(b.reportDateTime).getTime() - new Date(a.reportDateTime).getTime()
      })

      if (this.reportList.length > 0) {
        let point: any[] = []
        this.reportList.forEach(element => {
          point.push([element.point.lon, element.point.lat])
        });
        this.pageService.extent.next(point)
      }
    }).catch(err => {
    })
  }


  async mapFile(file: any[], reId: any) {
    let fileAll: any = {
      file: [],
      link: ''
    }
    for (const element of file) {
      if (element.fileType != FileTypeEnum[1]) {
        await this.getFile(element.id, reId, element.fileType).then(data => {
          fileAll.file.push({ src: data, type: element.fileType, name: element.fileName })
        })
      } else {
        fileAll.link = element.fileName
      }
    };

    return fileAll
  }

  async getFile(id: any, reId: any, typeFile: any): Promise<any> {
    let img = ''
    await this.serviceCenter.getAttachedFileReport(this.incident.id, reId, id).then(res => {
      if (typeFile == "IMAGE") {
        img = "data:image/png;base64," + res.fileContent
      } else {
        img = res.fileContent
      }
    }).catch(err => {
      console.log(err);
    })
    return Promise.resolve(img);
  }




  onSelectReport(report: any) {

    this.reportSelected = report
    this.reportDetailModal = this.ngbModal.open(this.reportDetail, { centered: true, size: "md" })
    this.pageService.extent.next([[report.point.lon, report.point.lat]])
  }

  cancelDelete() {
    this.reportDeleteModal.close()
  }


  onDelete() {


    this.serviceCenter.deleteReport(this.incident.id, this.reportSelected.id).then(res => {
      this.toastService.success("ลบรายงานเสร็จสิน")
      this.delete = ''
      this.getReport()
      this.cancelDelete()
      this.pageService.resetreportLayer.next(true)
    }).catch(err => {
      console.log(err);
      this.toastService.error("ลบรายงานไม่เสร็จสิน")
    })
  }

  onEditReport(report: any) {
    this.reportSelected = report
    this.page = PageReportEnum.create
    this.pageService.extent.next([[report.point.lon, report.point.lat]])
    if (this.reportDetailModal != undefined) {
      this.reportDetailModal.close()
    }
  }

  onDeleteReport(report: any) {
    this.reportSelected = report
    this.reportDeleteModal = this.ngbModal.open(this.reportDelete, { centered: true, size: "sm" })
  }

  onCloseReporModal() {
    this.reportDetailModal.close()
    this.reportSelected = new ReportModel()
  }


  hover: any;
  hoverReport(report: any) {
    // if (report.id != this.hover) {
    //   this.hover = report.id
    //   this.pageService.highlightSymbolReport.next(report.id)
    // }
  }

  onOpenImage(file: any) {
    let img = new Image();
    img.src = file.src
    let w: any = window.open("");
    w.document.write(img.outerHTML);
  }

  onOpenPDF(file: any) {
    var base64PDF = file.src;
    var binary = atob(base64PDF.replace(/\s/g, ''));
    var len = binary.length;
    var buffer = new ArrayBuffer(len);
    var view = new Uint8Array(buffer);
    for (var i = 0; i < len; i++) {
      view[i] = binary.charCodeAt(i);
    }
    var blob = new Blob([view], { type: 'application/pdf' });
    var link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    link.target = '_blank';
    link.click();
  }

  onOpenLink(link: any) {
    window.open(link)

  }

  onOpenResponsible(item: any) {
    this.reportSelected = item
    this.page = PageReportEnum.responsible
  }


  onResponsibleClose() {
    this.page = PageReportEnum.list
  }

}





