import { Component, Input, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { PageService } from 'src/app/page/page.service';
import { IncidentModel } from '../../../@shares/model/app-model';
import { ServiceCenter } from 'src/app/@common/service-center/service-center';
import { ToastService } from '../../toast/toast.service';
import { PageLayerEnum } from '../../../@shares/enum/app-enum';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-draw-detail',
  templateUrl: './draw-detail.component.html',
  styleUrls: ['./draw-detail.component.scss']
})
export class DrawDetailComponent implements OnInit, OnDestroy {

  @Input() incident: IncidentModel = new IncidentModel()



  description: string = ""
  geometry: any = null
  drawList: any[] = []
  drawSelected: any
  drawDeleteModal: any;
  delete = ""
  @ViewChild("drawDelete") drawDelete: any

  constructor(
    private pageService: PageService,
    private serviceCenter: ServiceCenter,
    private toastService: ToastService,
    private ngbModal: NgbModal
  ) {

    this.pageService.geometry.subscribe(data => {
      this.geometry = data
    })
  }

  ngOnInit() {
    this.getDrawList()
    this.pageService.layerDisplay.next(PageLayerEnum.draw)
  }

  getDrawList() {
    this.serviceCenter.getDrawList(this.incident.id).then(res => {
      this.drawList = res
      let point: any[] = []
      this.drawList.forEach(element => {
        element.coords.forEach((coords: any) => {
          point.push([coords.lon, coords.lat])
        });
      });
      this.pageService.extent.next(point)
    }).catch(err => {
      this.drawList = []
    })
  }


  onCreate() {
    const body = {
      id: "",
      incId: this.incident.id,
      desc: this.description,
      type: this.geometry.type,
      coords: this.geometry.coords
    }

    this.serviceCenter.createDraw(body).then(res => {
      this.toastService.success("บันทึกข้อมูลประกอบสำเร็จ")
      this.description = ''
      this.geometry = null
      this.pageService.removeGraphicsLayer.next(true)
      this.getDrawList()
      this.resetLayer()
    }).catch(err => {
      this.toastService.error("บันทึกข้อมูลประกอบไม่สำเร็จ")
    })

  }

  resetLayer() {
    this.pageService.resetPointLayer.next(true)
    this.pageService.resetLineLayer.next(true)
    this.pageService.resetPolygonLayer.next(true)
  }


  openDelete(draw: any) {
    this.drawSelected = draw
    this.drawDeleteModal = this.ngbModal.open(this.drawDelete, { centered: true, size: "sm" })
  }

  cancelDelete() {
    this.drawDeleteModal.close()
    this.delete = ''
  }

  onDeleteDraw() {
    this.serviceCenter.deleteDraw(this.drawSelected.id).then(res => {
      this.toastService.success("ลบข้อมูลประกอบสำเร็จ")
      this.getDrawList()
      this.resetLayer()
      this.cancelDelete()
    }).catch(err => {
      this.toastService.error("ลบข้อมูลประกอบไม่สำเร็จ")
    })
  }


  ngOnDestroy() {
    this.pageService.layerDisplay.next(PageLayerEnum.incident)
  }

}
