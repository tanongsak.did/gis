import { Component, ViewChild, OnInit, Input, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceCenter } from 'src/app/@common/service-center/service-center';
import { PageService } from 'src/app/page/page.service';
import { ToastService } from '../toast/toast.service';
import { IncidentModel, IncidentTypeModel } from '../../@shares/model/app-model';
import { FileTypeEnum, PageLayerEnum } from '../../@shares/enum/app-enum';
import { Configuration } from 'src/app/app.constant';

@Component({
  selector: 'app-create-page',
  templateUrl: './create-page.component.html',
  styleUrls: ['./create-page.component.scss']
})
export class CreatePageComponent implements OnInit, OnDestroy {



  eventTypeList: IncidentTypeModel[] = []
  time = new Date()
  file: any[] = []
  videoLink = "";
  fileDisplay: any[] = []
  createEventForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    eventType: new FormControl(1, [Validators.required]),
    date: new FormControl(new Date(), [Validators.required]),
    location: new FormControl('', [Validators.required]),
    description: new FormControl(''),
    eventTypeOther: new FormControl(''),
    respOrg: new FormControl(''),
  })
  isDrawDetail: boolean = false
  isResponsible: boolean = false
  responsibleType = ""
  isLoadingGetlocation: boolean = false
  dataEdit: IncidentModel = new IncidentModel();
  isSetData: boolean = false;
  lat: any = null;
  lon: any = null;
  currentTab = 0
  public disabled = false;
  public showSpinners = true;
  public showSeconds = false;
  public touchUi = false;
  public enableMeridian = false;
  public stepHour = 1;
  public stepMinute = 1;
  public stepSecond = 1;
  public color: any = 'primary';

  isLoading: boolean = false
  locationFromMap: any = null;
  location: any = null;

  @ViewChild('fileUpload') fileUpload: any;


  constructor(
    private router: Router,
    private dateAdapter: DateAdapter<Date>,
    private serviceCenter: ServiceCenter,
    private pageService: PageService,
    private toastService: ToastService,
    private activatedRoute: ActivatedRoute,
    private configuration: Configuration

  ) {
    this.dateAdapter.setLocale('th-TH');
    this.getIncidentTypes()
  }



  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((param: any) => {
      this.createEventForm.controls['respOrg'].disable();
      if (param.data != undefined && !this.isSetData) {
        this.dataEdit = this.pageService.decrypt(param.data)
        this.createEventForm.controls['name'].disable();
        this.createEventForm.controls['date'].disable();
        this.pageService.extent.next([[this.dataEdit.point.lon, this.dataEdit.point.lat]])

        this.setForm()
      }
    })

    this.pageService.locationForCreate.subscribe((data: any) => {
      this.lat = null
      this.lon = null
      this.locationFromMap = data
    })
    this.pageService.setSelectPointlocation.next(this.location == null)
    this.pageService.layerDisplay.next(PageLayerEnum.incident)
  }




  setForm() {
    this.createEventForm.setValue({
      name: this.dataEdit.name,
      eventType: parseInt(this.dataEdit.incidentType),
      date: new Date(this.dataEdit.createdDate),
      location: this.dataEdit.location,
      description: this.dataEdit.details,
      eventTypeOther: this.dataEdit.incidentTypeOther,
      respOrg: this.dataEdit.respOrg
    });

    this.time = new Date(this.dataEdit.createdDate)
    this.getAddressByPosition(this.dataEdit.point.lat, this.dataEdit.point.lon)
    this.mapFile(this.dataEdit.allAttachedFiles)
    this.isSetData = true
  }


  mapFile(file: any[]) {
    this.fileDisplay = []
    this.file = []
    file.forEach(element => {
      if (element.fileType == FileTypeEnum[1]) {
        this.videoLink = element.fileName
      } else {
        this.getFile(element.id)
      }
    });
    console.log(this.fileDisplay);

  }



  getIncidentById() {
    this.serviceCenter.getIncidentById(this.dataEdit.id).then(res => {
      this.router.navigate(
        ['/pages/story-map'],
        { queryParams: { p: '2', data: this.pageService.encrypt(res) } }
      );
    }).catch(err => {

    })
  }

  getFile(id: any) {
    this.serviceCenter.getAttachedFile(this.dataEdit.id, id).then(res => {
      this.setFile(res)
    }).catch(err => {
      console.log(err);
    })
  }

  setFile(res: any) {
    if (res.attachedEntity.fileType == FileTypeEnum[2]) {
      this.fileDisplay.push({ name: res.attachedEntity.filename, src: "data:image/png;base64," + res.fileContent, type: "image/jpeg", id: res.attachedEntity.attachId })
      const base64String = "data:image/png;base64," + res.fileContent;
      this.file.push(this.base64ToFile(base64String, res.attachedEntity.filename, "image/jpeg"));
    } else {
      this.fileDisplay.push({ name: res.attachedEntity.filename, src: res.fileContent, type: "application/pdf", id: res.attachedEntity.attachId })
      const base64String = "data:image/png;base64," + res.fileContent;
      this.file.push(this.base64ToFile(base64String, res.attachedEntity.filename, "application/pdf"));
    }
  }




  base64ToFile(base64String: any, filename: any, mimeType: any) {
    const binaryString = atob((base64String).split(',')[1]);
    const binaryLen = binaryString.length;
    const bytes = new Uint8Array(binaryLen);
    for (let i = 0; i < binaryLen; i++) {
      bytes[i] = binaryString.charCodeAt(i);
    }
    const blob = new Blob([bytes], { type: mimeType });
    const file = new File([blob], filename, { type: mimeType });
    return file;
  }





  onLocationFormChange() {
    if (this.lon != null && this.lat != null) {
      this.locationFromMap = { lat: this.lat, lon: this.lon }
    } else {
      this.locationFromMap = null
    }
  }



  getIncidentTypes() {
    this.serviceCenter.getIncidentTypes().then(res => {
      this.eventTypeList = res
    }).catch(err => {
    })
  }



  onBack() {
    this.router.navigate(
      ['/pages/story-map'],
      { queryParams: { p: '1' } }
    );
  }




  onPickFile() {
    this.fileUpload.nativeElement.value = "";
    this.fileUpload.nativeElement.click()
  }



  handleFileBaseChange(element: any) {
    for (let index = 0; index < element.target.files.length; index++) {
      if (!this.validateFileCount(element.target.files[index])) {
        break;
      }
    }
  }



  validateFileCount(file: any) {
    const nameDuplicate = this.file.filter(item => item.name == file.name).length
    const pdfCount = this.file.filter(item => item.type == "application/pdf").length
    const imageCount = this.file.filter(item => item.type.search("image") >= 0).length

    if (nameDuplicate > 0) {
      this.toastService.warning("คุณไม่สามารถเพิ่มไฟล์ซ้ำได้")
      return false
    }


    if (file.type == "application/pdf") {
      if (pdfCount < 3) {
        this.createAttachedFile(file, "DOCUMENT")
      } else {
        this.validateFileError()
        return false
      }
    } else {
      if (imageCount < 1) {

        this.createAttachedFile(file, "IMAGE")
      } else {
        this.validateFileError()
        return false
      }
    }
    return true
  }


  createAttachedFile(file: any, type: any) {
    if (this.dataEdit.id != undefined) {
      const files = new FormData()
      files.append('attachedFiles', file)
      const param = "fileType=" + type
      this.serviceCenter.createAttachedFile(this.dataEdit.id, param, files).then(res => {
        this.file.push(file)
        const reader: any = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.fileDisplay.push({ name: file.name, src: type == "IMAGE" ? reader.result : reader.result.substring(28, reader.result.length), type: file.type, id: res[0].id })
        };
        this.getIncidentById()
        this.toastService.success("เพิ่มข้อมูลไฟล์ประกอบสำเร็จ")
      }).catch(err => {
        this.toastService.error("เพิ่มข้อมูลไฟล์ประกอบไม่สำเร็จ")
        console.log(err);
      })
    } else {
      if (type == "IMAGE") {
        this.file.push(file)
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.fileDisplay.push({ name: file.name, src: reader.result, type: file.type })
        };
      } else {
        this.file.push(file)
        this.fileDisplay.push({ name: file.name, src: '', type: file.type })
      }
    }

  }



  validateFileError() {
    this.toastService.warning('คุณสามารถเลือกไฟล์รูปภาพ 1 ไฟล์ และ PDF 3 ไฟล์')
  }



  onTimeChange(time: Date) {
    this.time = time
  }



  onDeleteImage(image: any) {
    if (this.dataEdit.id != undefined) {
      this.serviceCenter.deleteAttachedFile(this.dataEdit.id, image.id).then(res => {
        this.file = this.file.filter(item => item.name != image.name)
        this.fileDisplay = this.fileDisplay.filter(item => item.name != image.name)
        this.getIncidentById()
        this.toastService.success("ลบข้อมูลไฟล์ประกอบสำเร็จ")
      }).catch(err => {
        this.toastService.error("ลบข้อมูลไฟล์ประกอบwไม่สำเร็จ")
      })
    } else {
      this.file = this.file.filter(item => item.name != image.name)
      this.fileDisplay = this.fileDisplay.filter(item => item.name != image.name)
    }
  }



  onSelectLocation() {
    this.getAddressByPosition(this.locationFromMap.lat, this.locationFromMap.lon)
  }



  onDeleteLocation() {
    this.locationFromMap = null
    this.location = null
    this.pageService.setSelectPointlocation.next(true)
    this.pageService.setDeletePointlocation.next(true)
  }



  onSave() {
    if (this.createEventForm.valid) {
      this.isLoading = true
      let text: any;
      if (this.dataEdit.id == undefined) {
        text = this.createEventForm.value.date?.toISOString()
      } else {
        text = this.dataEdit.incidentDateTime
      }
      let date = new Date(text)
      const dataTime = new Date(date.getFullYear(), date.getMonth(), date.getDate(), this.time.getHours(), this.time.getMinutes()).toISOString().substring(0, 19)
      const param: any =
        (this.dataEdit.id != undefined ? "incId=" + this.dataEdit.id + "&" : '') +
        "incName=" + (this.dataEdit.id == undefined ? this.createEventForm.value.name : this.dataEdit.name) + "&" +
        "incTypeId=" + this.createEventForm.value.eventType + "&" +
        "dateTime=" + dataTime + "&" +
        "videosUrl=" + this.videoLink + "&" +
        "respOrg=" + this.createEventForm.controls.respOrg.value + "&" +
        "lon=" + (this.location != null ? this.location.lon : '0') + "&" +
        "lat=" + (this.location != null ? this.location.lat : '0') + "&" +
        "desc=" + this.createEventForm.value.description + "&" +
        "loc=" + this.createEventForm.value.location + "&" +
        "incTypeOther=" + this.createEventForm.value.eventTypeOther

      const file = new FormData()
      this.file.forEach(element => {
        if (element.type == "application/pdf") {
          file.append('attachedFiles', element)
        } else {
          file.append('attachedImages', element)
        }
      });


      if (this.dataEdit.id == undefined) {
        this.serviceCenter.createIncident(param, file).then(res => {
          this.toastService.success("สร้างเหตุการณ์สำเร็จ")
          this.onBack()
          this.pageService.setSelectPointlocation.next(false)
          this.pageService.setDeletePointlocation.next(true)
          this.pageService.resetIncidentLayer.next(true)
          this.isLoading = false
        }).catch(err => {
          this.toastService.error("สร้างเหตุการณ์ไม่สำเร็จ")
          this.isLoading = false
        })
      } else {
        this.serviceCenter.updateIncident(param, file).then(res => {
          this.toastService.success("อัพเดตเหตุการณ์สำเร็จ")
          this.onBack()
          this.pageService.setSelectPointlocation.next(false)
          this.pageService.setDeletePointlocation.next(true)
          this.pageService.resetIncidentLayer.next(true)
          this.isLoading = false
        }).catch(err => {
          this.toastService.error("อัพเดตเหตุการณ์ไม่สำเร็จ")
          this.isLoading = false
        })
      }

    }
  }




  getAddressByPosition(lat: any, lon: any) {
    this.isLoadingGetlocation = true
    const param = "latitude=" + lat + "&longitude=" + lon + "&buffer_distance_meters=1"
    this.serviceCenter.getAdministrativeBoundaryByPosition(param).then(res => {
      if (this.locationFromMap != null) {
        this.location = this.locationFromMap
      } else {
        this.location = {
          lat: this.dataEdit.point.lat,
          lon: this.dataEdit.point.lon
        }
      }

      this.location['address'] = "ตำบล " + res[0].tambonth + " อำเภอ " + res[0].amphoeth + " จังหวัด " + res[0].provinceth
      if (this.dataEdit.id == undefined) {
        this.createEventForm.controls.location.setValue(this.location.address)
      }
      this.createEventForm.controls.respOrg.setValue(res[0].unitsth)
      this.isLoadingGetlocation = false
      this.pageService.setSelectPointlocation.next(false)
    }).catch(err => {
      this.isLoadingGetlocation = false
    })
  }







  drawDetail() {
    this.isDrawDetail = true
    this.pageService.isDrawDetail.next(true)
  }



  closeDrawDetail() {
    this.isDrawDetail = false
    this.pageService.isDrawDetail.next(false)
  }



  ngOnDestroy() {
    this.pageService.setSelectPointlocation.next(false)
    this.pageService.setDeletePointlocation.next(true)
  }



  onTabChange(event: any) {
    this.currentTab = event.index
    if (event.index == 1) {
      this.pageService.layerDisplay.next(PageLayerEnum.report)
    } else {
      this.pageService.setSelectPointlocation.next(false)
      this.pageService.setDeletePointlocation.next(true)
      this.pageService.layerDisplay.next(PageLayerEnum.incident)
      this.pageService.extent.next([[this.dataEdit.point.lon, this.dataEdit.point.lat]])
    }
  }


  onOpenResponsible(type: string) {
    this.responsibleType = type
    this.isResponsible = true
  }

  onResponsibleClose() {
    this.responsibleType = ""
    this.isResponsible = false
  }

  onOpenImage(file: any) {
    let img = new Image();
    img.src = file.src
    let w: any = window.open("");
    w.document.write(img.outerHTML);
  }

  onOpenPDF(file: any) {
    var base64PDF = file.src;
    var binary = atob(base64PDF.replace(/\s/g, ''));
    var len = binary.length;
    var buffer = new ArrayBuffer(len);
    var view = new Uint8Array(buffer);
    for (var i = 0; i < len; i++) {
      view[i] = binary.charCodeAt(i);
    }
    var blob = new Blob([view], { type: 'application/pdf' });
    var link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    link.target = '_blank';
    link.click();
  }

}





