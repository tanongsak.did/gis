import { Component, ViewChild, Output, EventEmitter, Input, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastService } from '../toast/toast.service';
import { PageService } from 'src/app/page/page.service';
import { ServiceCenter } from 'src/app/@common/service-center/service-center';
import { IncidentModel, ReportModel } from '../../@shares/model/app-model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CurrentPageReport, FileTypeEnum } from '../../@shares/enum/app-enum';

@Component({
  selector: 'app-create-report',
  templateUrl: './create-report.component.html',
  styleUrls: ['./create-report.component.scss']
})
export class CreateReportComponent implements OnInit, OnDestroy {

  @Input() incident: IncidentModel = new IncidentModel();
  @Input() reportSelected: ReportModel = new ReportModel();


  /////Resource/////

  videoLink = ''
  currentPage = CurrentPageReport.createReport
  currentPageReport = CurrentPageReport
  isEdit: boolean = false
  location: any = null;
  locationFromMap: any = null;
  lat: any = null;
  lon: any = null;
  time = new Date()
  createReportForm = new FormGroup({
    reportType: new FormControl(false, [Validators.required]),
    status: new FormControl(false, [Validators.required]),
    name: new FormControl('', [Validators.required]),
    date: new FormControl(new Date(), [Validators.required]),
    description: new FormControl('', []),
    disZone: new FormControl('', []),
    relfZone: new FormControl('', []),
  })

  reportTypeList = [
    { id: false, type: "สถานการณ์ (Situation)" },
    { id: true, type: "การปฏิบัติงาน (Action)" }
  ]

  isLoadingGetlocation: boolean = false
  statusList = [
    { id: false, type: "อยู่ระหว่างดําเนินการ" },
    { id: true, type: "ดําเนินการเสร็จสิ้น" }

  ]


  //////file////
  file: any[] = []
  fileDisplay: any[] = []

  @ViewChild('fileUpload') fileUpload: any;



  @Output() eventClose: EventEmitter<any> = new EventEmitter<any>()


  constructor(
    private toastService: ToastService,
    private pageService: PageService,
    private serviceCenter: ServiceCenter,
    private ngbModal: NgbModal,

  ) {
    this.pageService.locationForCreate.subscribe(data => {
      if (!this.isEdit) {
        this.lat = null
        this.lon = null
        this.locationFromMap = data
      }

    })
  }

  ngOnInit() {
    if (this.reportSelected.id != undefined) {
      this.isEdit = true
      this.setForm()
      this.createReportForm.controls['name'].disable();
      this.createReportForm.controls['reportType'].disable();
      this.createReportForm.controls['date'].disable();


    } else {
      this.getAddressByPosition(this.incident.point.lat, this.incident.point.lon)
    }


    this.pageService.setSelectPointlocation.next(false)

  }



  setForm() {
    this.createReportForm.setValue({
      reportType: this.reportSelected.action,
      status: this.reportSelected.statusDone,
      date: new Date(this.reportSelected.reportDateTime),
      name: this.reportSelected.topic,
      description: this.reportSelected.details,
      disZone: this.reportSelected.disZoneDeclare,
      relfZone: this.reportSelected.reliefZoneDeclare,

    });
    this.videoLink = this.reportSelected.videosUrl
    this.time = new Date(this.reportSelected.reportDateTime)
    this.getAddressByPosition(this.reportSelected.point.lat, this.reportSelected.point.lon)
    this.mapFile(this.reportSelected.allAttachedFiles)

    // this.pageService.createPonitIncident.next([this.dataEdit])
  }

  mapFile(file: any[]) {
    this.fileDisplay = []
    this.file = []
    file.forEach(element => {
      if (element.fileType == FileTypeEnum[1]) {
        this.videoLink = element.fileName
      } else {
        this.getFile(element.id)
      }
    });
  }

  getFile(id: any) {
    this.serviceCenter.getAttachedFileReport(this.incident.id, this.reportSelected.id, id).then(res => {
      this.setFile(res)
    }).catch(err => {
      console.log(err);
    })
  }


  setFile(res: any) {
    if (res.attachedEntity.fileType == FileTypeEnum[2]) {
      this.fileDisplay.push({ name: res.attachedEntity.filename, src: "data:image/png;base64," + res.fileContent, type: "image/jpeg", id: res.attachedEntity.attachId })
      const base64String = "data:image/png;base64," + res.fileContent;
      this.file.push(this.base64ToFile(base64String, res.attachedEntity.filename, "image/jpeg"));
    } else {
      this.fileDisplay.push({ name: res.attachedEntity.filename, src: "data:image/png;base64," + res.fileContent, type: "application/pdf", id: res.attachedEntity.attachId })
      const base64String = "data:image/png;base64," + res.fileContent;
      this.file.push(this.base64ToFile(base64String, res.attachedEntity.filename, "application/pdf"));
    }
  }

  base64ToFile(base64String: any, filename: any, mimeType: any) {
    const binaryString = atob((base64String).split(',')[1]);
    const binaryLen = binaryString.length;
    const bytes = new Uint8Array(binaryLen);
    for (let i = 0; i < binaryLen; i++) {
      bytes[i] = binaryString.charCodeAt(i);
    }
    const blob = new Blob([bytes], { type: mimeType });
    const file = new File([blob], filename, { type: mimeType });
    return file;
  }



  onLocationFormChange() {
    if (this.lon != null && this.lat != null) {
      this.locationFromMap = { lat: this.lat, lon: this.lon }
    } else {
      this.locationFromMap = null
    }
  }





  setResource() {

  }



  onPickFile() {
    this.fileUpload.nativeElement.value = "";
    this.fileUpload.nativeElement.click()
  }




  handleFileBaseChange(element: any) {
    for (let index = 0; index < element.target.files.length; index++) {
      if (!this.validateFileCount(element.target.files[index])) {
        break;
      }
    }
  }



  validateFileCount(file: any) {
    const nameDuplicate = this.file.filter(item => item.name == file.name).length
    const pdfCount = this.file.filter(item => item.type == "application/pdf").length
    const imageCount = this.file.filter(item => item.type.search("image") >= 0).length

    if (nameDuplicate > 0) {
      this.toastService.warning("คุณไม่สามารถเพิ่มไฟล์ซ้ำได้")
      return false
    }
    if (file.type == "application/pdf") {
      if (pdfCount < 3) {
        // this.file.push(file)
        // this.fileDisplay.push({ name: file.name, src: '', type: file.type })
        this.createAttachedFile(file, "Document")
      } else {
        this.validateFileError()
        return false
      }
    } else {
      if (imageCount < 2) {
        // this.file.push(file)
        this.createAttachedFile(file, "IMAGE")
      } else {
        this.validateFileError()
        return false
      }
    }
    return true
  }



  createAttachedFile(file: any, type: any) {
    if (this.isEdit) {
      const param = 'fileType=' + type
      const files = new FormData()
      files.append('attachedFiles', file)
      this.serviceCenter.createAttachedFileReport(this.incident.id, this.reportSelected.id, param, files).then(res => {
        if (type == "IMAGE") {
          this.file.push(file)
          const reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = () => {
            this.fileDisplay.push({ name: file.name, src: reader.result, type: file.type, id: res[0].id })
          };
        } else {
          this.file.push(file)
          this.fileDisplay.push({ name: file.name, src: '', type: file.type, id: res[0].id })
        }
        this.toastService.success("เพิ่มข้อมูลไฟล์ประกอบสำเร็จ")
      }).catch(err => {
        this.toastService.error("เพิ่มข้อมูลไฟล์ประกอบไม่สำเร็จ")
        console.log(err);
      })
    } else {
      if (type == "IMAGE") {
        this.file.push(file)
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.fileDisplay.push({ name: file.name, src: reader.result, type: file.type })
        };
      } else {
        this.file.push(file)
        this.fileDisplay.push({ name: file.name, src: '', type: file.type })
      }
    }
  }



  onDeleteImage(image: any) {
    if (this.isEdit) {
      this.serviceCenter.deleteAttachedFileReport(this.incident.id, this.reportSelected.id, image.id).then(res => {
        this.file = this.file.filter(item => item.name != image.name)
        this.fileDisplay = this.fileDisplay.filter(item => item.name != image.name)
        this.toastService.success("ลบข้อมูลไฟล์ประกอบสำเร็จ")
      }).catch(err => {
        this.toastService.error("ลบข้อมูลไฟล์ประกอบไม่สำเร็จ")
      })
    } else {
      this.file = this.file.filter(item => item.name != image.name)
      this.fileDisplay = this.fileDisplay.filter(item => item.name != image.name)
    }
  }



  validateFileError() {
    this.toastService.warning('คุณสามารถเลือกไฟล์รูปภาพ 2 ไฟล์')
  }



  onTimeChange(time: Date) {
    this.time = time
  }



  // drawDetail() {

  // }



  resourceUse() {
    this.currentPage = CurrentPageReport.resourceList
  }



  onSave() {
    if (this.createReportForm.valid) {
      let text: any;
      text = this.createReportForm.controls.date.value?.toISOString()
      let date = new Date(text)
      const dataTime = new Date(date.getFullYear(), date.getMonth(), date.getDate(), this.time.getHours(), this.time.getMinutes()).toISOString().substring(0, 19)


      const file = new FormData()

      this.file.forEach(element => {
        if (element.type == "application/pdf") {
          file.append('attachedFiles', element)
        } else {
          file.append('attachedImages', element)
        }
      });
      // file.append('attachedFiles', '')
      // file.append('attachedImages', '')

      if (!this.isEdit) {
        const param: any =
          "incId=" + this.incident.id + "&" +
          "isAction=" + this.createReportForm.controls.reportType.value + "&" +
          "dateTime=" + dataTime + "&" +
          "topic=" + this.createReportForm.controls.name.value + "&" +
          "videosUrl=" + this.videoLink + "&" +
          "lon=" + (this.location != null ? this.location.lon : '0') + "&" +
          "lat=" + (this.location != null ? this.location.lat : '0') + "&" +
          "desc=" + this.createReportForm.controls.description.value + "&" +
          "isDone=" + this.createReportForm.controls.status.value + "&" +
          // ("&"+ (this.createReportForm.controls.reportType.value ?))
          "disZoneDeclare=" + this.createReportForm.controls.disZone.value + "&" +
          "reliefZoneDeclare=" + this.createReportForm.controls.relfZone.value

        this.serviceCenter.createReport(param, file).then(res => {
          this.toastService.success("บันทึกรายงานสำเร็จ")
          this.eventClose.emit(true)
          this.pageService.setSelectPointlocation.next(false)
          this.pageService.setDeletePointlocation.next(true)
          this.pageService.resetreportLayer.next(true)
        }).catch(err => {
          this.toastService.error("บันทึกรายงานไม่สำเร็จ")
        })

      } else {
        const param: any =
          "lon=" + (this.location != null ? this.location.lon : '0') + "&" +
          "lat=" + (this.location != null ? this.location.lat : '0') + "&" +
          "desc=" + this.createReportForm.controls.description.value + "&" +
          "isDone=" + this.createReportForm.controls.status.value + "&" +
          "videosUrl=" + this.videoLink + "&" +
          "disZoneDeclare=" + this.createReportForm.controls.disZone.value + "&" +
          "reliefZoneDeclare=" + this.createReportForm.controls.relfZone.value


        this.serviceCenter.updateReport(this.incident.id, this.reportSelected.id, param, file).then(res => {
          this.toastService.success("บันทึกรายงานสำเร็จ")
          this.eventClose.emit(true)
          this.pageService.setSelectPointlocation.next(false)
          this.pageService.setDeletePointlocation.next(true)
          this.pageService.resetreportLayer.next(true)
        }).catch(err => {
          this.toastService.error("บันทึกรายงานไม่สำเร็จ")
        })

      }


    }

  }



  onDeleteLocation() {
    this.locationFromMap = null
    this.location = null
    this.pageService.setSelectPointlocation.next(true)
    this.pageService.setDeletePointlocation.next(true)
  }



  onSelectLocation() {
    this.getAddressByPosition(this.locationFromMap.lat, this.locationFromMap.lon)
  }



  getAddressByPosition(lat: any, lon: any) {
    this.isLoadingGetlocation = true
    const param = "latitude=" + lat + "&longitude=" + lon + "&buffer_distance_meters=1"
    this.serviceCenter.getAdministrativeBoundaryByPosition(param).then(res => {
      if (this.locationFromMap != null) {
        this.location = this.locationFromMap
      } else {
        this.location = {
          lat: lat,
          lon: lon
        }
      }

      this.location['address'] = "ตำบล " + res[0].tambonth + " อำเภอ " + res[0].amphoeth + " จังหวัด " + res[0].provinceth
      this.isLoadingGetlocation = false
      this.pageService.setSelectPointlocation.next(false)
    }).catch(err => {
      this.isLoadingGetlocation = false
    })
  }



  onClose() {
    this.eventClose.emit(true)
  }





  onCloseResource() {
    this.currentPage = CurrentPageReport.createReport
  }






  ngOnDestroy() {
    this.pageService.setSelectPointlocation.next(false)
    this.pageService.setDeletePointlocation.next(true)
  }


}


