import { Component, Input, OnInit, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
  selector: 'app-time-picker',
  templateUrl: './time-picker.component.html',
  styleUrls: ['./time-picker.component.scss']
})
export class TimePickerComponent implements OnInit, OnChanges {

  @Input() time = new Date()
  @Input() disable = false

  isOpen: boolean = false
  timeDisplay = ""

  hr = 0
  min = 0


  @Output() eventTime: EventEmitter<any> = new EventEmitter<any>();

  constructor() {

  }


  ngOnInit() {
  }

  ngOnChanges() {
    this.hr = this.time.getHours()
    this.min = this.time.getMinutes()
    this.timeDisplay = (this.hr < 10 ? '0' + this.hr : this.hr) + ':' + (this.min < 10 ? '0' + this.min : this.min)
  }



  onOpen() {
    if (!this.disable) {
      this.isOpen = !this.isOpen
    }

  }


  onchengeTimr(time: any, type: any) {
    if (time == 'h') {
      if (type == "u") {
        if (this.hr < 23) {
          this.hr += 1
        }
        else {
          this.hr = 0
        }
      } else {
        if (this.hr > 0) {
          this.hr -= 1
        }
        else {
          this.hr = 23
        }
      }
    } else {
      if (type == "u") {
        if (this.min < 59) {
          this.min += 1
        } else {
          this.min = 0
        }

      } else {
        if (this.min > 0) {
          this.min -= 1
        } else {
          this.min = 59
        }
      }
    }
  }

  onConfirm() {
    this.timeDisplay = (this.hr < 10 ? '0' + this.hr : this.hr) + ':' + (this.min < 10 ? '0' + this.min : this.min)
    this.isOpen = false
    const date = new Date()
    date.setHours(this.hr)
    date.setMinutes(this.min)
    this.eventTime.emit(date)
  }
}
