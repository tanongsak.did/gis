import { Component, Input, OnInit } from '@angular/core';
import { ServiceCenter } from 'src/app/@common/service-center/service-center';
import { ToastService } from '../toast/toast.service';

@Component({
  selector: 'app-damage',
  templateUrl: './damage.component.html',
  styleUrls: ['./damage.component.scss']
})
export class DamageComponent implements OnInit {
  @Input() incident: any

  statusList = [
    // { id: 1, text: "ประจำวัน" },
    { id: 2, text: "ฉบับสมบูรณ์ (สะสม)" },
  ]

  overAllForm = {
    date: new Date(),
    time: new Date(),
    status: 1,
    provinceTotal: null,
    districtTotal: null,
    subDistrictTotal: null,
    villageTotal: null,
    damagedValMbaht: null
  }

  isEditoverAll: boolean = false

  humanForm = {
    date: new Date(),
    time: new Date(),
    status: 1,
    emigrant: null,
    injured: null,
    dead: null,
    missing: null,
    hhAffected: null,
    pplImpacted: null
  }

  isEditHuman: boolean = false

  propForm = {
    date: new Date(),
    time: new Date(),
    status: 1,
    houseWhole: null,
    housePartly: null
  }

  isEditProp: boolean = false

  publicForm = {
    date: new Date(),
    time: new Date(),
    status: 1,
    roadLine: null,
    roadKm: null,
    roadPoint: null,
    bridge: null,
    drainPipe: null,
    weir: null,
    barrage: null,
    gov: null,
    school: null,
    worship: null
  }

  isEditPublic: boolean = false

  agriForm = {
    date: new Date(),
    time: new Date(),
    status: 1,
    farmer: null,
    farmRaiItem: {
      rai: 0,
      ngan: 0,
      squareWa: 0,
    },
    farmRai: 0,
    ricePaddyRaiItem: {
      rai: 0,
      ngan: 0,
      squareWa: 0,
    },
    ricePaddyRai: null,
    gardensRaiItem: {
      rai: 0,
      ngan: 0,
      squareWa: 0,
    },
    gardensRai: null,
    cowBuff: null,
    pigGoatSheep: null,
    poultry: null,
    aquaAreaRai: null
  }

  isEditAgri: boolean = false

  constructor(
    private serviceCenter: ServiceCenter,
    private toastService: ToastService,
  ) {
  }



  ngOnInit() {
    this.getDamageByIncidentId()
  }



  getDamageByIncidentId() {
    this.serviceCenter.getDamageByIncidentId(this.incident.id).then(res => {
      this.setDamage(res)
    }).catch(err => {
    })
  }



  setDamage(damage: any) {
    this.overAllForm = Object.assign({
      date: this.checkDate(damage.incDamagedSumDto.damagedUpdateDateTime),
      time: this.checkDate(damage.incDamagedSumDto.damagedUpdateDateTime),
      status: damage.incDamagedSumDto.accumulated == true ? 2 : 2,
      provinceTotal: damage.incDamagedSumDto.provinceTotal,
    }, damage.incDamagedSumDto)

    this.humanForm = Object.assign({
      date: this.checkDate(damage.incDamagedHumanDto.damagedUpdateDateTime),
      time: this.checkDate(damage.incDamagedHumanDto.damagedUpdateDateTime),
      status: damage.incDamagedHumanDto.accumulated == true ? 2 : 2,
    }, damage.incDamagedHumanDto)

    this.propForm = Object.assign({

      date: this.checkDate(damage.incDamagedPropDto.damagedUpdateDateTime),
      time: this.checkDate(damage.incDamagedPropDto.damagedUpdateDateTime),
      status: damage.incDamagedPropDto.accumulated == true ? 2 : 2,
    }, damage.incDamagedPropDto)

    this.publicForm = Object.assign({

      date: this.checkDate(damage.incDamagedPublicDto.damagedUpdateDateTime),
      time: this.checkDate(damage.incDamagedPublicDto.damagedUpdateDateTime),
      status: damage.incDamagedPublicDto.accumulated == true ? 2 : 2,
    }, damage.incDamagedPublicDto)

    this.agriForm = Object.assign({
      farmRaiItem: {
        rai: this.conversionArea(damage.incDamagedAgriDto.farmRai).rai,
        ngan: this.conversionArea(damage.incDamagedAgriDto.farmRai).ngan,
        squareWa: this.conversionArea(damage.incDamagedAgriDto.farmRai).squareWa,
      },
      ricePaddyRaiItem: {
        rai: this.conversionArea(damage.incDamagedAgriDto.ricePaddyRai).rai,
        ngan: this.conversionArea(damage.incDamagedAgriDto.ricePaddyRai).ngan,
        squareWa: this.conversionArea(damage.incDamagedAgriDto.ricePaddyRai).squareWa,
      },
      gardensRaiItem: {
        rai: this.conversionArea(damage.incDamagedAgriDto.gardensRai).rai,
        ngan: this.conversionArea(damage.incDamagedAgriDto.gardensRai).ngan,
        squareWa: this.conversionArea(damage.incDamagedAgriDto.gardensRai).squareWa,
      },

      date: this.checkDate(damage.incDamagedAgriDto.damagedUpdateDateTime),
      time: this.checkDate(damage.incDamagedAgriDto.damagedUpdateDateTime),
      status: damage.incDamagedAgriDto.accumulated == true ? 2 : 2,
    }, damage.incDamagedAgriDto)

  }


  checkDate(date: any) {
    if (date.length > 20) {
      return new Date(date.substring(0, 19) + ".00Z")
    } else {
      return new Date(date + ".00Z")
    }
  }



  conversionArea(area: any): { rai: number, ngan: number, squareWa: number } {
    const rai = Math.floor(area / 1)
    const ngan = Math.floor((area % 1) / 0.25)
    const squareWa = Math.round(((area % 1) % 0.25) * 400)
    return { rai: rai, ngan: ngan, squareWa: squareWa }
  }



  conversionAreaToRai(area: { rai: number, ngan: number, squareWa: number }) {
    return area.rai + (area.ngan * 0.25) + (area.squareWa / 400);
  }


  onTimeChange(time: any, type: number) {
    switch (type) {
      case 1:
        this.overAllForm.time = time
        break;
      case 2:
        this.humanForm.time = time
        break;
      case 3:
        this.propForm.time = time
        break;
      case 4:
        this.publicForm.time = time
        break;
      case 5:
        this.agriForm.time = time
        break;
      default:
        break;
    }
  }



  onUpdtaeDamageOverAll() {
    if (!this.isEditoverAll) {
      this.isEditoverAll = true;
    } else {
      const body = {
        id: "",
        incId: this.incident.id,
        additional: "string",
        accumulated: this.overAllForm.status == 2,
        damagedUpdateDateTime: this.setFormatDamagedUpdateDateTime(this.overAllForm.date, this.overAllForm.time),
        provinceTotal: this.overAllForm.provinceTotal,
        districtTotal: this.overAllForm.districtTotal,
        subDistrictTotal: this.overAllForm.subDistrictTotal,
        villageTotal: this.overAllForm.villageTotal,
        damagedValMbaht: this.overAllForm.damagedValMbaht
      }
      this.serviceCenter.updateDamageSummary(body).then(res => {
        this.toastService.success('บันทึกความเสียหายภาพรวมสำเร็จ')
        this.isEditoverAll = false
      }).catch(err => {
        this.toastService.error('บันทึกความเสียหายภาพรวมไม่สำเร็จ')

      })
    }
  }


  onUpdateDamageHuman() {
    if (!this.isEditHuman) {
      this.isEditHuman = true
    } else {
      const body = {
        id: "",
        incId: this.incident.id,
        additional: "string",
        accumulated: this.humanForm.status == 2,
        damagedUpdateDateTime: this.setFormatDamagedUpdateDateTime(this.humanForm.date, this.humanForm.time),

        emigrant: this.humanForm.emigrant,
        injured: this.humanForm.injured,
        dead: this.humanForm.dead,
        missing: this.humanForm.missing,
        hhAffected: this.humanForm.hhAffected,
        pplImpacted: this.humanForm.pplImpacted
      }
      this.serviceCenter.updateDamageHuman(body).then(res => {
        this.toastService.success('บันทึกความเสียหายด้านชีวิตสำเร็จ')
        this.isEditHuman = false
      }).catch(err => {
        this.toastService.error('บันทึกความเสียหายด้านชีวิตไม่สำเร็จ')

      })
    }
  }



  onUpdateDamageProp() {
    if (!this.isEditProp) {
      this.isEditProp = true
    } else {
      const body = {
        id: "",
        incId: this.incident.id,
        additional: "string",
        accumulated: this.propForm.status == 2,
        damagedUpdateDateTime: this.setFormatDamagedUpdateDateTime(this.propForm.date, this.propForm.time),
        houseWhole: this.propForm.houseWhole,
        housePartly: this.propForm.housePartly
      }
      this.serviceCenter.updateDamageProp(body).then(res => {
        this.toastService.success('บันทึกความเสียหายด้านทรัพย์สิน/ที่อยู่อาศัยสำเร็จ')
        this.isEditProp = false
      }).catch(err => {
        this.toastService.error('บันทึกความเสียหาด้านทรัพย์สิน/ที่อยู่อาศัยไม่สำเร็จ')

      })
    }
  }



  onUpdateDamagePublic() {
    if (!this.isEditPublic) {
      this.isEditPublic = true
    } else {
      const body = {
        id: "",
        incId: this.incident.id,
        additional: "string",
        accumulated: this.publicForm.status == 2,
        damagedUpdateDateTime: this.setFormatDamagedUpdateDateTime(this.publicForm.date, this.publicForm.time),
        roadLine: this.publicForm.roadLine,
        roadKm: this.publicForm.roadKm,
        roadPoint: this.publicForm.roadPoint,
        bridge: this.publicForm.bridge,
        drainPipe: this.publicForm.drainPipe,
        weir: this.publicForm.weir,
        barrage: this.publicForm.barrage,
        gov: this.publicForm.gov,
        school: this.publicForm.school,
        worship: this.publicForm.worship
      }
      this.serviceCenter.updateDamagePublic(body).then(res => {
        this.toastService.success('บันทึกความเสียหายด้านสิ่งสาธารณประโยชน์สำเร็จ')
        this.isEditPublic = false
      }).catch(err => {
        this.toastService.error('บันทึกความเสียหายด้านสิ่งสาธารณประโยชน์ไม่สำเร็จ')

      })
    }
  }



  onUpdateDamageAgri() {
    if (!this.isEditAgri) {
      this.isEditAgri = true
    } else {
      const body = {
        id: "",
        incId: this.incident.id,
        additional: "string",
        accumulated: this.agriForm.status == 2,
        damagedUpdateDateTime: this.setFormatDamagedUpdateDateTime(this.agriForm.date, this.agriForm.time),
        farmer: this.agriForm.farmer,
        farmRai: this.conversionAreaToRai(this.agriForm.farmRaiItem),
        ricePaddyRai: this.conversionAreaToRai(this.agriForm.ricePaddyRaiItem),
        gardensRai: this.conversionAreaToRai(this.agriForm.gardensRaiItem),
        cowBuff: this.agriForm.cowBuff,
        pigGoatSheep: this.agriForm.pigGoatSheep,
        poultry: this.agriForm.poultry,
        aquaAreaRai: this.agriForm.aquaAreaRai
      }
      this.serviceCenter.updateDamageAgri(body).then(res => {
        this.toastService.success('บันทึกความเสียหายด้านการเกษตรสำเร็จ')
        this.isEditAgri = false
      }).catch(err => {
        this.toastService.error('บันทึกความเสียหายด้านการเกษตรไม่สำเร็จ')

      })
    }
  }



  setFormatDamagedUpdateDateTime(date: Date, time: Date) {
    date = new Date(date?.toISOString())
    const dataTime = new Date(date.getFullYear(), date.getMonth(), date.getDate(), time.getHours(), time.getMinutes()).toISOString()
    return dataTime
  }



  onTabChange() {
    this.isEditoverAll = false
    this.isEditHuman = false
    this.isEditProp = false
    this.isEditPublic = false
    this.isEditAgri = false
    this.getDamageByIncidentId()
  }

}
