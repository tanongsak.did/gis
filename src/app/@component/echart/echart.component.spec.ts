// import { async, ComponentFixture, TestBed } from '@angular/core/testing';

// import { EchartComponent } from './echart.component';
// // import { GradingComponent } from 'src/app/pages/instructor/course/grading/grading.component';
// // import { ThemeTestModule } from '../../theme.test.module';

// xxdescribe('EchartComponent', () => {
//   let component: EchartComponent;
//   let fixture: ComponentFixture<EchartComponent>;

//   const option = {
//     title: {
//       text: 'Rating of Security Assessmen',
//       subtext: '',
//       x: 'center',
//       textStyle: { fontSize: '15' },
//       show: false
//     },
//     tooltip: {
//       trigger: 'item',
//       formatter: '{a} <br/>{b}: {c} ({d}%)'
//     },
//     legend: {
//       // orient: 'vertical',
//       bottom: 0,
//       left: 'center',
//       data: ['Passed', 'Failed', 'In Progress'],
//       itemGap: 30,
//       textStyle: {
//         fontWeight: 'bold',
//       }
//     },
//     series: [
//       {
//         name: 'Student Result',
//         type: 'pie',
//         radius: ['50%', '70%'],
//         avoidLabelOverlap: false,
//         emphasis: {
//           show: true,
//           formatter: '{b}: {d}'
//         },
//         label: {
//           normal: {
//             show: false,
//             position: 'outside'
//           },
//           emphasis: {
//             show: true,
//             textStyle: {
//               fontSize: '20',
//               fontWeight: 'normal'
//             }
//           }
//         },
//         labelLine: {
//           normal: {
//             show: false
//           }
//         },
//         data: [
//           {
//             value: 300, name: 'Passed', itemStyle: {
//               color: {
//                 type: 'solid',
//                 colorStops: [{
//                   offset: 0, color: '#009688'
//                 }],
//                 global: false
//               }
//             }
//           },
//           {
//             value: 100, name: 'Failed', itemStyle: {
//               color: {
//                 type: 'solid',
//                 colorStops: [{
//                   offset: 0, color: '#A82426'
//                 }],
//                 global: false
//               }
//             }
//           },
//           {
//             value: 50, name: 'In Progress', itemStyle: {
//               color: {
//                 type: 'solid',
//                 colorStops: [{
//                   offset: 0, color: '#FFC400'
//                 }],
//                 global: false
//               }
//             }
//           },
//         ]
//       }
//     ]
//   };

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       imports: [
//         // ...ThemeTestModule.prototype.getDefultModule()
//       ],
//       declarations: [EchartComponent]
//     })
//       .compileComponents();
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(EchartComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });

//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });

//   it('start component', () => {
//     component.chartLoadedEvent.subscribe(data => {
//       expect(data).toBe(true);
//       expect(component.isChartLoaded).toBe(true);
//     });
//     component.option = option;
//     spyOn(component.chart, 'nativeElement');
//     component.ngAfterViewInit();
//   });


//   it('window resize', () => {
//     component.chartLoadedEvent.subscribe(data => {
//       expect(data).toBe(true);
//       expect(component.isChartLoaded).toBe(true);
//       expect(component.isScreenChange).toBe(false);
//     });
//     component.option = option;
//     spyOn(component.chart, 'nativeElement');
//     component.onResizeListener(true);
//   });

//   it('change component', () => {
//     component.chartLoadedEvent.subscribe(data => {
//       expect(data).toBe(true);
//       expect(component.isChartLoaded).toBe(true);
//     });
//     component.option = option;
//     spyOn(component.chart, 'nativeElement');
//     component.ngOnChanges();
//   });


// });
