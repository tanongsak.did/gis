import { Component, OnInit, HostListener, ViewChild, ElementRef, Input, OnChanges, AfterViewInit, DoCheck, Output, EventEmitter } from '@angular/core';
import * as echarts from 'echarts';
import * as $ from 'jquery';
@Component({
  selector: 'app-echart',
  templateUrl: './echart.component.html',
  styleUrls: ['./echart.component.scss']
})
export class EchartComponent implements OnInit, OnChanges, AfterViewInit {


  @Input() option: any;

  isScreenChange: boolean = false;
  myChart: any;
  width: number = 0;
  height: number = 0;

  @Output() chartLoadedEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() chartEvent: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('chart') chart: any;


  constructor(
  ) {
  }



  @HostListener('window:resize', ['$event'])
  onResizeListener(event: any) {
    this.onSetChart();
    this.isScreenChange = true;
    setTimeout(() => {
      this.isScreenChange = false;
    }, 200);
  }



  ngOnInit() {
  }



  ngAfterViewInit() {
    this.onSetChart();
  }



  ngOnChanges() {
    this.onSetChart();

  }



  onSetChart() {
    // $.getJSON('/assets/dark.json', (themeJSON: any) => {
    //   echarts.registerTheme('dark', JSON.parse(JSON.stringify(themeJSON)));
    // });



    setTimeout(() => {
      this.myChart = echarts.init(this.chart.nativeElement);
      this.myChart.setOption(this.option);
      this.chartLoadedEvent.emit(true);
      this.chartEvent.emit(this.myChart)
    }, 1000);
  }

}

