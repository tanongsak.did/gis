import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { PageService } from 'src/app/page/page.service';
import { IncidentModel, ReportModel, resourceModel } from '../../@shares/model/app-model';
import { ServiceCenter } from 'src/app/@common/service-center/service-center';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastService } from '../toast/toast.service';

@Component({
  selector: 'app-resource',
  templateUrl: './resource.component.html',
  styleUrls: ['./resource.component.scss']
})
export class ResourceComponent implements OnInit {
  @Input() incident: IncidentModel = new IncidentModel();
  @Input() reportSelected: ReportModel = new ReportModel();

  resourceForm = {
    resType: "(001)กลุ่มรถอำนวยการ",
    resName: "รถจี๊ป",
    amount: 0,
    description: "",
  }

  resourceUseData: any;

  carTypeList: any[] = []
  carList: any[] = []
  resourceList: resourceModel[] = []

  resourceModal: any


  @Output() eventCloseResource: EventEmitter<any> = new EventEmitter<any>()
  @ViewChild('resource') resource: any
  constructor(
    private pageService: PageService,
    private serviceCenter: ServiceCenter,
    private ngbModal: NgbModal,
    private toastService: ToastService
  ) {

  }

  ngOnInit() {
    this.carTypeList = this.pageService.getCarType()
    this.carList = this.pageService.getCarByType("001")
    this.getResource()
  }

  getResource() {
    this.serviceCenter.getResourceById(this.reportSelected.id).then(res => {
      this.resourceList = res
    }).catch(err => {
    })
  }

  onOpenCreateResource() {
    this.setDefaultResourceForm()
    this.resourceModal = this.ngbModal.open(this.resource, { centered: true })
  }



  onEditResource(resource: resourceModel) {
    this.resourceUseData = resource
    this.resourceForm.resType = resource.resType
    this.resourceForm.resName = resource.resName
    this.resourceForm.amount = resource.amount
    this.resourceForm.description = resource.description
    this.resourceModal = this.ngbModal.open(this.resource, { centered: true })

  }



  onSaveResource() {
    if (this.resourceUseData == undefined) {



      const body = {
        id: "",
        reportId: this.reportSelected.id,
        incId: this.incident.id,
        resType: this.resourceForm.resType,
        resName: this.resourceForm.resName,
        amount: this.resourceForm.amount,
        description: this.resourceForm.description
      }
      this.serviceCenter.createResource(body).then(res => {
        this.resourceModal.close()
        this.getResource()
        this.toastService.success("บันทึกข้อมูลทรัพยากรเสร็จสิ้น")
      }).catch(err => {
        console.log(err);
        
        this.toastService.error("บันทึกข้อมูลทรัพยากรไม่เสร็จสิ้น")
      })
    } else {
      const param = "amount=" + this.resourceForm.amount + "&description=" + this.resourceForm.description
      this.serviceCenter.updateResource(this.resourceUseData.id, param).then(res => {
        this.resourceModal.close()
        this.getResource()
        this.toastService.success("บันทึกข้อมูลทรัพยากรเสร็จสิ้น")
      }).catch(err => {
        this.toastService.error("บันทึกข้อมูลทรัพยากรไม่เสร็จสิ้น")
      })


    }

  }


  onDeleteResource(resource: resourceModel) {
    this.serviceCenter.deleteResource(resource.id).then(res => {
      this.toastService.success("ลบข้อมูลทรัพยากรเสร็จสิ้น")
      this.getResource();
    }).catch(err => {
      this.toastService.error("ลบข้อมูลทรัพยากรไม่เสร็จสิ้น")
    })
  }


  onCarTypeChange() {
    setTimeout(() => {
      const cartype = this.carTypeList.find(item => item.car == this.resourceForm.resType)
      this.carList = this.pageService.getCarByType(cartype.type)
      this.resourceForm.resName = this.carList[0].car
    }, 1000);
  }

  setDefaultResourceForm() {
    this.resourceForm.resType = "(001)กลุ่มรถอำนวยการ";
    this.resourceForm.resName = "รถจี๊ป"
    this.resourceForm.amount = 0
    this.resourceForm.description = ""
    this.carTypeList = this.pageService.getCarType()
    this.carList = this.pageService.getCarByType("001")
    this.resourceUseData == undefined
  }


  onClose() {
    this.eventCloseResource.emit(true)
  }
}


