import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ServiceCenter } from 'src/app/@common/service-center/service-center';
import { DashboardModel } from 'src/app/@shares/model/app-model';
import { PageService } from 'src/app/page/page.service';
import { PageLayerEnum } from '../../@shares/enum/app-enum';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {



  eventType: any = [1, 2];
  tabNumber = 0
  eventTypeList: any[] = [];
  startDate: any = null;
  endDate: any = null;
  totalIncident = 0
  statusChart = {
    color: ["purple", "green"],
    title: [
      {
        text: 'จำนวนเหตุการณ์แยกตามสถานะ',
        left: 'center'
      }],
    tooltip: {
      trigger: 'item',
      formatter: '{a} <br/>{b} : {c} ({d}%)'
    },
    legend: {
      top: '90%',
      left: 'center'
    },
    series: [
      {
        name: 'สถานะ',
        type: 'pie',
        radius: ['40%', '70%'],
        avoidLabelOverlap: false,
        itemStyle: {
          borderRadius: 10,
          borderColor: '#fff',
          borderWidth: 2
        },
        label: {
          show: true,
          position: 'inside',
          formatter: '{c} ({d}%) '
        },
        data: [
          { value: 0, name: 'อยู่ระหว่างปฏิบัติงาน' },
          { value: 0, name: 'ดำเนินการเรียบร้อย' },
        ]
      }
    ]
  };


  typeChart = {
    color: ['#fd7e14'],
    title: {
      text: 'จำนวนเหตุการณ์แยกตามประเภท',
      left: 'center'
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'shadow'
      }
    },
    grid: {
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true
    },
    xAxis: {
      type: 'value',
      boundaryGap: [0, 0.01]
    },
    yAxis: {
      type: 'category',
      data: ['']
    },
    series: [
      {
        label: {
          show: true,
          position: 'inside',
          formatter: '{c}'
        },
        name: 'จำนวน',
        type: 'bar',
        data: [0]
      },
    ]
  };



  damageAgriChart = {
    tooltip: {
      trigger: 'item',
      formatter: '{a} <br/>{b} : {c} ({d}%)'
    },
    legend: {
      top: '85%',
      left: 'center'
    },
    series: [
      {
        name: 'ผลกระทบ',
        type: 'pie',
        radius: ['40%', '70%'],
        avoidLabelOverlap: false,
        itemStyle: {
          borderRadius: 10,
          borderColor: '#fff',
          borderWidth: 2
        },
        label: {
          show: true,
          position: 'inside',
          formatter: '({d}%) '
        },
        data: [
          { value: 0, name: 'เกษตรกร' },
          { value: 0, name: 'โค/กระบือ' },
          { value: 0, name: 'สุกร/แพะ/แกะ' },
          { value: 0, name: 'สัตว์ปีก' },
          { value: 0, name: 'พื้นที่เพาะเลี้ยงสัตว์น้ำ' },
        ]
      }
    ]
  };

  damageAgri2Chart = {
    tooltip: {
      trigger: 'item',
      formatter: '{a} <br/>{b} : {c} ({d}%)'
    },
    legend: {
      top: '85%',
      left: 'center'
    },
    series: [
      {
        name: 'ผลกระทบ',
        type: 'pie',
        radius: ['40%', '70%'],
        avoidLabelOverlap: false,
        itemStyle: {
          borderRadius: 10,
          borderColor: '#fff',
          borderWidth: 2
        },
        label: {
          show: true,
          position: 'inside',
          formatter: '({d}%) '
        },
        data: [
          { value: 0, name: 'ไร่' },
          { value: 0, name: 'นา' },
          { value: 0, name: 'สวน' },
        ]
      }
    ]
  };

  damagePropChart = {
    color: ["#2a6afd", "#1fcfb0"],

    tooltip: {
      trigger: 'item',
      formatter: '{a} <br/>{b} : {c} ({d}%)'
    },
    legend: {
      top: '90%',
      left: 'center'
    },
    series: [
      {
        name: 'ผลกระทบ',
        type: 'pie',
        radius: ['40%', '70%'],
        avoidLabelOverlap: false,
        itemStyle: {
          borderRadius: 10,
          borderColor: '#fff',
          borderWidth: 2
        },
        label: {
          show: true,
          position: 'inside',
          formatter: '({d}%) '
        },
        data: [
          { value: 0, name: 'บ้านเรือน (ทั้งหลัง)' },
          { value: 0, name: 'บ้านเรือน (บางส่วน)' },
        ]
      }
    ]
  };

  damagePublicChart = {
    tooltip: {
      trigger: 'item',
      formatter: '{a} <br/>{b} : {c} ({d}%)'
    },
    legend: {
      top: '80%',
      left: 'center'
    },
    series: [
      {
        name: 'ผลกระทบ',
        type: 'pie',
        radius: ['40%', '70%'],
        avoidLabelOverlap: false,
        itemStyle: {
          borderRadius: 10,
          borderColor: '#fff',
          borderWidth: 2
        },
        label: {
          show: true,
          position: 'inside',
          formatter: '({d}%) '
        },
        data: [
          { value: 0, name: 'ถนน (สาย)' },
          { value: 0, name: 'ถนน (กม.)' },
          { value: 0, name: 'ถนน (จุด)' },
          { value: 0, name: 'สะพาน' },
          { value: 0, name: 'ท่อระบายน้ำ' },
          { value: 0, name: 'ฝาย' },
          { value: 0, name: 'ทำนบ' },
          { value: 0, name: 'สถานที่ราชการ' },
          { value: 0, name: 'โรงเรียน' },
          { value: 0, name: 'ศาสนสถาน' },
        ]
      }
    ]
  };

  resourceChart = {
    color: ["#2a6afd", "#1fcfb0"],

    tooltip: {
      trigger: 'item',
      formatter: '{a} <br/>{b} : {c} ({d}%)'
    },
    legend: {
      top: '90%',
      left: 'center'
    },
    series: [
      {
        name: 'ทรัพยากร',
        type: 'pie',
        radius: ['40%', '70%'],
        avoidLabelOverlap: false,
        itemStyle: {
          borderRadius: 10,
          borderColor: '#fff',
          borderWidth: 2
        },
        label: {
          show: true,
          position: 'inside',
          formatter: '({d}%) '
        },
        data: [
          { value: 0, name: '' },
        ]
      }
    ]
  };



  incidentChart = {
    title: {
      text: 'จำนวนเหตุการณ์'
    },
    tooltip: {
      trigger: 'axis'
    },
    legend: {
      top: '90%',
      left: 'center'
    },
    grid: {
      left: '3%',
      right: '4%',
      bottom: '20%',
      containLabel: true
    },
    xAxis: {
      type: 'category',
      boundaryGap: false,
      data: ['']
    },
    yAxis: {
      type: 'value'
    },
    series: [
      {
        name: 'Active',
        type: 'line',
        data: [0]
      },
      {
        name: 'Close',
        type: 'line',
        data: []
      },
      {
        name: 'Incident',
        type: 'line',
        data: []
      }
    ]
  };

  dashboard: DashboardModel = new DashboardModel()



  viewExtent: any;


  constructor(
    private serviceCenter: ServiceCenter,
    private pageService: PageService
  ) {

  }

  ngOnInit(): void {
    this.getIncidentTypes()
    this.pageService.viewExtent.subscribe(data => {
      this.viewExtent = data
      this.getDashboard()
    })
    if (this.viewExtent != undefined) {
      this.getDashboard()
    }
    this.pageService.layerDisplay.next(PageLayerEnum.incident)
  }



  getIncidentTypes() {
    this.serviceCenter.getIncidentTypes().then(res => {
      this.eventTypeList = res
    }).catch(err => {
    })
  }

  onClearDate() {
    this.startDate = null
    this.endDate = null
    this.getDashboard()
  }

  dateChange() {
    if (this.startDate != null && this.endDate != null) {
      this.getDashboard()
    }

  }

  onSelectType() {
    setTimeout(() => {
      this.getDashboard()
    }, 1000);
  }

  getDashboard() {

    const param =
      this.getIncType() +
      (this.startDate != null ? "startDate=" + this.startDate.toISOString().substring(0, 19) + "&" : '') +
      (this.endDate != null ? "endDate=" + this.endDate.toISOString().substring(0, 19) : '')

    const body = {
      wkId: 4326,
      ymax: this.viewExtent.ymax,
      xmax: this.viewExtent.xmax,
      xmin: this.viewExtent.xmin,
      ymin: this.viewExtent.ymin
    }


    this.serviceCenter.getDashboard(param, body).then(res => {
      this.dashboard = res
      console.log(this.dashboard);

      this.setStatusTypeChart()
      this.setStatusChart()
      this.setPropChart()
      this.setAgriChart()
      this.setPublicChart()
      this.setresourceChart()
      this.setIncidentChart()

    }).catch(err => {
      console.log(err);
    })
  }


  setStatusTypeChart() {
    const data = []
    const value = []
    for (const property in this.dashboard.incidentInfoSum.typeCount) {
      data.push(this.eventTypeList.find(item => item.id == property).incidentType)
      value.push(this.dashboard.incidentInfoSum.typeCount[property])
    }
    this.totalIncident = this.dashboard.incidentInfoSum.names.length
    this.typeChart.yAxis.data = data
    this.typeChart.series[0].data = value
    this.typeChart = JSON.parse(JSON.stringify(this.typeChart))
    
  }



  setStatusChart() {
    this.statusChart.series[0].data[0].value = this.dashboard.incidentInfoSum.active
    this.statusChart.series[0].data[1].value = this.dashboard.incidentInfoSum.closed
    this.statusChart = JSON.parse(JSON.stringify(this.statusChart))
  }


  setPropChart() {
    this.damagePropChart.series[0].data[0].value = this.dashboard.damagedPropSum.houseWhole
    this.damagePropChart.series[0].data[1].value = this.dashboard.damagedPropSum.housePartly
    this.damagePropChart = JSON.parse(JSON.stringify(this.damagePropChart))
  }

  setAgriChart() {
    this.damageAgriChart.series[0].data[0].value = this.dashboard.damagedAgriSum.farmer
    this.damageAgriChart.series[0].data[1].value = this.dashboard.damagedAgriSum.cowBuff
    this.damageAgriChart.series[0].data[2].value = this.dashboard.damagedAgriSum.pigGoatSheep
    this.damageAgriChart.series[0].data[3].value = this.dashboard.damagedAgriSum.poultry
    this.damageAgriChart.series[0].data[4].value = this.dashboard.damagedAgriSum.aquaAreaRai
    this.damageAgriChart = JSON.parse(JSON.stringify(this.damageAgriChart))


    this.damageAgri2Chart.series[0].data[0].value = this.dashboard.damagedAgriSum.farmRai
    this.damageAgri2Chart.series[0].data[1].value = this.dashboard.damagedAgriSum.ricePaddyRai
    this.damageAgri2Chart.series[0].data[2].value = this.dashboard.damagedAgriSum.gardensRai
    this.damageAgri2Chart = JSON.parse(JSON.stringify(this.damageAgri2Chart))
  }

  setPublicChart() {
    this.damagePublicChart.series[0].data[0].value = this.dashboard.damagedPublicSum.roadLine
    this.damagePublicChart.series[0].data[1].value = this.dashboard.damagedPublicSum.roadKm
    this.damagePublicChart.series[0].data[2].value = this.dashboard.damagedPublicSum.roadPoint
    this.damagePublicChart.series[0].data[3].value = this.dashboard.damagedPublicSum.bridge
    this.damagePublicChart.series[0].data[4].value = this.dashboard.damagedPublicSum.drainPipe
    this.damagePublicChart.series[0].data[5].value = this.dashboard.damagedPublicSum.weir
    this.damagePublicChart.series[0].data[6].value = this.dashboard.damagedPublicSum.barrage
    this.damagePublicChart.series[0].data[7].value = this.dashboard.damagedPublicSum.gov
    this.damagePublicChart.series[0].data[8].value = this.dashboard.damagedPublicSum.school
    this.damagePublicChart.series[0].data[9].value = this.dashboard.damagedPublicSum.worship
    this.damagePublicChart = JSON.parse(JSON.stringify(this.damagePublicChart))
  }

  setresourceChart() {
    const data = []
    for (const property in this.dashboard.resourcesSum) {
      data.push({ value: this.dashboard.resourcesSum[property], name: property })
    }
    this.resourceChart.series[0].data = data
    this.resourceChart = JSON.parse(JSON.stringify(this.resourceChart))
  }

  setIncidentChart() {

    const year = ["2022"]
    const active = [0]
    const close = [0]
    const incident = [0]
    for (const property in this.dashboard.yearlyData) {
      year.push(property)
      active.push(this.dashboard.yearlyData[property].activeCount.length)
      close.push(this.dashboard.yearlyData[property].closedCount.length)
      incident.push(this.dashboard.yearlyData[property].incidentsCount.length)
    }
    this.incidentChart.xAxis.data = year
    this.incidentChart.series[0].data = active
    this.incidentChart.series[1].data = close
    this.incidentChart.series[2].data = incident
    this.incidentChart = JSON.parse(JSON.stringify(this.incidentChart))
  }


  getIncType() {
    let type = ''
    this.eventType.forEach((element: any) => {
      type += "incTypeId=" + element + "&"
    });
    return type
  }

  onTabChange(tab: any) {
    this.tabNumber = tab.index
  }

}
