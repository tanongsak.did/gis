import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavBarComponent } from './nav-bar/nav-bar.component'
import { MatButtonModule } from '@angular/material/button';
import { EventPageComponent } from './event-page/event-page.component';
import { MapComponent } from './map/map.component';
import { MatInputModule } from '@angular/material/input';
import { CreatePageComponent } from './create-page/create-page.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DateEventPipe } from '../@pipe/date-event.pipe'
import { DateComparePipe } from '../@pipe/date-compare.pipe'
import { TimeEventPipe } from '../@pipe/time-event.pipe';
import { MatDatepickerModule, } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatNativeDateModule } from '@angular/material/core';
import { TimePickerComponent } from './time-picker/time-picker.component';
import { ToastsContainer } from './toast/toast.component';
import { DrawDetailComponent } from './create-page/draw-detail/draw-detail.component';
import { DamageComponent } from './damage/damage.component';
import { ReportComponent } from './create-page/report/report.component';
import { CreateReportComponent } from './create-report/create-report.component';
import { ResourceComponent } from './resource/resource.component';
import { DashboardComponent } from './dashboard/dashboard.component'
import { EchartComponent } from './echart/echart.component';
import { ResponsibleComponent } from './responsible/responsible.component';
import { MatSliderModule } from '@angular/material/slider';
import { FilterCompareComponent } from './filter-compare/filter-compare.component';
import { MatRadioModule } from '@angular/material/radio';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { DisasterNewsComponent } from './disaster-news/disaster-news.component';
import { CreateDisasterNewsComponent } from './create-disaster-news/create-disaster-news.component';
// import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatStepperModule } from '@angular/material/stepper';


const declarations = [
  NavBarComponent,
  EventPageComponent,
  MapComponent,
  CreatePageComponent,
  DateEventPipe,
  TimeEventPipe,
  DateComparePipe,
  TimePickerComponent,
  DrawDetailComponent,
  DamageComponent,
  ReportComponent,
  CreateReportComponent,
  ResourceComponent,
  DashboardComponent,
  EchartComponent,
  ResponsibleComponent,
  NavBarComponent,
  EventPageComponent,
  MapComponent,
  CreatePageComponent,
  TimePickerComponent,
  DashboardComponent,
  EchartComponent,
  FilterCompareComponent,
  DisasterNewsComponent,
  CreateDisasterNewsComponent,

  // ToastsContainer,
]


const imports = [
  CommonModule,
  MatButtonModule,
  MatInputModule,
  MatTabsModule,
  MatFormFieldModule,
  MatIconModule,
  MatSelectModule,
  FormsModule,
  MatDatepickerModule,
  MatNativeDateModule,
  ReactiveFormsModule,
  MatSliderModule,
  MatRadioModule,
  MatTableModule,
  MatPaginatorModule,
  MatCheckboxModule,
  MatStepperModule
  // NgbModule,


]

@NgModule({

  exports: [
    ...imports,
    ...declarations
    // ToastsContainer,

  ],
  declarations: [
    ...declarations,

  ],
  imports: [
    ...imports
  ],
  providers: [
    // ServiceCenter,
    // ConnectionService,
    // LocalStorageServices,


    // { provide: NgxMatDateAdapter, useValue: 'th-TH' },
    // { provide: DateAdapter, useClass: MomentDateAdapter },
    { provide: MAT_DATE_LOCALE, useValue: 'th-TH' },
    // {
    //   provide: MAT_DATE_FORMATS,
    //   useValue: {
    //     parse: {
    //       dateInput: 'LL',
    //     },
    //     display: {
    //       dateInput: 'DD/MM/YYYY',
    //       monthYearLabel: 'MMMM YYYY',
    //       dateA11yLabel: 'LL',
    //       monthYearA11yLabel: 'MMMM YYYY',
    //     },
    //   },
    // },
  ]
})
export class ComponentModule { }
