import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { ServiceCenter } from 'src/app/@common/service-center/service-center';
import { FilterModel, IncidentModel, ReportModel } from '../../@shares/model/app-model';
import { PageService } from 'src/app/page/page.service';
import { LocalStorageServices } from 'src/app/@common/service/local-storage.service';


@Component({
  selector: 'app-responsible',
  templateUrl: './responsible.component.html',
  styleUrls: ['./responsible.component.scss']
})
export class ResponsibleComponent implements OnInit, OnDestroy {
  @Input() report: ReportModel = new ReportModel()
  @Input() incident: IncidentModel = new IncidentModel()
  @Input() type: string = 'boundarie'


  @Output() eventBack: EventEmitter<any> = new EventEmitter<any>()
  radius = 0

  filterList: FilterModel[] = []
  locationFilterList: any[] = []
  constructor(
    private serviceCenter: ServiceCenter,
    private pageService: PageService,
    private localStorageServices: LocalStorageServices
  ) {

  }

  ngOnInit(): void {

    if (this.report.id != undefined) {
      if (this.localStorageServices.getRadiusReport() == null) {
        this.radius = 25
      } else {
        this.radius = this.localStorageServices.getRadiusReport()
      }
      this.pageService.displayFilterLayer.next({ radiusDisplay: true, isDisplay: true, list: [], radius: this.radius, point: this.report.point })
    } else {
      if (this.type != "location") {
        if (this.localStorageServices.getRadiusIncident() == null) {
          this.radius = 25
        } else {
          this.radius = this.localStorageServices.getRadiusIncident()
        }
        this.pageService.displayFilterLayer.next({ radiusDisplay: true, isDisplay: true, list: [], radius: this.radius, point: this.incident.point })
      } else {
        if (this.localStorageServices.getRadiusIncidentLocation() == null) {
          this.radius = 25
        } else {
          this.radius = this.localStorageServices.getRadiusIncidentLocation()
        }
        this.pageService.displayFilterLayer.next({ radiusDisplay: true, isDisplay: false, list: [], radius: this.radius, point: this.incident.point })
      }
    }


    this.getFilter()

  }




  getFilter() {
    const type = this.report.id != undefined ? "REPORT" : "INCIDENT"
    const id = this.report.id != undefined ? this.report.id : this.incident.id
    if (this.type == "boundarie") {
      this.deleteFilter(id, type)
    } else {
      const param = "?incId=" + id + "&radiusMeters=" + (this.radius * 1000)
      this.serviceCenter.getFilterLocation(param, null).then(res => {
        let list: any = []

        res.communities.forEach((communities: any) => {
          list.push({ unitsTH: communities.UnitsTH, name: communities.name, tambonth: communities.TambonTH, amphoeth: communities.AmphoeTH, provinceth: communities.ProvinceTH, latitude: communities.Latitude, longitude: communities.Longitude, type: "communities" })
        });

        res.hospitals.forEach((communities: any) => {
          list.push({ unitsTH: communities.UnitsTH, name: communities.hosname, tambonth: communities.TambonTH, amphoeth: communities.AmphoeTH, provinceth: communities.ProvinceTH, latitude: communities.latitude, longitude: communities.longitude, type: "hospital" })
        });

        res.religions.forEach((communities: any) => {
          list.push({ unitsTH: communities.UnitsTH, name: communities.name == ' ' ? 'ไม่มีชื่อ' : communities.name, tambonth: communities.TambonTH, amphoeth: communities.AmphoeTH, provinceth: communities.ProvinceTH, latitude: communities.Latitude, longitude: communities.Longitude, type: "religion" })
        });

        res.schools.forEach((communities: any) => {
          list.push({ unitsTH: communities.UnitsTH, name: communities.SchoolName, tambonth: communities.SubDistric, amphoeth: communities.District, provinceth: communities.Province, latitude: communities.Latitude, longitude: communities.Longitude, type: "school" })
        });

        this.locationFilterList = list
        this.pageService.displayFilterLayer.next({ radiusDisplay: true, isDisplay: false, list: [], radius: this.radius, point: type == "REPORT" ? this.report.point : this.incident.point })
      }).catch(err => {
        console.log(err);
      })
    }

  }

  deleteFilter(id: any, type: any) {
    const deleteParam = "?type=" + type + "&id=" + id
    this.serviceCenter.deleteFilter(deleteParam).then(res => {
      const param = "?type=" + type + "&id=" + id + "&radiusMeters=" + (this.radius * 1000)
      this.serviceCenter.getFilter(param, null).then(res => {
        this.filterList = res
        this.pageService.displayFilterLayer.next({ radiusDisplay: true, isDisplay: true, list: this.filterList, radius: this.radius, point: type == "REPORT" ? this.report.point : this.incident.point })
      }).catch(err => {
        console.log(err);
      })
    }).catch(err => {

    })
  }


  onInputChange() {
    setTimeout(() => {
      this.limitRadius()
    }, 500);
  }

  onInputStart(event: any) {
    if (this.report.id != undefined) {
      this.pageService.displayFilterLayer.next({ radiusDisplay: true, isDisplay: true, list: [], radius: event, point: this.report.point })
    } else {

      this.pageService.displayFilterLayer.next({ radiusDisplay: true, isDisplay: this.type == "location" ? false : true, list: [], radius: event, point: this.incident.point })
    }
  }



  limitRadius() {
    if (this.radius > 50) {
      this.radius = 50
    }
    if (this.radius < 0) {
      this.radius = 0
    }
    if (this.report.id != undefined) {
      this.localStorageServices.setRadiusReport(this.radius)
    } else {
      this.type == 'location' ? this.localStorageServices.setRadiusIncidentLocation(this.radius) : this.localStorageServices.setRadiusIncident(this.radius)
    }
    this.getFilter()
  }

  onBack() {
    this.eventBack.emit(true)
  }


  ngOnDestroy(): void {
    if (this.report.id != undefined) {
      this.pageService.displayFilterLayer.next({ radiusDisplay: false, isDisplay: false, list: [], radius: this.radius, point: this.report.point })
    } else {
      this.pageService.displayFilterLayer.next({ radiusDisplay: false, isDisplay: false, list: [], radius: this.radius, point: this.incident.point })
    }
  }

  onGotoLocation(item: any) {
    if (item.longitude > 0 || item.latitude > 0) {
      this.pageService.goTo.next({ lat: item.latitude, lon: item.longitude })
    }
  }

}




