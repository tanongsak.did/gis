import { Component, Output, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceCenter } from 'src/app/@common/service-center/service-center';
import * as crypto from 'crypto-js';
import { PageService } from 'src/app/page/page.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastService } from '../toast/toast.service';
import { PageLayerEnum } from '../../@shares/enum/app-enum';
import { IncidentModel } from 'src/app/@shares/model/app-model';

@Component({
  selector: 'app-event-page',
  templateUrl: './event-page.component.html',
  styleUrls: ['./event-page.component.scss']
})
export class EventPageComponent implements OnInit {

  delete = ''
  year: string = 'all'
  status: string = "all"
  incidentType: string = "all"
  yearList = [
    { text: "ทุกปี", value: "all" },
  ]


  statusList = [
    { text: "ทุกสถานะ", value: "all" },
    { text: "กำลังดำเนินการ", value: "false" },
    { text: "ดำเนินการเสร็จสิ้น", value: "true" },
  ]

  incidentTypeList = [
    { text: "ทุกประเภท", value: "all" },
  ]



  searchText: string = ''
  incidentListDisplay: IncidentModel[] = []
  incidentList: IncidentModel[] = []
  deleteDialogModal: any;
  deleteItem: any;
  @ViewChild("deleteDialog") deleteDialog: any
  @ViewChild("viewDialog") viewDialog: any

  constructor(
    private router: Router,
    private serviceCenter: ServiceCenter,
    private pageService: PageService,
    private ngbModal: NgbModal,
    private toastService: ToastService,
  ) {

  }

  ngOnInit() {
    this.setYearList()
    this.getIncidentTypes()
    this.getIncidentList()
    this.pageService.layerDisplay.next(PageLayerEnum.incident)
  }


  getIncidentTypes() {
    this.serviceCenter.getIncidentTypes().then(res => {
      res.forEach((element: any) => {
        this.incidentTypeList.push({ text: element.incidentType, value: element.id })
      });
    }).catch(err => {
    })
  }

  setYearList() {
    for (let index = 0; index < 10; index++) {
      const year = (new Date().getFullYear() + 543 - index).toString()
      this.yearList.push({ text: "ปี " + year, value: year })
    }


  }

  getIncidentList() {
    this.serviceCenter.getIncidentList().then(res => {


      res.content.map((item: any) => {
        item.incidentDateTime = item.incidentDateTime + ".00Z"
      })
      this.incidentList = res.content.sort((a: any, b: any) => {
        return new Date(b.incidentDateTime).getTime() - new Date(a.incidentDateTime).getTime()
      })

      this.filterEvent()
      // this.incidentListDisplay = this.incidentList
      // this.pageService.createPonitIncident.next(this.incidentListDisplay)

    }).catch(err => {
    })
  }



  onCreate() {
    this.router.navigate(
      ['/pages/story-map'],
      { queryParams: { p: '2' } }
    );
  }


  onSearch() {
    this.filterEvent()
  }


  onFilter() {
    this.filterEvent()
  }

  filterEvent() {
    if (this.searchText == '') {
      this.incidentListDisplay = this.incidentList
    } else {
      this.incidentListDisplay = this.incidentList.filter(item => item.name.toUpperCase().search(this.searchText.toUpperCase()) >= 0)
    }

    if (this.year != 'all') {
      this.incidentListDisplay = this.incidentListDisplay.filter(item => (new Date(item.incidentDateTime).getFullYear() + 543) == parseInt(this.year))
    }

    if (this.status != 'all') {
      this.incidentListDisplay = this.incidentListDisplay.filter(item => item.statusClosed.toString() == this.status)
    }

    if (this.incidentType != 'all') {
      this.incidentListDisplay = this.incidentListDisplay.filter(item => item.incidentType == this.incidentType)
    }

    // this.pageService.createPonitIncident.next(this.incidentListDisplay)


    let point: any[] = []
    this.incidentListDisplay.forEach(element => {
      point.push([element.point.lon, element.point.lat])
    });
    this.pageService.extent.next(point)
  }


  onEdit(incident: any) {
    this.router.navigate(
      ['/pages/story-map'],
      { queryParams: { p: '2', data: this.pageService.encrypt(incident) } }
    );
  }

  onView(incident: any) {
    // this.ngbModal.open(this.viewDialog, { centered: true })
    // this.pageService.extent.next([[incident.point.lon, incident.point.lat]])
  }

  hover: any;
  hoverIncident(incident: any) {
    // if (incident.id != this.hover) {
    //   this.hover = incident.id
    //   this.pageService.highlightSymbolIncident.next(incident.id)
    // }
  }



  onOpenDeleteModal(incident: any) {
    this.deleteItem = incident
    this.deleteDialogModal = this.ngbModal.open(this.deleteDialog, { centered: true, size: "md" })
  }

  onDelete() {
    this.cancelDelete()
    this.serviceCenter.deleteIncident(this.deleteItem.id).then(res => {
      this.getIncidentList()
      this.toastService.success("ลบสถานการณ์เสร็จสิน")
      this.delete = ''
      this.pageService.resetIncidentLayer.next(true)
    }).catch(err => {
      this.toastService.error("ลบสถานการณ์ไม่เสร็จสิน")
    })
  }

  cancelDelete() {
    this.deleteDialogModal.close()
  }

}


