import { Component, OnInit, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';

import Map from "@arcgis/core/Map.js";
import MapView from '@arcgis/core/views/MapView.js';
import Point from "@arcgis/core/geometry/Point.js";
import Graphic from "@arcgis/core/Graphic.js";
import Locate from "@arcgis/core/widgets/Locate.js";
import Search from "@arcgis/core/widgets/Search.js";
import Sketch from "@arcgis/core/widgets/Sketch.js";
import Basemap from "@arcgis/core/Basemap.js";
import BasemapGallery from "@arcgis/core/widgets/BasemapGallery.js";
import SceneView from "@arcgis/core/views/SceneView.js";
import BasemapToggle from "@arcgis/core/widgets/BasemapToggle.js";
import FeatureLayer from "@arcgis/core/layers/FeatureLayer.js";
import Expand from "@arcgis/core/widgets/Expand.js";
import ScaleBar from "@arcgis/core/widgets/ScaleBar.js";
import esriRequest from "@arcgis/core/request.js";
import * as geometryEngine from "@arcgis/core/geometry/geometryEngine.js";
import esriId from "@arcgis/core/identity/IdentityManager.js";
import OAuthInfo from "@arcgis/core/identity/OAuthInfo.js";
import Popup from "@arcgis/core/widgets/Popup.js";
import Extent from "@arcgis/core/geometry/Extent.js";
import Circle from "@arcgis/core/geometry/Circle.js";
import Polyline from "@arcgis/core/geometry/Polyline.js";
import SimpleFillSymbol from "@arcgis/core/symbols/SimpleFillSymbol.js";
import UniqueValueRenderer from "@arcgis/core/renderers/UniqueValueRenderer.js";
import PictureMarkerSymbol from "@arcgis/core/symbols/PictureMarkerSymbol.js";
import GraphicsLayer from "@arcgis/core/layers/GraphicsLayer.js";
import * as projection from "@arcgis/core/geometry/projection.js";
import SimpleMarkerSymbol from "@arcgis/core/symbols/SimpleMarkerSymbol.js";
import { ActivatedRoute, Router } from '@angular/router';
import { PageEnum, PageLayerEnum } from '../../@shares/enum/app-enum';
import { PageService } from 'src/app/page/page.service';
import { TimeEventPipe } from '../../@pipe/time-event.pipe'
import { DateEventPipe } from '../../@pipe/date-event.pipe'
import LabelClass from "@arcgis/core/layers/support/LabelClass.js";
import esriConfig from "@arcgis/core/config.js";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LocalStorageServices } from 'src/app/@common/service/local-storage.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {


  map: any;
  view: any;
  page: number = 0
  location: any = { lat: 0, lon: 0 };
  sketch: any;
  featureIncidentLayer: any;
  additionalLineLayer: any;
  additionalPointLayer: any;
  additionalPolygonLayer: any;
  religionPlaceLayer: any
  regionLayer: any
  provBranchLayer: any
  filterLayer: any
  OPPRLayer: any
  OPTLayer: any
  hospitalLayer: any
  schoolLayer: any

  clusterConfig = {
    type: "cluster",
    clusterRadius: "150px",
    clusterMinSize: "20px",
    clusterMaxSize: "20px",
    clusterPopupTemplate: {
      title: "Cluster summary",
      content: "This cluster represents {point_count} features."
    }
  };

  timeOutExtent: any;

  reportLayer: any;
  layerList: any[] = []
  drawDialogModal: any
  graphicsLayer: any
  currentLayerPage = PageLayerEnum.incident
  isStatusPopupDeatil: boolean = false

  isSelectPointlocation: boolean = false;


  tokenLayer = "3MERWsqre28k9EsZKdm0Ko-xbrrOt7seXH_QVRy65WIjneRoVGRawNCFgAyA3wwbQ3ADFUAgCh5xpYk0Gt2B2AiAmwVAzcO6-WEnmxtg9d0gYBtMUrd38iIzzBxP-cqeMNc211DD_9l0fDtL1r9ZIikyJWDOJVcJVo1dyNuuJMVE3m2czLxBpV2A_ZoQxYAaEyL29q1q8WxF7raNvwufzA.."

  @ViewChild('createPopup') createPopup: any
  @ViewChild('layer') layer: any
  @ViewChild("drawDialog") drawDialog: any




  constructor(
    private activatedRoute: ActivatedRoute,
    private pageService: PageService,
    private router: Router,
    private ngbModal: NgbModal,
    private localStorageServices: LocalStorageServices
  ) {

    activatedRoute.queryParams.subscribe((param: any) => {
      this.page = param.p
      if (this.view != undefined) {
        this.view.graphics.removeAll();
      }
    })

    this.pageService.goTo.subscribe((data: { lat: any, lon: any }) => {
      this.goTo(data)
    })

    this.pageService.extent.subscribe((data: any[]) => {
      this.extent(data)
    })

    this.pageService.removeGraphicsLayer.subscribe(data => {
      this.removeGraphicsLayer()
    })

    this.pageService.setDeletePointlocation.subscribe(data => {
      this.removeGraphic("pointLocation")
    })

    this.pageService.layerDisplay.subscribe(data => {
      this.currentLayerPage = data
      this.displayLayerFromPage()
    })

    this.pageService.highlightSymbolIncident.subscribe(data => {
      this.highlightSymbol(data, "incident")
    })

    this.pageService.highlightSymbolReport.subscribe(data => {
      this.highlightSymbol(data, "report")
    })

    this.pageService.setSelectPointlocation.subscribe((data: boolean) => {
      this.isSelectPointlocation = data
    })

    this.pageService.resetIncidentLayer.subscribe(data => {
      this.featureIncidentLayer.refresh()
      this.view.graphics.removeAll()
    })

    this.pageService.resetreportLayer.subscribe(data => {
      this.reportLayer.refresh()
      this.view.graphics.removeAll()
    })

    this.pageService.resetPointLayer.subscribe(data => {
      this.additionalPointLayer.refresh()
      this.view.graphics.removeAll()
    })

    this.pageService.resetLineLayer.subscribe(data => {
      this.additionalLineLayer.refresh()
      this.view.graphics.removeAll()
    })

    this.pageService.resetPolygonLayer.subscribe(data => {
      this.additionalPolygonLayer.refresh()
      this.view.graphics.removeAll()
    })

    this.pageService.displayFilterLayer.subscribe((data: { radiusDisplay: boolean, isDisplay: boolean, list: any[], radius: number, point: any }) => {
      if (data.list.length > 0) {
        this.definitionExpression = ''
        data.list.forEach((element, index) => {
          this.definitionExpression += "tambonid = '" + element.tambonid + "'" + ((index + 1) != data.list.length ? " OR " : "")
        });
        this.filterLayer.definitionExpression = this.definitionExpression
      }
      const point = new Point({
        x: data.point.lon,
        y: data.point.lat
      });
      this.removeGraphic("circle")
      if (data.radiusDisplay) {
        this.addResponsiblelocation(point, data.radius)
      }
      this.filterLayer.visible = data.isDisplay
    })




    if (this.localStorageServices.getLayerDisplayr() == null) {
      this.layerList = [
        { id: 1, text: 'จุดเกิดเหตุ', display: true, isSelected: true, image: "../../../assets/image/location.svg" },
        { id: 2, text: 'โรงเรียน', display: true, isSelected: true, image: "../../../assets/image/school.svg" },
        { id: 3, text: 'โรงพยาบาล', display: true, isSelected: true, image: "../../../assets/image/hospital.svg" },
        { id: 4, text: 'ศาสนสถาน', display: true, isSelected: true, image: "../../../assets/image/religion-place.svg" },
        { id: 5, text: 'จุดรายงาน', display: true, isSelected: true, image: "../../../assets/image/action.svg", image2: "../../../assets/image/situation.svg" },
        { id: 6, text: 'ข้อมูลประกอบ points', display: true, isSelected: true, image: "../../../assets/image/POINT.svg" },
        { id: 7, text: 'ข้อมูลประกอบ lines', display: true, isSelected: true, image: "../../../assets/image/LINESTRING.png" },
        { id: 8, text: 'ข้อมูลประกอบ Polygon', display: true, isSelected: true, image: "../../../assets/image/POLYGON.png" },
        { id: 9, text: 'ปภ.ศูนย์เขต', display: true, isSelected: true, image: "../../../assets/image/logo.png" },
        { id: 10, text: 'ปภ.จังหวัด/ปภ.สาขา', display: true, isSelected: true, image: "../../../assets/image/location-pin.svg", image2: "../../../assets/image/location-prov.svg" },
        { id: 11, text: 'อปพร', display: true, isSelected: true, image: "../../../assets/image/points-oppr.svg" },
        { id: 12, text: 'อปท (อบต.)', display: true, isSelected: true, image: "../../../assets/image/points-opt.svg" },
      ]
      this.localStorageServices.setLayerDisplay(this.layerList)
    } else {
      this.layerList = this.localStorageServices.getLayerDisplayr()
    }



  }




  ngOnInit() {
    this.graphicsLayer = new GraphicsLayer();
    this.map = new Map({
      basemap: "topo-vector",
      ground: "world-elevation",
      layers: [this.graphicsLayer]
    });

    let viewScene = new SceneView({
      container: "view",
      map: this.map
    });

    let basemapToggle = new BasemapToggle({
      view: viewScene,
      nextBasemap: "hybrid"
    });

    this.view = new MapView({
      map: this.map,
      center: [100, 14.027],
      zoom: 7,
      container: "view"
    });

    this.view.popupEnabled = false;

    this.view.when(() => {
      this.sendExtent()
    })
    this.view.on('drag', (event: any) => {
      this.sendExtent()
    });
    this.view.on('mouse-wheel', (event: any) => {
      this.sendExtent()
    });

    this.view.on("pointer-move", (event: any) => {
      this.view.hitTest(event).then((response: any) => {
        if (response.results.length > 0) {
          var feature = response.results[0].graphic;
          if (feature) {
            var attributes = feature.attributes;
            if (attributes.inc_name != undefined) {
              this.isStatusPopupDeatil = true
              this.view.popup.open({
                title: "เหตุการณ์",
                content: `<small> ชื่อเหตุการณ์ ` + attributes.inc_name + `</small><br>
                <small> ประเภทเหตุการณ์ ` + attributes.inc_type + `</small><br>
                <small> สถานที่ ` + attributes.loc + `</small><br>
                <small> วันที่เกิดเหตุ ` + DateEventPipe.prototype.transform(attributes.inc_date) + " " + TimeEventPipe.prototype.transform(attributes.inc_date) + `น.</small><br>
                <small> สถานะ ` + (attributes.status == "A" ? 'กำลังดำเนินการ' : "ดำเนินการเสร็จสิ้น") + `</small>`,
                location: response.results[0].mapPoint
              });
            } else if (attributes.topic != undefined) {
              this.isStatusPopupDeatil = true
              this.view.popup.open({
                title: "รายงาน",
                content: `<small> ชื่อรายงาน ` + attributes.topic + `</small><br>
                <small> ประเภทรายงาน ` + (attributes.is_action == "N" ? 'สถานการณ์ (Situation)' : "การปฏิบัติงาน (Action)") + `</small><br>
                <small> วันที่เกิดเหตุ ` + DateEventPipe.prototype.transform(attributes.rep_date) + " " + TimeEventPipe.prototype.transform(attributes.rep_date) + `น.</small><br>
                <small> รายละเอียด ` + attributes.details + `</small><br>
                <small> สถานะ ` + (attributes.is_done == "N" ? 'กำลังดำเนินการ' : "ดำเนินการเสร็จสิ้น") + `</small>`,
                location: response.results[0].mapPoint
              });
            } else if (attributes.description != undefined) {
              // this.isStatusPopupDeatil = true
              // this.view.popup.open({
              //   title: "ข้อมูลประกอบ",
              //   content: `<small> รายละเอียด ` + attributes.description + `</small><br>`,
              //   location: response.results[0].mapPoint
              // });
            } else {
              if (this.isStatusPopupDeatil) {
                this.view.popup.close()
                this.isStatusPopupDeatil = false
              }
            }
          }
        }

      })
    })



    this.view.on("click", (event: any) => {
      this.view.hitTest(event).then((response: any) => {

        var feature = response.results[0].graphic;
        if (feature) {
          if (this.isSelectPointlocation) {
            if (event.button == 0) {
              const location = {
                lat: event.mapPoint.latitude,
                lon: event.mapPoint.longitude
              }
              this.pageService.locationForCreate.next(location)
              var point = new Point({
                x: event.mapPoint.longitude,
                y: event.mapPoint.latitude
              });
              this.removeGraphic("pointLocation")
              this.addPointlocation(point)
            } else if (event.button == 2) {
              this.removeGraphic("pointLocation")
              this.pageService.locationForCreate.next(null)
            }
          }

          if (this.page == PageEnum.list) {
            this.location.lat = event.mapPoint.latitude
            this.location.lon = event.mapPoint.longitude
            if (event.button == 0) {
              this.view.openPopup({
                title: "รายละเอียด",
                content: this.createPopup.nativeElement,
                location: event.mapPoint
              });
            } else if (event.button == 2) {
              this.view.closePopup()
            }

          }
          // }
        }
      });
    });

    const locate = new Locate({
      view: this.view,
      useHeadingEnabled: false,
      goToOverride: function (view, options) {
        options.target.scale = 1500;
        return view.goTo(options.target);
      }
    });

    const popupTemplate = {
      title: "รายละเอียด", // Use attribute names from your data
      content: (features: any) => {
        var button = document.createElement("span");
        button.textContent = "+ สร้างเหตุการณ์ ";
        button.style.color = "rgb(255, 174, 0)"
        button.style.cursor = "pointer"
        button.addEventListener("click", () => {
          this.onCreate();
        });
        return button;
      }
    };

    const search = new Search({  //Add Search widget
      view: this.view,
      popupTemplate: popupTemplate
    });

    this.view.ui.add(basemapToggle, {
      position: "bottom-right"
    });

    var scaleBar = new ScaleBar({
      view: this.view,
      unit: "dual" // The scale bar will show both metric and non-metric units.
    });

    this.view.ui.add(search, "top-right");
    this.view.ui.move("zoom", "top-right");
    this.view.ui.add(locate, "top-right");
    this.view.ui.add(scaleBar, "bottom-left");

    this.view.when(() => {
      this.drawLayer()
    })

    this.createIncidentLayer()
    this.createAdditionalLineLayer()
    this.createAdditionalPointLayer()
    this.createAdditionalPolygonLayer()
    this.createReportLayer()
    this.createReligionPlaceLayer()
    this.createHospitalLayer()
    this.createSchoolLayer()

    this.creatFilterLayer()
    this.createRegionLayer()
    this.createProvBranchLayer()
    this.createOPPRLayer()
    this.createOPTLayer()

    this.pageService.isDrawDetail.subscribe(data => {
      if (data) {
        this.view.ui.add(this.sketch, "top-left");
      } else {
        this.view.ui.remove(this.sketch);
      }
    })

  }





  ngAfterViewInit() {
    const bgExpand = new Expand({
      view: this.view,
      content: this.layer.nativeElement,
      expandIconClass: "esri-icon-layers", // Use the same icon class as in your HTML
      expandTooltip: "Layer List"
    });
    this.view.ui.add(bgExpand, "top-right");
  }



  onCreate() {
    this.view.closePopup()
    this.router.navigate(
      ['/pages/story-map'],
      { queryParams: { p: '2' } }
    );
  }


  geometry: any
  drawLayer() {
    this.sketch = new Sketch({
      layer: this.graphicsLayer,
      view: this.view,
      creationMode: "update"
    });
    this.sketch.visibleElements = {
      createTools: {
        rectangle: false,
        circle: false
      },
      selectionTools: {
        "lasso-selection": false,
        "rectangle-selection": false
      },
      settingsMenu: false,
      undoRedoMenu: false,
      snappingControls: false,
      duplicateButton: false,
      multipleSelectionEnabled: false
    }

    this.sketch.on("create", (event: any) => {
      if (event.state === "start") {
        this.graphicsLayer.removeAll()
        this.geometry = null
        this.pageService.geometry.next(this.geometry)
      }

      if (event.state === "complete") {
        event.graphic.geometry
        this.getGeometryPoint(event, event.graphic.geometry)
      }
    });

    this.sketch.on("update", (event: any) => {
      this.getGeometryPoint(event, event.graphics[0].geometry)
    })

    this.sketch.on("delete", (event: any) => {
      this.pageService.geometry.next(null)
    })
  }


  sendExtent() {
    if (this.timeOutExtent != undefined) {
      clearTimeout(this.timeOutExtent)
    }
    this.timeOutExtent = setTimeout(() => {
      var currentExtent = this.view.extent;
      var wgs84Extent = projection.project(currentExtent, {
        wkid: 4326
      });
      this.pageService.viewExtent.next(wgs84Extent)
    }, 2000);

  }

  removeGraphicsLayer() {
    this.graphicsLayer.removeAll()
  }



  getGeometryPoint(event: any, geometry: any) {
    switch (event.tool) {
      case "polyline":
        this.geometry = {
          type: "LINESTRING",
          coords: this.convertLocationToLatLon(geometry.paths)

        };
        break;
      case "point":
        this.geometry = {
          type: "POINT",
          coords: [
            {
              lat: geometry.latitude,
              lon: geometry.longitude,
              height: 0
            }
          ]
        };
        break;
      case "polygon":
        this.geometry = {
          type: "POLYGON",
          coords: this.convertLocationToLatLon(geometry.rings)

        };
        break;
      default:
        break;
    }
    this.pageService.geometry.next(this.geometry)
  }


  convertLocationToLatLon(geometry: any) {
    var paths = geometry.map((path: any) => {
      return path.map((coordinates: any) => {
        var point = new Point({
          x: coordinates[0],
          y: coordinates[1],
          spatialReference: {
            wkid: 102100 // Web Mercator projection
          }
        });
        return { lon: point.longitude, lat: point.latitude, height: 0 };
      });
    });

    return paths[0]

  }




  createPonitIncident(IncidentList: any[]) {
    this.view.graphics.removeAll();
    // IncidentList.forEach(element => {
    //   this.addPoint(new Point({
    //     y: element.point.lat,
    //     x: element.point.lon
    //   }), element)
    // });
  }



  addPoint(point: any, incident: any) {
    var symbol = new PictureMarkerSymbol({
      url: "../assets/image/location.svg",
      width: "20px",
      height: "20px"
    });
    let pointAtt = incident
    var graphic = new Graphic({
      geometry: point,
      symbol: symbol,
      attributes: pointAtt
    });

    this.view.graphics.add(graphic);
  }


  createIncidentLayer() {
    var symbol = new PictureMarkerSymbol({
      url: "../assets/image/location.svg",
      width: "20px",
      height: "20px"
    });

    var symbolDone = new PictureMarkerSymbol({
      url: "../assets/image/location-done.svg",
      width: "20px",
      height: "20px"
    });

    const renderer = new UniqueValueRenderer({
      field: "status",
      uniqueValueInfos: [
        {
          value: "A",
          symbol: symbol,
        }, {
          value: "C",
          symbol: symbolDone
        }
      ]

    })


    this.featureIncidentLayer = new FeatureLayer({
      url: "https://gis-portal.disaster.go.th/arcgis/rest/services/Hosted/ck_incident_points/FeatureServer/0",
      apiKey: this.tokenLayer,
      outFields: ["*"],
    });

    this.featureIncidentLayer.renderer = renderer
    this.featureIncidentLayer.visible = this.layerList.find(item => item.id == 1).isSelected
    this.map.add(this.featureIncidentLayer);
  }

  createReligionPlaceLayer() {
    var symbol = new PictureMarkerSymbol({
      url: "../assets/image/religion-place.svg",
      width: "20px",
      height: "20px"
    });

    var labelClass = this.labelClass("When($feature.name == ' ', 'ไม่มีชื่อ', $feature.name)")
    this.religionPlaceLayer = new FeatureLayer({
      url: "https://gis-portal.disaster.go.th/arcgis/rest/services/OSM_religious_xy/FeatureServer",
      outFields: ["*"],
      labelingInfo: [labelClass]
    });

    this.religionPlaceLayer.renderer = {
      type: "simple",
      symbol: symbol
    }
    this.clusterConfig.clusterRadius = "120px"
    this.religionPlaceLayer.featureReduction = this.clusterConfig
    this.religionPlaceLayer.visible = this.layerList.find(item => item.id == 4).isSelected
    this.map.add(this.religionPlaceLayer);

  }

  createHospitalLayer() {
    var symbol = new PictureMarkerSymbol({
      url: "../assets/image/hospital.svg",
      width: "20px",
      height: "20px"
    });

    var labelClass = this.labelClass("$feature.hosname")

    this.hospitalLayer = new FeatureLayer({
      url: "https://gis-portal.disaster.go.th/arcgis/rest/services/Hospital_MoPH/FeatureServer",
      outFields: ["*"],
      labelingInfo: [labelClass]
    });

    this.hospitalLayer.renderer = {
      type: "simple",
      symbol: symbol
    }
    this.clusterConfig.clusterRadius = "100px"
    this.hospitalLayer.featureReduction = this.clusterConfig
    this.hospitalLayer.visible = this.layerList.find(item => item.id == 3).isSelected
    this.map.add(this.hospitalLayer);

  }

  createSchoolLayer() {
    var symbol = new PictureMarkerSymbol({
      url: "../assets/image/school.svg",
      width: "20px",
      height: "20px"
    });

    var labelClass = this.labelClass("$feature.SchoolName")

    this.schoolLayer = new FeatureLayer({
      url: "https://gis-portal.disaster.go.th/arcgis/rest/services/MOE_School/FeatureServer",
      outFields: ["*"],
      labelingInfo: [labelClass]
    });

    this.schoolLayer.renderer = {
      type: "simple",
      symbol: symbol
    }

    this.clusterConfig.clusterRadius = "150px"
    this.schoolLayer.featureReduction = this.clusterConfig
    this.schoolLayer.visible = this.layerList.find(item => item.id == 2).isSelected
    this.map.add(this.schoolLayer);

  }

  definitionExpression = "22"


  creatFilterLayer() {
    this.filterLayer = new FeatureLayer({
      url: "https://gis-portal.disaster.go.th/arcgis/rest/services/Hosted/dpm_tambon_regionall/FeatureServer",
      apiKey: this.tokenLayer,
      outFields: ["*"],
      definitionExpression: this.definitionExpression
    });
    this.filterLayer.visible = false
    this.map.add(this.filterLayer);
  }


  createRegionLayer() {
    var symbol = new PictureMarkerSymbol({
      url: "../assets/image/logo.png",
      width: "14px",
      height: "20px"
    });
    this.regionLayer = new FeatureLayer({
      url: "https://gis-portal.disaster.go.th/arcgis/rest/services/Hosted/DPM008_18region/FeatureServer/0",
      apiKey: this.tokenLayer,
      outFields: ["*"],
    });
    this.regionLayer.renderer = {
      type: "simple",
      symbol: symbol
    }
    this.regionLayer.visible = this.layerList.find(item => item.id == 9).isSelected
    this.map.add(this.regionLayer);
  }


  createProvBranchLayer() {
    var symbol = new PictureMarkerSymbol({
      url: "../assets/image/location-prov.svg",
      width: "20px",
      height: "20px"
    });

    var symbol2 = new PictureMarkerSymbol({
      url: "../assets/image/location-pin.svg",
      width: "20px",
      height: "20px"
    });

    const renderer = new UniqueValueRenderer({
      field: "ประเภท",
      uniqueValueInfos: [
        {
          value: 2,
          symbol: symbol,
        }, {
          value: 1,
          symbol: symbol2
        }
      ]
    })
    var labelClass = this.labelClass("$feature.หน่วยงาน")
    this.provBranchLayer = new FeatureLayer({
      url: "https://gis-portal.disaster.go.th/arcgis/rest/services/DDPM_Location_Prov_Branch_1/FeatureServer/49",
      apiKey: this.tokenLayer,
      outFields: ["*"],
      labelingInfo: [labelClass]
    });
    this.provBranchLayer.renderer = renderer
    this.provBranchLayer.visible = this.layerList.find(item => item.id == 10).isSelected
    this.map.add(this.provBranchLayer);
  }

  createOPPRLayer() {

    var symbol = new PictureMarkerSymbol({
      url: "../assets/image/points-oppr.svg",
      width: "20px",
      height: "20px"
    });

    this.OPPRLayer = new FeatureLayer({
      url: "https://gis-portal.disaster.go.th/arcgis/rest/services/DPM023_VolunteerCenter/FeatureServer/11",
      apiKey: this.tokenLayer,
      outFields: ["*"],
    });

    this.OPPRLayer.renderer = {
      type: "simple",
      symbol: symbol
    }
    this.clusterConfig.clusterRadius = "300px"
    this.OPPRLayer.featureReduction = this.clusterConfig
    this.OPPRLayer.visible = this.layerList.find(item => item.id == 11).isSelected
    this.map.add(this.OPPRLayer);
  }


  createOPTLayer() {
    var symbol = new PictureMarkerSymbol({
      url: "../assets/image/points-opt.svg",
      width: "20px",
      height: "20px"
    });
    var labelClass = this.labelClass("$feature.DLA")
    this.OPTLayer = new FeatureLayer({
      url: "https://gis-portal.disaster.go.th/arcgis/rest/services/DLA_LocalAdministrative/FeatureServer/0",
      apiKey: this.tokenLayer,
      outFields: ["*"],
      labelingInfo: [labelClass]
    });

    this.OPTLayer.renderer = {
      type: "simple",
      symbol: symbol
    }

    this.clusterConfig.clusterRadius = "250px"
    this.OPTLayer.featureReduction = this.clusterConfig
    this.OPTLayer.visible = this.layerList.find(item => item.id == 12).isSelected
    this.map.add(this.OPTLayer);
  }



  createAdditionalLineLayer() {
    var labelClass = this.labelClass("$feature.description")
    this.additionalLineLayer = new FeatureLayer({
      url: "https://gis-portal.disaster.go.th/arcgis/rest/services/Hosted/ck_incident_additional_lines/FeatureServer/0",
      apiKey: this.tokenLayer,
      outFields: ["*"],
      labelingInfo: [labelClass]
    });
    var renderer = {
      type: "simple",
      symbol: {
        type: "simple-fill",
        color: [120, 0, 0, 0],
        outline: {
          width: 2
        }
      },

    };
    this.additionalLineLayer.renderer = renderer;
    this.additionalLineLayer.visible = this.layerList.find(item => item.id == 7).isSelected
    this.map.add(this.additionalLineLayer);
  }


  createAdditionalPolygonLayer() {
    var labelClass = this.labelClass("$feature.description")
    this.additionalPolygonLayer = new FeatureLayer({
      url: "https://gis-portal.disaster.go.th/arcgis/rest/services/Hosted/ck_incident_additional_poly/FeatureServer/0",
      apiKey: this.tokenLayer,
      outFields: ["*"],
      labelingInfo: [labelClass]
    });

    var renderer = {
      type: "simple",
      symbol: {
        type: "simple-fill",
        color: [0, 0, 0, 0.1],
        outline: {
          width: 2
        }
      },

    };
    this.additionalPolygonLayer.renderer = renderer;

    this.additionalPolygonLayer.visible = this.layerList.find(item => item.id == 8).isSelected
    this.map.add(this.additionalPolygonLayer);
  }



  createAdditionalPointLayer() {
    var symbol = new PictureMarkerSymbol({
      url: "../assets/image/POINT.svg",
      width: "20px",
      height: "20px"
    });
    var labelClass = this.labelClass("$feature.description")
    this.additionalPointLayer = new FeatureLayer({
      url: "https://gis-portal.disaster.go.th/arcgis/rest/services/Hosted/ck_incident_additional_points/FeatureServer/0",
      apiKey: this.tokenLayer,
      outFields: ["*"],
      labelingInfo: [labelClass]
    });

    this.additionalPointLayer.renderer = {
      type: "simple",
      symbol: symbol
    }
    this.additionalPointLayer.visible = this.layerList.find(item => item.id == 6).isSelected
    this.map.add(this.additionalPointLayer);
  }


  createReportLayer() {
    var symbolActionDone = new PictureMarkerSymbol({
      url: "../assets/image/action-done.svg",
      width: "20px",
      height: "20px"
    });

    var symbolAction = new PictureMarkerSymbol({
      url: "../assets/image/action.svg",
      width: "20px",
      height: "20px"
    });

    var symbolSituation = new PictureMarkerSymbol({
      url: "../assets/image/situation.svg",
      width: "20px",
      height: "20px"
    });

    var symbolSituationDone = new PictureMarkerSymbol({
      url: "../assets/image/situation-done.svg",
      width: "20px",
      height: "20px"
    });

    const renderer = new UniqueValueRenderer({
      field: "is_action",  // The first field
      field2: "is_done",
      fieldDelimiter: ", ",
      uniqueValueInfos: [
        {
          value: "Y, Y",
          symbol: symbolActionDone,
        }, {
          value: "Y, N",
          symbol: symbolAction
        }, {
          value: "N, Y",
          symbol: symbolSituationDone
        }
        , {
          value: "N, N",
          symbol: symbolSituation
        }
      ]

    })


    this.reportLayer = new FeatureLayer({
      url: "https://gis-portal.disaster.go.th/arcgis/rest/services/Hosted/ck_report_points/FeatureServer/0",
      apiKey: this.tokenLayer,
      outFields: ["*"],
    });

    this.reportLayer.renderer = renderer
    this.reportLayer.visible = this.layerList.find(item => item.id == 5).isSelected
    this.map.add(this.reportLayer);

  }

  labelClass(expression: any) {
    var labelClass = new LabelClass({
      labelExpressionInfo: { expression: expression }, // Use an Arcade expression
      symbol: {
        type: "text",
        color: "black",
        haloSize: 1,
        haloColor: "white"
      },
      labelPlacement: "above-center",
    });
    return labelClass
  }





  addPointlocation(point: any) {
    var symbol = new PictureMarkerSymbol({
      url: "../assets/image/location-create.svg",
      width: "20px",
      height: "20px"
    });

    let pointAtt = {
      Name: "pointLocation",
      Owner: "TransCanada",
    };
    var graphic = new Graphic({
      geometry: point,
      symbol: symbol,
      attributes: pointAtt
    });
    this.view.graphics.add(graphic);
  }


  addResponsiblelocation(point: any, radius: any) {

    var circle = new Circle({
      center: point,
      radius: radius,
      radiusUnit: "kilometers"
    });

    var fillSymbol = new SimpleFillSymbol({
      color: [255, 255, 255, 0.5],
      outline: {
        color: [255, 255, 255],
        width: 1
      }
    });

    let circleAtt = {
      Name: "circle",
      Owner: "TransCanada",
    };
    var circleGraphic = new Graphic({
      geometry: circle,
      symbol: fillSymbol,
      attributes: circleAtt
    });
    this.view.graphics.add(circleGraphic);
  }



  goTo(position: { lat: any, lon: any }) {
    this.view.goTo({
      center: [position.lon, position.lat],
      zoom: 15
    });
  }



  extent(points: any[]) {
    const extent = new Extent({
      xmax: Math.max(...points.map(point => point[0])) + 0.1,
      xmin: Math.min(...points.map(point => point[0])) - 0.1,
      ymax: Math.max(...points.map(point => point[1])) + 0.1,
      ymin: Math.min(...points.map(point => point[1])) - 0.1,
      spatialReference: {
        wkid: 4326
      },
      zmax: 0,
      zmin: 0
    });
    this.view.extent = extent;

  }



  removeGraphic(name: any) {
    if (this.view.graphics.items.length) {
      this.view.graphics.items.forEach((element: any) => {
        if (element.attributes.Name == name) {
          this.view.graphics.remove(element);
        }
      });
    }
  }



  onSelectLayer(isSave: boolean, item: any) {
    setTimeout(() => {
      this.featureIncidentLayer.visible = this.layerList.find(item => item.id == 1).isSelected
      this.reportLayer.visible = this.layerList.find(item => item.id == 5).isSelected
      this.additionalLineLayer.visible = this.layerList.find(item => item.id == 7).isSelected
      this.additionalPointLayer.visible = this.layerList.find(item => item.id == 6).isSelected
      this.additionalPolygonLayer.visible = this.layerList.find(item => item.id == 8).isSelected
      this.schoolLayer.visible = this.layerList.find(item => item.id == 2).isSelected
      this.hospitalLayer.visible = this.layerList.find(item => item.id == 3).isSelected
      this.religionPlaceLayer.visible = this.layerList.find(item => item.id == 4).isSelected
      this.regionLayer.visible = this.layerList.find(item => item.id == 9).isSelected
      this.provBranchLayer.visible = this.layerList.find(item => item.id == 10).isSelected
      this.OPPRLayer.visible = this.layerList.find(item => item.id == 11).isSelected
      this.OPTLayer.visible = this.layerList.find(item => item.id == 12).isSelected



      if (isSave) {
        const list = this.localStorageServices.getLayerDisplayr()
        const layer = list.find((l: any) => l.id == item.id).isSelected = item.isSelected
        this.localStorageServices.setLayerDisplay(list)
      }
    }, 500);




  }

  displayLayerFromPage() {
    switch (this.currentLayerPage) {
      case PageLayerEnum.incident:
        const id = [5, 6, 7, 8]
        id.forEach(element => {
          const layer = this.layerList.find(item => item.id == element)
          layer.display = false
          layer.isSelected = false
        })
        break;
      case PageLayerEnum.draw:
        this.layerList = this.localStorageServices.getLayerDisplayr()
        const idOpent = [6, 7, 8]
        idOpent.forEach(element => {
          const layer = this.layerList.find(item => item.id == element)
          layer.display = true
        })
        const report = this.layerList.find(item => item.id == 5)
        report.isSelected = false
        report.display = false
        break;

      case PageLayerEnum.report:
        this.layerList = this.localStorageServices.getLayerDisplayr()
        const idClose = [6, 7, 8]
        idClose.forEach(element => {
          const layer = this.layerList.find(item => item.id == element)
          layer.display = false
          layer.isSelected = false
        })
        const reportlayer = this.layerList.find(item => item.id == 5)
        reportlayer.display = true

        break;

      default:
        break;
    }
    this.onSelectLayer(false, null)
  }



  highlightSymbol(id: any, layerType: any) {
    this.view.graphics.removeAll()
    let layer = null
    if (layerType == "incident") {
      layer = this.featureIncidentLayer
    } else if (layerType == "report") {
      layer = this.reportLayer
    }

    layer.queryFeatures().then((result: any) => {
      let feature = null
      if (layerType == "incident") {
        feature = result.features.find((item: any) => item.attributes.inc_id == id);
      } else if (layerType == "report") {
        feature = result.features.find((item: any) => item.attributes.report_id == id);
      }

      if (feature) {
        var highlightSymbol = new SimpleMarkerSymbol({
          style: "circle",
          color: [0, 255, 255, 0.2],
          size: "30px",
          outline: {
            color: [0, 255, 255, 0.2],
            width: 2
          }
        });
        feature.symbol = highlightSymbol;
        this.view.graphics.add(feature);
      }
    })
  }
}


