import { Injectable, TemplateRef } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ToastService {



    toasts: any[] = [];




    open(textOrTpl: string | TemplateRef<any>, options: any = {}) {
        options.classname = 'bg-light text-black'
        this.toasts.push({ textOrTpl, ...options });
    }

    info(textOrTpl: string | TemplateRef<any>, options: any = {}) {
        options.classname = 'bg-info text-black'
        this.toasts.push({ textOrTpl, ...options });
    }



    success(textOrTpl: string | TemplateRef<any>, options: any = {}) {
        options.classname = 'bg-success text-light'
        this.toasts.push({ textOrTpl, ...options });
    }


    error(textOrTpl: string | TemplateRef<any>, options: any = {}) {
        options.classname = 'bg-danger text-light'
        this.toasts.push({ textOrTpl, ...options });
    }

    warning(textOrTpl: string | TemplateRef<any>, options: any = {}) {
        options.classname = 'bg-warning text-black'
        this.toasts.push({ textOrTpl, ...options });
    }


    remove(toast: any) {
        this.toasts = this.toasts.filter(t => t !== toast);
    }


    clear() {
        this.toasts.splice(0, this.toasts.length);
    }
}
