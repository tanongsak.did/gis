import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PageService } from 'src/app/page/page.service';
import { PageEnum } from '../../@shares/enum/app-enum';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent {
  @Input() name: string = ''
  @Input() nameEN: string = ''

  noti = {
    name: '  dsfdsfdfsds',
    location: "asdasdasd",
    incidentDateTime: new Date(),
    statusClosed: true,
    point: null
  }
  isNoti: boolean = false;
  isMenu: boolean = false;


  menuList = [
    {
      id: PageEnum.list, menu: "ระบบการจัดการข้อมูลสาธารณภัยเชิงพื้นที่", icon: "", isSelect: false,
      sub: [
        { id: PageEnum.list, menu: "เหตุการณ์", icon: "report", isSelect: false },
        { id: PageEnum.dashboard, menu: "Dashboard", icon: "bar_chart", isSelect: false },
      ]
    },
    {
      id: 4, menu: "ระบบข้อมูลประกาศแจ้งเตือน", icon: "", isSelect: false,
      sub: [
        { id: 5, menu: "จัดทำข่าวสาธารณภัย แผ่นดินไหว", icon: "description", isSelect: false },
        { id: 6, menu: "จัดทำรายงานพายุ", icon: "description", isSelect: false },
      ]
    },
    // { id: PageEnum.list, menu: "เหตุการณ์", icon: "report", isSelect: false },
    // { id: PageEnum.dashboard, menu: "Dashboard", icon: "bar_chart", isSelect: false },
  ]

  constructor(
    private pageService: PageService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {


    this.activatedRoute.queryParams.subscribe((param: any) => {
      if (param.p == undefined) {
        this.menuList[1].isSelect = true
      } else if (param.p != 3) {
        this.menuList[0].sub[0].isSelect = true
      } if (param.p == 3) {
        this.menuList[0].sub[1].isSelect = true
      }
    })

  }

  ngOnInit() {
    this.pageService.socketNoti.subscribe(data => {

      this.isNoti = false
      this.noti = {
        name: data.name,
        location: data.location,
        incidentDateTime: new Date(data.incidentDateTime + ".00Z"),
        statusClosed: data.statusClosed,
        point: data.point
      }
      this.isNoti = true

      setTimeout(() => {
        this.isNoti = false
      }, 5000);

    })
  }

  onMenu() {
    // this.pageService.isMenu.next(true)
    this.isMenu = !this.isMenu

  }


  onSelectNoti() {
    this.pageService.goTo.next(this.noti.point)
  }


  onSelectMenu(menu: any) {
    this.menuList.map(item => {
      if (item.menu == menu.menu) {
        item.isSelect = true
      } else {
        item.isSelect = false
      }
      if (item.sub != undefined) {
        item.sub.map(sub => {
          if (sub.menu == menu.menu) {
            sub.isSelect = true
          } else {
            sub.isSelect = false
          }
        })
      }

    })

    this.isMenu = false

    if (menu.id > 3) {
      this.router.navigate(
        ['/pages/compare']);
    } else {
      this.router.navigate(
        ['/pages/story-map'],
        { queryParams: { p: menu.id } }
      );
    }

  }
}
