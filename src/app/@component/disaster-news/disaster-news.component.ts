import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { EarthquakePageEnum } from 'src/app/@shares/enum/app-enum';

@Component({
  selector: 'app-disaster-news',
  templateUrl: './disaster-news.component.html',
  styleUrls: ['./disaster-news.component.scss']
})
export class DisasterNewsComponent {

  name = ""

  status = 1

  startDate = null
  endDate = null
  statusList = [
    { status: "รอการจัดทำ", value: 1 },
    { status: "จัดทำเรียบร้อย", value: 2 },
    { status: "อยู่ระหว่างจัดทำ", value: 3 },
    { status: "ยกเลิก", value: 4 }
  ]

  displayedColumns: string[] = ['no', 'name', 'source', 'dateChange', 'lavel', 'status', 'download', 'action'];
  disasterNewsList = [
    { name: "รายงานแผ่นดินไหว ทั่วโลก", source: "DSS", dateChange: new Date(), lavel: 'เฝ้าระวัง', status: "รอการจัดทำ" },
    { name: "รายงานแผ่นดินไหว ทั่วโลก", source: "DSS", dateChange: new Date(), lavel: 'เฝ้าระวัง', status: "รอการจัดทำ" },
    { name: "รายงานแผ่นดินไหว ทั่วโลก", source: "DSS", dateChange: new Date(), lavel: 'เฝ้าระวัง', status: "รอการจัดทำ" },
    { name: "รายงานแผ่นดินไหว ทั่วโลก", source: "DSS", dateChange: new Date(), lavel: 'เฝ้าระวัง', status: "รอการจัดทำ" },
    { name: "รายงานแผ่นดินไหว ทั่วโลก", source: "DSS", dateChange: new Date(), lavel: 'เฝ้าระวัง', status: "รอการจัดทำ" },
    { name: "รายงานแผ่นดินไหว ทั่วโลก", source: "DSS", dateChange: new Date(), lavel: 'เฝ้าระวัง', status: "รอการจัดทำ" },
    { name: "รายงานแผ่นดินไหว ทั่วโลก", source: "DSS", dateChange: new Date(), lavel: 'เฝ้าระวัง', status: "รอการจัดทำ" },
    { name: "รายงานแผ่นดินไหว ทั่วโลก", source: "DSS", dateChange: new Date(), lavel: 'เฝ้าระวัง', status: "รอการจัดทำ" },
    { name: "รายงานแผ่นดินไหว ทั่วโลก", source: "DSS", dateChange: new Date(), lavel: 'เฝ้าระวัง', status: "รอการจัดทำ" },
    { name: "รายงานแผ่นดินไหว ทั่วโลก", source: "DSS", dateChange: new Date(), lavel: 'เฝ้าระวัง', status: "รอการจัดทำ" },
    { name: "รายงานแผ่นดินไหว ทั่วโลก", source: "DSS", dateChange: new Date(), lavel: 'เฝ้าระวัง', status: "รอการจัดทำ" },
    { name: "รายงานแผ่นดินไหว ทั่วโลก", source: "DSS", dateChange: new Date(), lavel: 'เฝ้าระวัง', status: "รอการจัดทำ" },
    { name: "รายงานแผ่นดินไหว ทั่วโลก", source: "DSS", dateChange: new Date(), lavel: 'เฝ้าระวัง', status: "รอการจัดทำ" },
    { name: "รายงานแผ่นดินไหว ทั่วโลก", source: "DSS", dateChange: new Date(), lavel: 'เฝ้าระวัง', status: "รอการจัดทำ" },
  ]


  constructor(
    private router: Router
  ) {

  }

  onBack() {
    this.router.navigate(
      ['/pages/compare'],
      { queryParams: { c: EarthquakePageEnum.earthquake } }
    );
  }

  onCreate() {
    this.router.navigate(
      ['/pages/compare'],
      { queryParams: { c: EarthquakePageEnum.createDisasterNews } }
    );
  }
}
