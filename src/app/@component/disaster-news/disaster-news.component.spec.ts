import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisasterNewsComponent } from './disaster-news.component';

describe('DisasterNewsComponent', () => {
  let component: DisasterNewsComponent;
  let fixture: ComponentFixture<DisasterNewsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DisasterNewsComponent]
    });
    fixture = TestBed.createComponent(DisasterNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
