import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDisasterNewsComponent } from './create-disaster-news.component';

describe('CreateDisasterNewsComponent', () => {
  let component: CreateDisasterNewsComponent;
  let fixture: ComponentFixture<CreateDisasterNewsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CreateDisasterNewsComponent]
    });
    fixture = TestBed.createComponent(CreateDisasterNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
