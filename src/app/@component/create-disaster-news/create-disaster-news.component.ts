import { Component } from '@angular/core';
import { DateEventPipe } from '../../@pipe/date-event.pipe'
import { TimeEventPipe } from '../../@pipe/time-event.pipe'
import { EarthquakePageEnum } from 'src/app/@shares/enum/app-enum';
import { Router } from '@angular/router';
@Component({
  selector: 'app-create-disaster-news',
  templateUrl: './create-disaster-news.component.html',
  styleUrls: ['./create-disaster-news.component.scss']
})
export class CreateDisasterNewsComponent {

  page = 0

  descriptionForm = {
    source: 'DSS',
    date: DateEventPipe.prototype.transform(new Date()),
    time: TimeEventPipe.prototype.transform(new Date()),
    name: "รายงานแผ่นดินไหวทั่วโลก",
    createDate: DateEventPipe.prototype.transform(new Date()),
    createTime: TimeEventPipe.prototype.transform(new Date()),
    isSea: true,
    damage: 1,
    size: 6.0,
    deep: 10,
    distance: 4800,
    mapDescription: '',
    notiDescription: '',
    SMSDescription: ''
  }

  statusList = [
    { text: "รอจัดทำ", value: 1 },
    { text: "จัดทำเรียบร้อย", value: 2 },
    { text: "อยู่ระหว่างจัดทำ", value: 3 },
    { text: "ยกเลิก", value: 4 },
  ]

  provinceList = [
    { text: "พิษณุโลก ", value: 1 },
    { text: "พิจิตร", value: 2 },
    { text: "เพชรบูรณ์", value: 3 },
    { text: "เลย", value: 4 },
  ]

  mapDescriptionForm = {
    latitude: 4.45,
    longitude: 144.77,
    province: [1, 2, 3],
    status: 1
  }

  areaList = [
    { text: "บนบก", value: false },
    { text: "ทะเล", value: true },
  ]

  damageSeaList = [
    { text: "ไม่เกิดสึนามิ", value: 1 },
    { text: "เฝ้าระวัง มีโอกาศเกิดสึนามิ", value: 2 },
    { text: "แจ้งเตือน มีโอกาศเกิดสึนามิสูง", value: 3 },
  ]

  damageLandList = [
    { text: "ไม่เสียหาย", value: 1 },
    { text: "เฝ้าระวัง ", value: 2 },
    { text: "เสียหายเล็กน้อย แจ้งเตือน", value: 3 },
    { text: "เสียหายปานกลาง แจ้งเตือน", value: 3 },
    { text: "เสียหายมาก แจ้งเตือน", value: 4 },
  ]

  disasterNewsList = [
    { name: "หนังสือแจ้งเตือน" },
    { name: "โทรสาร รายงานข่าว" },
    { name: "SMS รายงานข่าว" },
    { name: "แผนที่" },
  ]

  displayedColumns = ['no', 'name', 'download']

  constructor(
    private router: Router
  ) { }


  onBack() {
    this.router.navigate(
      ['/pages/compare'],
      { queryParams: { c: EarthquakePageEnum.disasterNews } }
    );
  }

}
