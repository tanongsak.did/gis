import { Component, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
  selector: 'app-filter-compare',
  templateUrl: './filter-compare.component.html',
  styleUrls: ['./filter-compare.component.scss']
})
export class FilterCompareComponent implements OnInit {

  dateDisplay = new Date()
  dataDisplay = "1"

  criterion: any[] = [1, 2, 3, 4]
  criterionList = [
    { name: "ไม่มีรายงานข่าว", value: 1 },
    { name: "เกณฑ์รายงานข่าว", value: 2 },
    { name: "เกณฑ์เฝ้าระวัง", value: 3 },
    { name: "เกณฑ์แจ้งเตื่อนภัย", value: 4 },
  ]

  sizeMag: any[] = [1, 2, 3, 4, 5]
  sizeMagList = [
    { name: "0-1", value: 1 },
    { name: "1-2", value: 2 },
    { name: "2-3", value: 3 },
    { name: "3-4", value: 4 },
    { name: "4 ขึ้นไป", value: 5 },
  ]

  resources: any[] = [1, 2, 3, 4, 5]
  resourcesList = [
    { name: "EMSC", value: 1 },
    { name: "GEOFON", value: 2 },
    { name: "IRIS", value: 3 },
    { name: "TMD_TH", value: 4 },
    { name: "USGS_GOV", value: 5 },
  ]

  damageCriteria: any[] = [1, 2, 3, 4, 5, 6, 7]
  damageCriteriaList = [
    { name: "ไม่เกิดความเสียหาย", value: 1 },
    { name: "อาจเกิดความเสียหายเล็กน้อย", value: 2 },
    { name: "อาจเกิดความเสียหาย", value: 3 },
    { name: "เสียหายเล็กน้อย", value: 4 },
    { name: "เสียหายปานกลาง", value: 5 },
    { name: "เสียหายมาก", value: 6 },
    { name: "เสียหายรุนแรงมาก", value: 7 },
  ]

  damageTsunami: any[] = [1, 2, 3, 4]
  damageTsunamiList = [
    { name: "ไม่เกิดสึนามิ", value: 1 },
    { name: "คาดว่าไม่เกิดสึนามิ", value: 2 },
    { name: "มีโอกาสเกิดสึนามิ", value: 3 },
    { name: "มีโอกาสสูงมากเกิดสึนามิ", value: 4 }
  ]



  @Output() eventFilter: EventEmitter<any> = new EventEmitter<any>()
  constructor() {

  }

  ngOnInit(): void {
    this.onFilter()
  }

  onFilter() {
    this.eventFilter.emit({
      criterion: this.criterion,
      sizeMag: this.sizeMag,
      resources: this.resources,
      damageCriteria: this.damageCriteria,
      damageTsunami: this.damageTsunami,
      dataDisplay: this.dataDisplay,
      date: this.dateDisplay
    })
  }

}
