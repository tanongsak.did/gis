import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterCompareComponent } from './filter-compare.component';

describe('FilterCompareComponent', () => {
  let component: FilterCompareComponent;
  let fixture: ComponentFixture<FilterCompareComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FilterCompareComponent]
    });
    fixture = TestBed.createComponent(FilterCompareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
