import { Component, OnInit } from '@angular/core';
import { WebSocketAPI } from './@common/service/socket-client.service';
import { PageService } from './page/page.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'GIS';
  constructor(
    private webSocketAPI: WebSocketAPI,
    private pageService: PageService
  ) {

  }

  ngOnInit() {
    this.webSocketAPI.connect((connect: any) => {
    })

    const interval = setInterval(() => {
      console.log(this.webSocketAPI.stompClient.connected);
      if (this.webSocketAPI.stompClient.connected) {
        this.webSocketAPI.on('/topic/new-incident', (data: any) => {
          if (data != null) {
            data = JSON.parse(JSON.parse(JSON.stringify(data)))
            this.pageService.socketNoti.next(data)
          }
        })
        clearInterval(interval)
      }
    }, 500);
  }



}
