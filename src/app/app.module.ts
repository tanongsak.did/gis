import { NgModule } from '@angular/core';
import { BrowserModule, } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ComponentModule } from './@component/component.module';
import { FormsModule } from '@angular/forms';

import { ServiceCenter } from './@common/service-center/service-center'
import { ConnectionService } from "./@common/service/connection.service"
import { LocalStorageServices } from './@common/service/local-storage.service'
import { HttpClientModule } from '@angular/common/http';
import { LocalStorageService, NgxWebstorageModule } from 'ngx-webstorage';
import { ToastsContainer } from './@component/toast/toast.component'
import { WebSocketAPI } from './@common/service/socket-client.service';
@NgModule({
  declarations: [
    AppComponent,
    ToastsContainer,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [
    ServiceCenter,
    ConnectionService,
    LocalStorageServices,
    LocalStorageService,
    WebSocketAPI,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
