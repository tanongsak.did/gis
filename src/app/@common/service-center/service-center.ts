import { Injectable } from '@angular/core';
import { MasterService } from '../service/master.service';
import { ConnectionService } from '../service/connection.service';
import { ENDPOINT_TOPIC } from '../../@shares/enpoint-topic/enpoint.topic';
import { Configuration } from 'src/app/app.constant';
import { CreateIncidentModel } from '../../@shares/model/app-model';

@Injectable()
export class ServiceCenter extends MasterService {

    constructor(
        private connectService: ConnectionService,
        private configuration: Configuration
    ) {
        super();
    }



    public getIncidentTypes(): Promise<any> {
        return this.mapService(this.connectService.get(ENDPOINT_TOPIC.INCIDENT_TYPE))
    }


    public getIncidentList(): Promise<any> {
        return this.mapService(this.connectService.get(ENDPOINT_TOPIC.INCIDENT_MGNT_TACTS))

    }

    public getIncidentById(icnId: any): Promise<any> {
        return this.mapService(this.connectService.get(ENDPOINT_TOPIC.INCIDENT_MGNT_TACT + "/" + icnId))

    }

    public getDrawList(id: any): Promise<any> {
        return this.mapService(this.connectService.get(ENDPOINT_TOPIC.INCIDENT_MGNT_TACT_ADDITIONAL_FEATURES + "/" + id))

    }

    public getFilter(param: any, body: any): Promise<any> {
        return this.mapService(this.connectService.post(ENDPOINT_TOPIC.ANALYSIS_FILTER_BOUNDARIES + param, body))

    }

    public getFilterLocation(param: any, body: any): Promise<any> {
        return this.mapService(this.connectService.post(ENDPOINT_TOPIC.ANALYSIS_FILTER_LOCATION + param, body))

    }

    public deleteFilter(param: any): Promise<any> {
        return this.mapService(this.connectService.delete(ENDPOINT_TOPIC.ANALYSIS_FILTER + param))

    }

    public getDashboard(param: any, body: any): Promise<any> {
        return this.mapService(this.connectService.post(ENDPOINT_TOPIC.INCIDENT_DASHBOARD + "?" + param, body))

    }



    public getReportListById(id: any): Promise<any> {
        return this.mapService(this.connectService.get(ENDPOINT_TOPIC.INCIDENT_MGNT_TACT_REPORTS + "/" + id))
    }

    public getResourceById(id: any): Promise<any> {
        return this.mapService(this.connectService.get(ENDPOINT_TOPIC.INCIDENT_MGNT_TACT_REPORT + "/" + id + "/resource"))
    }

    public createReport(param: any, file: any): Promise<any> {
        return this.mapService(this.connectService.upload(ENDPOINT_TOPIC.INCIDENT_MGNT_TACT_REPORT + "?" + param, file))
    }

    public updateReport(incidentId: any, reprotId: any, param: any, file: any): Promise<any> {
        return this.mapService(this.connectService.patch(ENDPOINT_TOPIC.INCIDENT_MGNT_TACT + "/" + incidentId + "/report/" + reprotId + "?" + param, file))
    }



    public createResource(body: any): Promise<any> {
        return this.mapService(this.connectService.post(ENDPOINT_TOPIC.INCIDENT_MGNT_TACT_REPORT_RESOURCE, body))
    }

    public updateResource(id: any, param: any): Promise<any> {
        return this.mapService(this.connectService.patch(ENDPOINT_TOPIC.INCIDENT_MGNT_TACT_REPORT_RESOURCE + "/" + id + "?" + param, null))
    }


    public createIncident(param: any, file: any): Promise<any> {
        return this.mapService(this.connectService.upload(ENDPOINT_TOPIC.INCIDENT_MGNT_TACT + "?" + param, file))
    }

    public createDraw(body: any): Promise<any> {
        return this.mapService(this.connectService.post(ENDPOINT_TOPIC.INCIDENT_MGNT_TACT_ADDITIONAL_FEATURE, body))
    }


    public createAttachedFile(incId: any, param: any, file: any): Promise<any> {
        return this.mapService(this.connectService.upload(ENDPOINT_TOPIC.INCIDENT_MGNT_TACT + "/" + incId + "/attached-file?" + param, file))
    }

    public deleteAttachedFile(incId: any, attachedId: any): Promise<any> {
        return this.mapService(this.connectService.delete(ENDPOINT_TOPIC.INCIDENT_MGNT_TACT + "/" + incId + "/attached-file/" + attachedId))
    }


    public createAttachedFileReport(incId: any, reId: any, param: any, file: any): Promise<any> {
        return this.mapService(this.connectService.upload(ENDPOINT_TOPIC.INCIDENT_MGNT_TACT + "/" + incId + "/report/" + reId + "/attached-file?" + param, file))
    }

    public deleteAttachedFileReport(incId: any, reportId: any, attachedId: any): Promise<any> {
        return this.mapService(this.connectService.delete(ENDPOINT_TOPIC.INCIDENT_MGNT_TACT + "/" + incId + "/report/" + reportId + "/attached-file/" + attachedId))
    }


    public deleteDraw(id: any): Promise<any> {
        return this.mapService(this.connectService.delete(ENDPOINT_TOPIC.INCIDENT_MGNT_TACT_ADDITIONAL_FEATURE + "/" + id))
    }


    public deleteResource(id: any): Promise<any> {
        return this.mapService(this.connectService.delete(ENDPOINT_TOPIC.INCIDENT_MGNT_TACT_REPORT_RESOURCE + "/" + id))
    }


    public updateIncident(param: any, file: any): Promise<any> {
        return this.mapService(this.connectService.patch(ENDPOINT_TOPIC.INCIDENT_MGNT_TACT + "?" + param, file))
    }

    public deleteIncident(id: any): Promise<any> {
        return this.mapService(this.connectService.delete(ENDPOINT_TOPIC.INCIDENT_MGNT_TACT + "/" + id))
    }

    public deleteReport(incidentId: any, id: any): Promise<any> {
        return this.mapService(this.connectService.delete(ENDPOINT_TOPIC.INCIDENT_MGNT_TACT + "/" + incidentId + "/report/" + id))
    }


    public getAdministrativeBoundaryByPosition(param: any): Promise<any> {
        return this.mapService(this.connectService.get(ENDPOINT_TOPIC.SERVICE_GIS_ADMIN_BOUNDARY + '?' + param))
    }

    public getAttachedFile(icnId: any, attachedId: any): Promise<any> {
        return this.mapService(this.connectService.get(ENDPOINT_TOPIC.INCIDENT_MGNT_TACT + '/' + icnId + "/attached-file/" + attachedId))
    }

    public getAttachedFileReport(icnId: any, reportId: any, attachedId: any): Promise<any> {
        return this.mapService(this.connectService.get(ENDPOINT_TOPIC.INCIDENT_MGNT_TACT + '/' + icnId + "/report/" + reportId + "/attached-file/" + attachedId))
    }


    public updateDamageSummary(body: any): Promise<any> {
        return this.mapService(this.connectService.update(ENDPOINT_TOPIC.INCIDENT_DAMAGE_SUMMARY, body))
    }

    public updateDamageHuman(body: any): Promise<any> {
        return this.mapService(this.connectService.update(ENDPOINT_TOPIC.INCIDENT_DAMAGE_HUMAN, body))
    }

    public updateDamageProp(body: any): Promise<any> {
        return this.mapService(this.connectService.update(ENDPOINT_TOPIC.INCIDENT_DAMAGE_PROP, body))
    }

    public updateDamagePublic(body: any): Promise<any> {
        return this.mapService(this.connectService.update(ENDPOINT_TOPIC.INCIDENT_DAMAGE_PUBLIC, body))
    }
    public updateDamageAgri(body: any): Promise<any> {
        return this.mapService(this.connectService.update(ENDPOINT_TOPIC.INCIDENT_DAMAGE_AGRI, body))
    }

    public getDamageByIncidentId(id: any): Promise<any> {
        return this.mapService(this.connectService.get(ENDPOINT_TOPIC.INCIDENT_DAMAGE + "/" + id))
    }


    public getEarthquake(param: any): Promise<any> {
        return this.mapService(this.connectService.get('/earthquake/api/v1/earthquake-filter?' + param, this.configuration.proxyServer))
    }
}



