import { Injectable } from '@angular/core';
// import { LocalStorageService } from 'ngx-webstorage';
import { LOCAL_TOPIC } from 'src/app/@shares/local-topic/local.topic';

@Injectable()
export class LocalStorageServices {
    constructor(
        // private readonly localStorageService: LocalStorageService
    ) {
    }

    setActivatedUser(value: any) {
        localStorage.getItem('user-theme')
    }

    getActivatedUser() {
        return localStorage.getItem(LOCAL_TOPIC.ACTIVATED_USER)

    }

    removeActivatedUser() {
    }


    setLayerDisplay(value: any) {
        localStorage.setItem(LOCAL_TOPIC.LAYER_DISPLAY, JSON.stringify(value))
    }

    getLayerDisplayr(): any {
        const data: any = localStorage.getItem(LOCAL_TOPIC.LAYER_DISPLAY)
        return JSON.parse(data)
    }

    removeLayerDisplay() {
        localStorage.removeItem(LOCAL_TOPIC.LAYER_DISPLAY)
    }


    setRadiusReport(value: any) {
        localStorage.setItem(LOCAL_TOPIC.RADIUS_REPORT, JSON.stringify(value))
    }

    getRadiusReport(): any {
        const data: any = localStorage.getItem(LOCAL_TOPIC.RADIUS_REPORT)
        return JSON.parse(data)
    }

    removeRadiusReport() {
        localStorage.removeItem(LOCAL_TOPIC.RADIUS_REPORT)
    }


    setRadiusIncident(value: any) {
        localStorage.setItem(LOCAL_TOPIC.RADIUS_INCIDENT, JSON.stringify(value))
    }

    getRadiusIncident(): any {
        const data: any = localStorage.getItem(LOCAL_TOPIC.RADIUS_INCIDENT)
        return JSON.parse(data)
    }

    removeRadiusIncident() {
        localStorage.removeItem(LOCAL_TOPIC.RADIUS_INCIDENT)
    }


    setRadiusIncidentLocation(value: any) {
        localStorage.setItem(LOCAL_TOPIC.RADIUS_INCIDENT_LOCATION, JSON.stringify(value))
    }

    getRadiusIncidentLocation(): any {
        const data: any = localStorage.getItem(LOCAL_TOPIC.RADIUS_INCIDENT_LOCATION)
        return JSON.parse(data)
    }

    removeRadiusIncidentLocation() {
        localStorage.removeItem(LOCAL_TOPIC.RADIUS_INCIDENT_LOCATION)
    }

    removeAll() {
        // this.localStorageService.clear();
        setTimeout(() => {
            location.reload();
        }, 500)
    }

}
