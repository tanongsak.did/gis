import { Injectable } from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { Configuration } from '../../app.constant'


@Injectable()
export class WebSocketAPI {

    webSocketEndPoint: any;
    stompClient: any;
    constructor(
        private configuration: Configuration
    ) {
        this.webSocketEndPoint = this.configuration.socket
    }

    connect(callback: Function) {
        let ws = new SockJS(this.webSocketEndPoint);
        this.stompClient = Stomp.over(ws);
        this.stompClient.connect({}, (frame: any) => {
            this.stompClient.reconnect_delay = 2000;
        }, (err: any) => {
            setTimeout(() => {
                this.connect((connect: any) => { });
            }, 5000);

        });
    };

    on(topic: any, callback: Function) {
        this.stompClient.subscribe(topic, (message: any) => {
            const data = JSON.parse(JSON.stringify(message.body));
            callback(data);
        });
    }

    disconnect() {
        if (this.stompClient !== null) {
            this.stompClient.disconnect();
        }
        console.log("Disconnected");
    }




    send(topic: any, message: any) {
        this.stompClient.send(topic, {}, JSON.stringify(message));
    }


}