import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LocalStorageServices } from './local-storage.service';
import { Router } from '@angular/router';
@Injectable({
    providedIn: 'root'
})
export class HandlerErrorService {
    constructor(
        private localStorageServices: LocalStorageServices,
        private router: Router
    ) {
    }

    public handleError = (error: any) => {
        error = error.error || error;
        const message: string = error.text
        if (error === null) {
            // return Observable.throw(('error.' + null)['value']);
            return '';
        } else {
            switch (error.status) {
                case undefined:
                    return message
                    // this.sessionTimeout();
                    break;
                case 401:
                    this.sessionTimeout();
                    break;
                case 403:
                    // this.sessionTimeout();
                    break;
                case 404:
                    // this.sessionTimeout();
                    break;
                case 500:
                    // this.sessionTimeout();
                    break;
                default: return error;
                    break;
            }

        }


    }


    sessionTimeout() {
        setTimeout(() => {
            this.localStorageServices.removeActivatedUser();
            this.router.navigate(['sign-in'])
        }, 500);
    }

}
