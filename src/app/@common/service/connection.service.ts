
import { Injectable, OnInit } from '@angular/core';

import { HandlerErrorService } from './handler-error'
import { Configuration } from '../../app.constant';
import { LocalStorageServices } from './local-storage.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root',
})
export class ConnectionService {
    private headers: any;
    private authorizationTxt = 'authorization';
    public secretKey = 'Bearer ';

    constructor(
        public httpClient: HttpClient,
        private configuration: Configuration,
        private localStorageServices: LocalStorageServices,
        private handlerErrorService: HandlerErrorService,
    ) {


        this.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        };
    }


    getJSONHeader(token: any = '') {

        const sign :any = this.localStorageServices.getActivatedUser();
        if (sign !== null) {
            this.headers[this.authorizationTxt] = this.secretKey + sign.token;
        }
        if (token != '') {
            this.headers["api_key"] = token
        }
        const headers = new HttpHeaders(this.headers);
        const httpOptions = {
            headers: headers
        };
        return httpOptions;
    }

    getJSONHeaderFromData() {
        const sign :any = this.localStorageServices.getActivatedUser();
        if (sign !== null) {
            this.headers[this.authorizationTxt] = this.secretKey + sign.token;
        }
        const headers = new HttpHeaders({
            'Accept': 'application/json',
            'authorization': this.secretKey + sign.token,
        });
        const httpOptions = {
            headers: headers
        };
        return httpOptions;
    }



    get(endpoint: any, server = this.configuration.server, token = ''): Promise<any> {
        return this.httpClient
            .get(server + endpoint, this.getJSONHeader(token))
            .toPromise()
            .then((response: any) => {

                return response;
            })
            .then((response) => {
                return new Promise((resolve, reject) => {
                    resolve(response);

                });
            })
            .catch(error => {
                throw this.handlerErrorService.handleError(error);
            });
    }



    upload(endpoint: any, data: any, server = this.configuration.server): Promise<any> {
        const outGoingData = data;
        return this.httpClient
            .post(server + endpoint, data)
            .toPromise()
            .then((response: any) => {
                return response;
            })
            .then((response) => {
                return new Promise((resolve, reject) => {
                    resolve(response);
                });
            })
            .catch(error => {
                throw this.handlerErrorService.handleError(error);
            });
    }



    update(endpoint: any, data: any, server = this.configuration.server): Promise<any> {
        const outGoingData = data;
        return this.httpClient
            .put(server + endpoint, data)
            .toPromise()
            .then((response: any) => {
                return response;
            })
            .then((response) => {
                return new Promise((resolve, reject) => {
                    resolve(response);
                });
            })
            .catch(error => {
                throw this.handlerErrorService.handleError(error);
            });
    }


    patch(endpoint: any, data: any, server = this.configuration.server, token = ''): Promise<any> {
        return this.httpClient
            .patch(server + endpoint, data)
            .toPromise()
            .then((response: any) => {
                return response;
            })
            .then((response) => {
                return new Promise((resolve, reject) => {
                    resolve(response);
                });
            })
            .catch(error => {
                throw this.handlerErrorService.handleError(error);
            });
    }



    post(endpoint: any, data: any, server = this.configuration.server, token = ''): Promise<any> {
        const outGoingData = data;
        return this.httpClient
            .post(server + endpoint, outGoingData, this.getJSONHeader(token))
            .toPromise()
            .then((response: any) => {
                return response;
            })
            .then((response) => {
                return new Promise((resolve, reject) => {
                    resolve(response);
                });
            })
            .catch(error => {
                throw this.handlerErrorService.handleError(error);
            });
    }


    put(endpoint: any, data: any, server = this.configuration.server, token = ''): Promise<any> {
        const outGoingData = data;
        return this.httpClient
            .put(server + endpoint, outGoingData, this.getJSONHeader(token))
            .toPromise()
            .then((response: any) => {
                return response;
            })
            .then((response) => {
                return new Promise((resolve, reject) => {
                    resolve(response);
                });
            })
            .catch(error => {
                throw this.handlerErrorService.handleError(error);
            });
    }




    delete(endpoint: any, server = this.configuration.server, token = ''): Promise<any> {

        return this.httpClient
            .delete(server + endpoint, this.getJSONHeader(token))
            .toPromise()
            .then((response: any) => {
                return response;
            })
            .then((response) => {
                return new Promise((resolve, reject) => {
                    resolve(response);
                });
            })
            .catch(error => {
                throw this.handlerErrorService.handleError(error);
            });
    }

}

