import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
@Injectable({
    providedIn: 'root',
})
export class Configuration {
    public server: string = environment.server + '/api/v1';
    public serverGIS: string = environment.serverGIS + '/api/v1'
    public serverGISToken: string = "kN(h?q@#5:yv#:m70y&Y6vM0"
    public serverImage: string = environment.serverImage + ""
    // public server: string = '/api/v1';
    // public server: string = '/api/v2';
    // public maximumArea: number = 1000;
    // public maximumPoint: number = 10;
    // public minimumPoint: number = 4;
    // public aoiNameMax: number = 50;
    // public aoiDescriptionMax: number = 250;
    public socket: string = environment.server + '/gis-ws'
    // // public socket: string = '/rim-ws'
    // public sizeBoxAoiMax: number = 450
    // public sizeBoxAoi: number = 330
    // public offset: number = 0.5
    // public minYearSelect: number = 2014
    // public sizeTileWMS: number = 512
    // public version: string = ''

    // public location: any = { lat: 14.0045942, lon: 100.6161706 }

    // // public proxyServer : string = "http://10.0.1.62:4200/api/v2"
    // public proxyServer: string = "http://192.168.113.191:4200/api/v2"
    public proxyServer: string = "http://localhost:4200/api/v2"
    // public proxyServer: string = "http://0.0.0.0:4200/api/v2"





}
