
export enum ComparePageEnum {

}

export enum FileTypeEnum {
    "VIDEO" = 1,
    "IMAGE"
}

export enum PageReportEnum {
    "list" = 1,
    "create",
    "responsible"
}

export enum CurrentPageReport {
    createReport = 1,
    resourceList,
    createResource
}


export enum PageLayerEnum {
    incident = 1,
    draw,
    report
}

export enum PageEnum {
    list = 1,
    create = 2,
    dashboard = 3
}

export enum EarthquakePageEnum {
    earthquake = 1,
    disasterNews = 2,
    createDisasterNews = 3
}