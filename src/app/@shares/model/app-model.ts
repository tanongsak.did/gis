export class DeviceModel {
    browser!: string;
    browser_version!: string;
    device!: string;
    deviceType!: string;
    orientation!: string;
    os!: string;
    os_version!: string;
    platformId!: string;
    reTree: any;
    ua!: string;
    userAgent!: string;
}


export class DashboardModel {

    incidentInfoSum!: {
        typeCount: any,
        active: number,
        closed: number,
        names: string[]
    }
    damagedAgriSum!: {
        id: string,
        incId: string,
        additional: string,
        accumulated: boolean,
        damagedUpdateDateTime: string,
        farmer: number,
        farmRai: number,
        ricePaddyRai: number,
        gardensRai: number,
        cowBuff: number,
        pigGoatSheep: number,
        poultry: number,
        aquaAreaRai: number
    }
    damagedHumanSum!: {
        id: string,
        incId: string,
        additional: string,
        accumulated: boolean,
        damagedUpdateDateTime: string,
        emigrant: number,
        injured: number,
        dead: number,
        missing: number,
        hhAffected: number,
        pplImpacted: 0
    }
    damagedPropSum!: {
        id: string,
        incId: string,
        additional: string,
        accumulated: boolean,
        damagedUpdateDateTime: string,
        houseWhole: number,
        housePartly: number
    }
    damagedPublicSum!: {
        id: string,
        incId: string,
        additional: string,
        accumulated: boolean,
        damagedUpdateDateTime: string,
        roadLine: number,
        roadKm: number,
        roadPoint: number,
        bridge: number,
        drainPipe: number,
        weir: number,
        barrage: number,
        gov: number,
        school: number,
        worship: number
    }
    damagedSum!: {
        id: string,
        incId: string,
        additional: string,
        accumulated: boolean,
        damagedUpdateDateTime: string,
        provinceTotal: number,
        districtTotal: number,
        subDistrictTotal: number,
        villageTotal: number,
        damagedValMbaht: number
    }
    resourcesSum!: any
    yearlyData!: any
}



export class IncidentTypeModel {
    id!: number;
    incidentType!: string;
}


export class CreateIncidentModel {
    incName !: string;
    incTypeId !: number;
    dateTime !: string;
    lon!: number;
    lat!: number;
    loc!: string;
    desc?: string;
    incTypeOther?: string;

}

export class ReportModel {
    id!: string
    incId!: string
    reportDateTime!: string
    topic!: string
    details!: string
    attachedFiles!: string
    attachedImages!: string
    videosUrl!: string
    createdBy!: string
    createdDate!: string
    updatedBy!: string
    updatedDate!: string
    point!: {
        lat: number,
        lon: number,
        height: number
    }
    statusDone!: boolean
    action!: boolean
    disZoneDeclare!: string
    reliefZoneDeclare!: string
    allAttachedFiles?: any
    imageDisplay?: any

}

export class IncidentModel {
    bufferRadius !: null;
    createdBy !: string;;
    createdDate !: string;;
    details!: string;;
    id!: string;
    incidentDateTime!: string;
    incidentType!: string;
    incidentTypeOther!: string;
    location!: string;
    name!: string;
    point!: { lat: number, lon: number, height: number };
    statusClosed!: boolean;
    updatedBy!: string;
    updatedDate!: string;
    respOrg!: string
    videosUrl?: string[]
    allAttachedFiles?: any

}

export class resourceModel {
    id!: string
    reportId!: string
    incId!: string
    resType!: string
    resName!: string
    amount!: number
    description!: string
}

export class FilterModel {
    id!: string
    tambonid!: number
    provinceth!: string
    amphoeen!: string
    f4region!: string
    tmdregion!: string
    formalregion!: string
    tambonth!: string
    amphoeth!: string
    units!: string
    provinceid!: number
    unitsth!: string
    amphoeid!: number
    metroregion!: string
    tambonen!: string
    provinceen!: string
    risklevel_count!: number

}

export class EarthquakeResModel {
    Earthquake_key!: string;
    Earthquake_date!: string;
    Earthquake_mag!: number;
    Earthquake_lat!: number;
    Earthquake_long!: number;
    Earthquake_depth!: number;
    Earthquake_region!: string;
    Earthquake_timestamp!: string;
    Earthquake_source!: string;
    status_land!: string;
    distance_th!: number;
    status_inth!: string;
    edit_pdf!: string | null;
}