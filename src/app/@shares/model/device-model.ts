export class DeviceModel {
    browser!: string;
    browser_version!: string;
    device!: string;
    deviceType!: string;
    orientation!: string;
    os!: string;
    os_version!: string;
    platformId!: string;
    reTree: any;
    ua!: string;
    userAgent!: string;
}
