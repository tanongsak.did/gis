// tslint:disable-next-line:class-name
export class LOCAL_TOPIC {
    public static readonly ACTIVATED_USER = 'activatedUser';
    public static readonly LAYER_DISPLAY = 'LayerDisplay';
    public static readonly RADIUS_REPORT = 'RadiusReport';
    public static readonly RADIUS_INCIDENT = 'RadiusIncident';
    public static readonly RADIUS_INCIDENT_LOCATION = 'RadiusIncidentLocation';



}
