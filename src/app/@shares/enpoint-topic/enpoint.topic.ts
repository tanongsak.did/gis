// tslint:disable-next-line:class-name
export class ENDPOINT_TOPIC {


    public static readonly INFO = '/info';
    public static readonly INCIDENT_TYPE = '/incident-types';
    public static readonly INCIDENT_MGNT_TACTS = '/incident-mgnt-tacts';
    public static readonly INCIDENT_MGNT_TACT = '/incident-mgnt-tact';
    public static readonly INCIDENT_MGNT_TACT_ADDITIONAL_FEATURES = '/incident-mgnt-tact/additional-features';
    public static readonly INCIDENT_MGNT_TACT_ADDITIONAL_FEATURE = '/incident-mgnt-tact/additional-feature';
    public static readonly INCIDENT_MGNT_TACT_REPORT = '/incident-mgnt-tact/report';
    public static readonly INCIDENT_MGNT_TACT_REPORTS = '/incident-mgnt-tact/reports';
    public static readonly INCIDENT_DASHBOARD = '/dashboard/incs-summary';
    public static readonly ANALYSIS_FILTER = '/analysis/filter';
    public static readonly ANALYSIS_FILTER_BOUNDARIES = '/analysis/filter/admin-boundaries';
    public static readonly ANALYSIS_FILTER_LOCATION = '/analysis/filter/locations';

    public static readonly INCIDENT_MGNT_TACT_REPORT_RESOURCE = '/incident-mgnt-tact/report/resource';

    public static readonly GIS_CURRENT_ADDRESS = "/gis/current_address"
    public static readonly SERVICE_GIS_ADMIN_BOUNDARY = "/services/gis/admin-boundary"

    public static readonly INCIDENT_DAMAGE_SUMMARY = "/incident-mgnt-tact/damaged/summary"
    public static readonly INCIDENT_DAMAGE_HUMAN = "/incident-mgnt-tact/damaged/human"
    public static readonly INCIDENT_DAMAGE_PROP = "/incident-mgnt-tact/damaged/prop"
    public static readonly INCIDENT_DAMAGE_PUBLIC = "/incident-mgnt-tact/damaged/public"
    public static readonly INCIDENT_DAMAGE_AGRI = "/incident-mgnt-tact/damaged/agri"
    public static readonly INCIDENT_DAMAGE = "/incident-mgnt-tact/damaged"

}
