import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageRoutingModule } from './page-routing.module';
import { ComponentModule } from '../@component/component.module';
import { PageComponent } from './page.component';
import { StoryMapComponent } from './story-map/story-map.component';
import { CompareComponent } from './compare/compare.component';

@NgModule({
  declarations: [
    PageComponent,
    StoryMapComponent,
    CompareComponent
  ],
  imports: [
    ComponentModule,
    CommonModule,
    PageRoutingModule,
    ComponentModule

  ],
  providers: [],
  bootstrap: [PageComponent]
})
export class PageModule { }
