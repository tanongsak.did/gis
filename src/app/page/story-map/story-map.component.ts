import { Component, OnInit } from '@angular/core';
import { PageService } from "../page.service"
import { Router, ActivatedRoute } from '@angular/router';
import { PageEnum } from 'src/app/@shares/enum/app-enum';
@Component({
  selector: 'app-story-map',
  templateUrl: './story-map.component.html',
  styleUrls: ['./story-map.component.scss']
})
export class StoryMapComponent implements OnInit {

  isTapOpen: boolean = true
  pageEnum = PageEnum
  page = 0

  constructor(
    private pageService: PageService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.pageService.page.next(0)
  }

  ngOnInit() {
    this.pageService.isMenu.subscribe(data => {
      this.isTapOpen = !this.isTapOpen
    })

    this.activatedRoute.queryParams.subscribe((param: any) => {
      console.log(param);

      if (param.p == undefined) {
        this.router.navigate(
          ['/pages/story-map'],
          { queryParams: { p: '1' } }
        );
      }
      this.page = param.p
    })
  }



}

