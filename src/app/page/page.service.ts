import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import * as crypto from 'crypto-js';
import { dataTool } from 'echarts';


@Injectable({
  providedIn: 'root'
})
export class PageService {

  public isMenu: Subject<boolean> = new Subject<boolean>();
  public locationForCreate: Subject<any> = new Subject<any>();
  public isDrawDetail: Subject<any> = new Subject<any>();
  public page: Subject<any> = new Subject<any>();
  public goTo: Subject<any> = new Subject<any>();
  public extent: Subject<any> = new Subject<any>();
  public socketNoti: Subject<any> = new Subject<any>();
  public geometry: Subject<any> = new Subject<any>();
  public resetIncidentLayer: Subject<any> = new Subject<any>();
  public resetreportLayer: Subject<any> = new Subject<any>();
  public resetPointLayer: Subject<any> = new Subject<any>();
  public resetLineLayer: Subject<any> = new Subject<any>();
  public resetPolygonLayer: Subject<any> = new Subject<any>();
  public viewExtent: Subject<any> = new Subject<any>();
  public layerDisplay: Subject<any> = new Subject<any>();
  public setSelectPointlocation: Subject<any> = new Subject<any>();
  public setDeletePointlocation: Subject<any> = new Subject<any>();
  public highlightSymbolIncident: Subject<any> = new Subject<any>();
  public highlightSymbolReport: Subject<any> = new Subject<any>();
  public removeGraphicsLayer: Subject<any> = new Subject<any>();


  public displayFilterLayer: Subject<{ radiusDisplay: boolean, isDisplay: boolean, list: any[], radius: number, point: any }> = new Subject<any>();

  ViewExtent: any;
  constructor() {
    this.viewExtent.subscribe(data => {
      this.ViewExtent = dataTool
    })
  }


  encrypt(data: any) {
    return crypto.AES.encrypt(JSON.stringify(data), 'secret key 123').toString();
  }

  decrypt(data: any) {
    const bytes = crypto.AES.decrypt(data, 'secret key 123')
    return JSON.parse(bytes.toString(crypto.enc.Utf8));
  }

  getViewExtent() {
    return this.ViewExtent
  }



  getCarType() {
    return this.carType;
  }

  getCarByType(id: string) {
    return this.carList[id];
  }


  carType = [
    { type: "001", car: "(001)กลุ่มรถอำนวยการ" },
    { type: "002", car: "(002)กลุ่มรถบรรทุกขนาดเล็ก" },
    { type: "003", car: "(003)กลุ่มรถบรรทุกขนาดใหญ่" },
    { type: "004", car: "(004)กลุ่มรถบริการน้ำมัน" },
    { type: "005", car: "(005)กลุ่มรถบรรทุกน้ำ" },
    { type: "006", car: "(006)กลุ่มรถบรรทุกเทท้าย" },
    { type: "007", car: "(007)กลุ่มรถบรรทุกขนาดใหญ่ติดตั้งเครน" },
    { type: "008", car: "(008)กลุ่มรถตู้คอนเทนเนอร์" },
    { type: "009", car: "(009)กลุ่มรถหัวลาก" },
    { type: "010", car: "(010)กลุ่มรถแทรคเตอร์ตีนตะขาบขนาดใหญ่" },
    { type: "011", car: "(011)กลุ่มรถแทรคเตอร์ตีนตะขาบขนาดกลาง" },
    { type: "012", car: "(012)กลุ่มรถอุปโภคและบริโภค" },
    { type: "013", car: "(013)กลุ่มรถแทรคเตอร์ล้อยาง" },
    { type: "015", car: "(015)กลุ่มรถตัก" },
    { type: "016", car: "(016)กลุ่มรถเกลี่ยดิน" },
    { type: "018", car: "(018)กลุ่มรถบดล้อยาง" },
    { type: "019", car: "(019)กลุ่มรถบดล้อเหล็ก" },
    { type: "020", car: "(020)กลุ่มรถบดตีนแกะ" },
    { type: "021", car: "(021)กลุ่มรถเครน" },
    { type: "022", car: "(022)กลุ่มผสมคอนกรีต" },
    { type: "023", car: "(023)กลุ่มเครื่องบดอัด" },
    { type: "024", car: "(024)กลุ่มเครื่องเจาะ" },
    { type: "025", car: "(025)กลุ่มเครื่องสูบน้ำ" },
    { type: "026", car: "(026)กลุ่มรถกู้ภัย" },
    { type: "027", car: "(027)กลุ่มรถดับเพลิง" },
    { type: "028", car: "(028)กลุ่มรถผลิตไฟฟ้า" },
    { type: "029", car: "(029)กลุ่มเรือกู้ภัย" },
    { type: "030", car: "(030)กลุ่มหางลากพ่วง" },
    { type: "031", car: "(031)กลุ่มเครื่องยนต์เรือ" },
    { type: "032", car: "(032)กลุ่มอากาศยาน" },
    { type: "033", car: "(033)กลุ่มสะพาน" },
    { type: "034", car: "(034)กลุ่มรถยก" },
    { type: "035", car: "(035)กลุ่มรถขุดตักไฮดรอลิค" },
    { type: "036", car: "(036)กลุ่มเครื่องเจาะบ่อบาดาลแบบหมุน" },
    { type: "040", car: "(040)กลุ่มเครื่องเจาะบ่อน้ำตื้น" },
    { type: "041", car: "(041)กลุ่มรถจักรยานยนต์" },
    { type: "051", car: "(051)กลุ่มเครื่องมือขึ้นรูปโลหะ" },
    { type: "052", car: "(052)กลุ่มเครื่องประจุแบตเตอรี่" },
    { type: "053", car: "(053)กลุ่มเครื่องล้างทำความสะอาด" },
    { type: "054", car: "(054)กลุ่มเครื่องยกไฮดรอลิค" },
    { type: "055", car: "(055)กลุ่มเครื่องมือเจาะ" },
    { type: "056", car: "(056)กลุ่มเครื่องมือวัดและปรับแต่งเครื่องยนต์" },
    { type: "057", car: "(057)กลุ่มเครื่องตัดเหล็ก" },
    { type: "058", car: "(058)กลุ่มเครื่องกำเนิดไฟฟ้า" },
    { type: "059", car: "(059)กลุ่มเครื่องเจียรนัย" },
    { type: "060", car: "(060)กลุ่มเครื่องมือปรับแต่งกระบอกสูบ/ลูกสูบ" },
    { type: "061", car: "(061)กลุ่มประแจกระแทก" },
    { type: "062", car: "(062)กลุ่มเครื่องมือหัวฉีด" },
    { type: "063", car: "(063)กลุ่มแม่แรง" },
    { type: "064", car: "(064)กลุ่มเครื่องกลึง" },
    { type: "065", car: "(065)กลุ่มเครื่องวัดไฟฟ้า" },
    { type: "066", car: "(066)กลุ่มเครื่องอัด" },
    { type: "067", car: "(067)กลุ่มรอกโซ่" },
    { type: "068", car: "(068)กลุ่มปากกาจับ" },
    { type: "069", car: "(069)กลุ่มเครื่องเชื่อม" },
    { type: "070", car: "(070)กลุ่มเครื่องมือประจำโรงซ่อม" },
    { type: "071", car: "(071)กลุ่มเครื่องมือทำเกลียว" },
    { type: "072", car: "(072)กลุ่มเลื่อยประจำโรงซ่อม" },
    { type: "073", car: "(073)กลุ่มเครื่องไส" },
    { type: "074", car: "(074)กลุ่มเครื่องมือชุดถอด-ประกอบบู๊ช" },
    { type: "075", car: "(075)กลุ่มชุดบริการหล่อลื่นและบำรุงรักษาเครื่องจักรกล" },
    { type: "076", car: "(076)กลุ่มเครื่องอัดอากาศ" },
    { type: "077", car: "(077)กลุ่มเครื่องทำความสะอาดหัวเทียน" },
    { type: "078", car: "(078)กลุ่มเครื่องมือตัดประจำโรงซ่อม" },
    { type: "079", car: "(079)กลุ่มเครื่องมือสีรถ" },
    { type: "080", car: "(080)กลุ่มเครื่องมือชุดบัดกรี" },
    { type: "081", car: "(081)กลุ่มเครื่องทดสอบสปริง" },
    { type: "082", car: "(082)กลุ่มวิทยุรับ-ส่ง" },
    { type: "083", car: "(083)กลุ่มเครื่องมือชุดดูดอุปกรณ์" },
    { type: "084", car: "(084)กลุ่มเครื่องมือตรวจสอบมอเตอร์" },
    { type: "085", car: "(085)กลุ่มเครื่องมือเครื่องปรับอากาศ " },
    { type: "086", car: "(086)กลุ่มเครื่องมือระบบไฮดรอลิค" },
    { type: "087", car: "(087)กลุ่มเครื่องมือวัดระยะ" },
    { type: "088", car: "(088)กลุ่มแท่นติดตั้งซ่อมเครื่องจักรกล" },
    { type: "089", car: "(089)กลุ่มเครื่องมือจัดการคอนกรีต" },
    { type: "090", car: "(090)กลุ่มเครื่องมือซ่อมบ่อบาดาล" },
    { type: "091", car: "(091)กลุ่มถังน้ำมัน" },
    { type: "092", car: "(092)กลุ่มอุปกรณ์การเจาะ" },
    { type: "093", car: "(093)กลุ่มเครื่องมือซ่อมล้อรถ" },
    { type: "094", car: "(094)กลุ่มเครื่องมือทดสอบแรงม้า" },
    { type: "095", car: "(095)กลุ่มเตาอบชุบโลหะ" },
    { type: "096", car: "(096)กลุ่มเครื่องกัดเซาะโลหะ" },
    { type: "097", car: "(097)กลุ่มเครื่องพับเหล็ก" },
    { type: "098", car: "(098)กลุ่มเครื่องดัดท่อ" },
    { type: "121", car: "(121)กลุ่มชุดโคมไฟฟ้าส่องสว่าง" },
    { type: "122", car: "(122)กลุ่มเครื่องตัดหญ้า " },
    { type: "123", car: "(123)กลุ่มเครื่องดูด/เป่า " },
    { type: "124", car: "(124)กลุ่มอุปกรณ์กู้ชีพ" },
    { type: "125", car: "(125)กลุ่มอุปกรณ์ป้องกันส่วนบุคคล" },
    { type: "126", car: "(126)กลุ่มอุปกรณ์เจาะ ตัด ทำลาย" },
    { type: "127", car: "(127)กลุ่มเครื่องตัดถ่างค้ำยัน" },
    { type: "128", car: "(128)กลุ่มชุดเครื่องช่วยหายใจ " },
    { type: "129", car: "(129)กลุ่มเครื่องอัดอากาศบริสุทธิ์ " },
    { type: "130", car: "(130)กลุ่มอุปกรณ์ประชาสัมพันธ์" },
    { type: "131", car: "(131)เครื่องมือดับเพลิง" },
  ]



  carList: any = {
    "001": [
      { car: "รถจี๊ป" },
      { car: "รถตรวจการณ์" },
      { car: "รถยนต์เก๋ง" },
      { car: "รถอำนวยการสื่อสาร" },
      { car: "รถตรวจการณ์สมรรถนะสูง ขับเคลื่อน 4 ล้อ" },
      { car: "รถบัญชาการเหตุการณ์" },
    ],
    "002": [
      { car: "รถบรรทุกขนาดเล็ก แบบที่นั่งตอนเดียว" },
      { car: "รถบรรทุกขนาดเล็ก แบบที่นั่งตอนครึ่ง" },
      { car: "รถบรรทุกขนาดเล็ก แบบที่นั่ง 2 ตอน" },
      { car: "รถโดยสารขนาดเล็ก (รถตู้ขนาด 9,11,12,15 ที่นั่ง )" },
      { car: "รถพยาบาล" },
      { car: "รถตู้เอนกประสงค์" },
      { car: "รถบรรทุกขยะขนาดเล็ก" },
      { car: "รถบรรทุกขนาดเล็กขับเคลื่อน 4 ล้อ แบบที่นั่งตอนเดียว (4x4)" },
      { car: "รถบรรทุกขนาดเล็กขับเคลื่อน 4 ล้อ แบบที่นั่งตอนครึ่ง (4x4)" },
      { car: "รถบรรทุกขนาดเล็กขับเคลื่อน 4 ล้อ แบบที่นั่ง 2 ตอน (4x4)" },
    ],
    "003": [
      { car: "รถบรรทุกขนาดใหญ่ ขนาด 6 ตัน 6 ล้อ" },
      { car: "รถบรรทุกขนาดใหญ่ ขนาด 12 ตัน 10 ล้อ" },
      { car: "รถบรรทุกขนาดกลาง ขนาด 4 ตัน 4 ล้อ หรือ 6 ล้อ" },
      { car: "รถบรรทุกขนาดใหญ่ติดตั้งตู้เก็บโสตทัศนูปกรณ์" },
      { car: "รถโดยสารขนาดใหญ่" },
      { car: "รถบรรทุกเอนกประสงค์" },
      { car: "รถบรรทุกติดตั้งเครื่องเจาะบ่อบาดาล แบบหมุน " },
      { car: "รถบรรทุกติดตั้งเครื่องเจาะบ่อบาดาล แบบกระแทก" },
      { car: "รถบรรทุกติดตั้งเครื่องเจาะบ่อน้ำตื้น" },
      { car: "รถบรรทุกติดตั้งเครื่องอัดอากาศขนาดใหญ่ " },
      { car: "รถปฏิบัติการบรรเทาอุทกภัย พร้อมเครื่องสูบน้ำขนาดใหญ่" },
      { car: "รถบรรทุกติดตั้งเครื่องสูบน้ำระยะไกล" },
      { car: "รถบรรทุกติดตั้งชุดสูบน้ำท่วม/ขัง" },
      { car: "รถปฏิบัติการเคลื่อนย้ายผู้ประสบภัย พร้อมอุปกรณ์ " },
      { car: "รถสูบส่งน้ำ ไม่น้อยกว่า 35,000 ลิตร/นาที และส่งน้ำระยะไกลไม่น้อยกว่า 10 กิโลเมตร พร้อมอุปกรณ" },
      { car: "รถสูบน้ำกู้ภัยเคลื่อนที่สมรรถนะสูง แบบโมบายยูนิต พร้อมอุปกรณ์ " },
    ],

    "004": [
      { car: "รถบริการน้ำมันหล่อลื่น ขนาด 6 ตัน 6 ล้อ" },
      { car: "รถบริการน้ำมันหล่อลื่น ขนาด 2-4 ตัน 4 ล้อ หรือ 6 ล้อ" },
      { car: "รถบริการหล่อลื่นและซ่อมบำรุงเครื่องจักรกล" },
    ],
    "005": [
      { car: "รถบรรทุกน้ำ ขนาด 6,000 ลิตร" },
      { car: "รถบรรทุกน้ำ ขนาด 10,000 ลิตร" },
      { car: "รถบรรทุกน้ำ ขนาด 1,200 ลิตร แบบ 4 ล้อ ที่ รพช. ผลิต" },
      { car: "รถบรรทุกน้ำมันเชื้อเพลิง ขนาด 6,000 ลิตร 6 ตัน 6 ล้อ" },
      { car: "รถผลิตน้ำดื่ม" },
      { car: "รถบรรทุกน้ำ ขนาด 5,000 ลิตร" },
      { car: "รถบรรทุกน้ำมันเชื้อเพลิงอากาศยาน ขนาด 12,000 ลิตร" },
      { car: "รถบรรทุกน้ำอเนกประสงค์ ขนาด 12,000 ลิตร" },
    ],

    "006": [
      { car: "รถบรรทุกเทท้าย ขนาด 6 ตัน 6 ล้อ" },
      { car: "รถบรรทุกเทท้าย ขนาด 12 ตัน 10 ล้อ" },
      { car: "รถบรรทุกเทท้าย แบบมีขอเกี่ยวยกกระบะ" },

    ],
    "007": [
      { car: "รถบรรทุกขนาดใหญ่ ขนาด 6 ตัน 6 ล้อ ติดตั้งเครน ขนาดน้อยกว่า 6 ตัน" },
      { car: "รถบรรทุกขนาดใหญ่ ขนาด 6 ตัน 6 ล้อ ติดตั้งเครื่องยกพัฒนาบ่อบาดาล" },
      { car: "รถบรรทุกขนาดใหญ่ ขนาด 12 ตัน 10 ล้อ ติดตั้งเครน ขนาด 6 ตันขึ้นไป" },

    ],
    "008": [
      { car: "รถบริการซ่อมเคลื่อนที่เร็ว" },
      { car: "รถบรรทุกแบบตู้คอนเทนเนอร์เอนกประสงค์" },
      { car: "รถบรรทุกขนาดเล็ก แบบตู้คอนเทนเนอร์" },
    ],
    "009": [
      { car: "รถหัวลาก" },
      { car: "รถบรรทุกกระบะขนย้ายเครื่องจักรกล ขนาด 12 ตัน 10 ล้อ" },
      { car: "รถหัวลากขนาดเล็ก แบบ 4 X 4 ที่ รพช. ผลิต" },
      { car: "รถลากอากาศยาน" },
    ],
    "010": [
      { car: "รถแทรคเตอร์ตีนตะขาบขนาดใหญ่ ขนาด 150 แรงม้าขึ้นไป" },
      { car: "รถแทรคเตอร์ตีนตะขาบขนาดใหญ่ แบบ LGP ขนาด 150 แรงม้าขึ้นไป" },

    ],
    "011": [
      { car: "รถแทรคเตอร์ตีนตะขาบขนาดกลาง ขนาดต่ำกว่า 150 แรงม้า" },
      { car: "รถแทรคเตอร์ตีนตะขาบขนาดกลาง แบบ LGP ขนาดต่ำกว่า 150 แรงม้า" },

    ],
    "012": [
      { car: "รถประกอบอาหารพร้อมอุปกรณ์" },
    ],
    "013": [
      { car: "รถแทรคเตอร์ล้อยาง" },
      { car: "รถแทรคเตอร์ล้อยาง แบบขับเคลื่อน 4 ล้อ" },

    ],
    "015": [
      { car: "รถตักล้อยาง" },
      { car: "รถตักตีนตะขาบ" },
      { car: "รถตักหน้าขุดหลัง" },
      { car: "รถตักล้อยางอเนกประสงค์ แบบบังคับเลี้ยว 4 ล้อ" },

    ],
    "016": [
      { car: "รถเกลี่ยดินขนาดใหญ่ ขนาด 125 แรงม้าขึ้นไป" },
      { car: "รถเกลี่ยดินขนาดกลาง ขนาดต่ำกว่า 125 แรงม้า" },
    ],
    "018": [
      { car: "รถบดล้อยางแบบขับเคลื่อนด้วยตัวเอง" },
      { car: "ล้อยางบดถนนแบบลากพ่วง" },
    ],
    "019": [
      { car: "รถบดล้อเหล็กขับเคลื่อนด้วยตัวเอง ขนาด 4 ตันขึ้นไป" },
      { car: "รถบดล้อเหล็กขับเคลื่อนด้วยตัวเอง ขนาดต่ำกว่า 4 ตัน" },
      { car: "ลูกกลิ้งล้อเหล็กบดถนนแบบเดินตาม/แบบลากพ่วง" },
      { car: "รถบดล้อเหล็กแบบผสม (ล้อเหล็ก - ล้อยาง) ขนาด 4 ตันขึ้นไป" },
      { car: "รถบดล้อเหล็กแบบผสม (ล้อเหล็ก - ล้อยาง) ขนาดต่ำกว่า 4 ตัน" },
      { car: "รถบดล้อเหล็กขับเคลื่อนด้วยตัว ที่ รพช. ผลิต" },
    ],
    "020": [
      { car: "รถบดถนนแบบตีนแกะขับเคลื่อนด้วยตัวเอง" },
      { car: "ลูกกลิ้งตีนแกะบดถนนแบบลากพ่วง" },
    ],
    "021": [
      { car: "รถเครน" },
      { car: "รถเครนขนาดใหญ่บรรเทาสาธารณภัย" },
      { car: "รถเครนกู้ภัยสมรรถนะสูง ไม่น้อยกว่า 56,000 กิโลกรัม พร้อมอุปกรณ์" },
    ],
    "022": [
      { car: "เครื่องผสมคอนกรีต" },
    ],
    "023": [
      { car: "เครื่องบดอัดแบบแผ่นสั่นสะเทือน" },
    ],
    "024": [
      { car: "เครื่องเจาะสำรวจชั้นดิน" },
    ],
    "025": [
      { car: "เครื่องสูบน้ำ ขนาดท่อส่งไม่เกิน 5 นิ้ว" },
      { car: "เครื่องสูบน้ำ ขนาดท่อส่ง 6 นิ้ว – 10 นิ้ว" },
      { car: "เครื่องสูบน้ำ ขนาดท่อส่ง 12 นิ้วขึ้นไป" },
      { car: "เครื่องสูบโคลน" },
      { car: "เครื่องสูบน้ำแบบจุ่ม" },
      { car: "เครื่องสูบน้ำดับเพลิง แบบหาบหาม" },
      { car: "เครื่องสูบน้ำดับเพลิง แบบทุ่นลอย" },
      { car: "เครื่องสูบน้ำระยะไกล 3 กิโลเมตร" },
      { car: "เครื่องสูบน้ำระยะไกล 2 กิโลเมตร" },
      { car: "เครื่องสูบน้ำ อัตราสูบ 35,000 ลิตรต่อนาที" },
    ],
    "026": [
      { car: "รถยนต์กู้ภัยเคลื่อนที่เร็วพร้อมอุปกรณ์" },
      { car: "รถยนต์กู้ภัยเอนกประสงค์ขนาดกลาง" },
      { car: "รถกู้ภัยอเนกประสงค์ขนาดใหญ่" },
      { car: "รถเครื่องช่วยหายใจพร้อมอุปกรณ์" },
      { car: "รถปฏิบัติการกู้ภัยสารเคมีและวัตถุอันตราย" },
      { car: "รถบรรทุกเรือยนต์กู้ภัยพร้อมอุปกรณ์" },
      { car: "รถผลิตอากาศ พร้อมระบบส่งระยะไกล" },
      { car: "รถปฏิบัติการกู้ภัยช่วยชีวิตตึกถล่ม" },
    ],
    "027": [
      { car: "รถดับเพลิงอาคาร" },
      { car: "รถดับเพลิงโฟมและเคมีขนาดใหญ่" },
      { car: "รถดับเพลิงชนิดหอน้ำพร้อมบันไดและอุปกรณ์" },
      { car: "รถดับไฟป่าพร้อมอุปกรณ์" },
      { car: "รถบรรทุกน้ำช่วยดับเพลิง" },
      { car: "รถดับเพลิงพร้อมระบบโฟมอัดอากาศแรงดันสูง พร้อมชุดยานยนต์ดับเพลิงฉีดหมอกน้ำ/โฟม ด้วยระบบควบคุมระยะไกล" },
      { car: "ยานยนต์ดับเพลิง ฉีดหมอกน้ำ/โฟม" },
      { car: "รถยนต์ดับเพลิงชนิดหอน้ำ ขนาดความสูง 90 เมตร" },
      { car: "ยานยนต์ดับเพลิง พร้อมระบบควบคุมระยะไกล และอุปกรณ์" },
      { car: "รถดับเพลิง ขนาดอัตราสูบฉีดน้ำ/โฟม ไม่น้อยกว่า 10,000 ลิตรต่อนาที " },
      { car: "รถดับเพลิงพร้อมติดตั้งเครื่องสูบน้ำระยะไกล" },
    ],
    "028": [
      { car: "รถไฟฟ้าส่องสว่าง" },
      { car: "รถเครื่องกำเนิดไฟฟ้า ขนาด 200 kVA." },
      { car: "รถไฟฟ้า ที่ รพช. ผลิต" },
    ],
    "029": [
      { car: "เรือท้องแบน" },
      { car: "เรือท้องแบนไฟเบอร์กลาส" },
      { car: "เรือยาง" },
      { car: "เรือหางยาว" },
      { car: "ยานเบาะอากาศกู้ภัย" },
      { car: "เรือเร็วตรวจการณ์" },
      { car: "เรือยนต์กู้ภัย (Rescue Boat)" },
      { car: "ยานยนต์กู้ภัยเคลื่อนที่เร็วบนผิวน้ำ" },
      { car: "เรือขนาดเล็ก" },
      { car: "เรือท้องแบนอลูมิเนียมบรรทุกเครื่องผลิตน้ำดื่ม" },
      { car: "เรือแอร์โบ๊ท" },
    ],
    "030": [
      { car: "หางลากจูงบรรทุกเครื่องจักรกล" },
      { car: "รถตู้บริการน้ำมันหล่อลื่นแบบลากพ่วง" },
      { car: "ตู้เก็บพัสดุอะไหล่แบบลากพ่วง" },
      { car: "ถังน้ำแบบลากพ่วง" },
      { car: "ถังน้ำมันเชื้อเพลิงแบบลากพ่วง" },
      { car: "ตู้เก็บเครื่องมือซ่อมแบบลากพ่วง" },
      { car: "รถพ่วงอื่นๆ" },
      { car: "หางลากพ่วงบรรทุกเครื่องผลิตน้ำดื่ม" },
    ],
    "031": [
      { car: "เครื่องยนต์เรือ แบบหางสั้น" },
      { car: "เครื่องยนต์เรือ แบบหางยาว" },
    ],
    "032": [
      { car: "อากาศยานปีกหมุน (Helicopter)" },
      { car: "อากาศยานตรวจการณ์ไร้คนขับ (UAV/Drone)" },
    ],
    "033": [

      { car: "สะพานถอดประกอบได้ (Bailey Bridge) พร้อมชุดทอดสะพาน ความยาว 48 เมตร" },
      { car: "สะพานถอดประกอบได้ (Bailey Bridge) พร้อมทางขึ้น-ลง ความยาว 51 เมตร" },
    ],
    "034": [
      { car: "รถยกของในโรงงานแบบงาแซะ" },
      { car: "เครื่องยกของ แบบงาแซะ" },
    ],
    "035": [
      { car: "รถขุดตักไฮดรอลิค" },
      { car: "รถขุดตักไฮดรอลิคแบบสะเทินน้ำสะเทินบก" },
      { car: "รถขุดตักไฮดรอลิค แบบแขนตักยาว" },
      { car: "รถขุดล้อยางกู้ภัย ชนิดปรับระดับฐานล้อยกสูง พร้อมอุปกรณ์" },
      { car: "รถขุดล้อยางกู้ภัยขนาดใหญ่ ชนิดปรับระดับฐานล้อได้ พร้อมอุปกรณ์" },
      { car: "รถขุดตักไฮดรอลิค แบบหัวเจาะกระแทกคอนกรีต" },
      { car: "รถขุดตักไฮดรอลิค แบบหัวจับ-กระแทกแผ่นกันดินถล่ม (Sheet Pile)" },
    ],
    "036": [

      { car: "เครื่องเจาะบ่อบาดาลแบบหมุน ติดตั้งบนรถบรรทุก " },
    ],
    "040": [
      { car: "เครื่องเจาะบ่อน้ำตื้น ติดตั้งบนรถบรรทุก " },
    ],
    "041": [

      { car: "รถจักรยานยนต์" },
    ],
    "051": [
      { car: "ทั่ง" },
      { car: "เครื่องมือขึ้นรูปโลหะและตัวถัง" },
    ],
    "052": [
      { car: "เครื่องประจุไฟแบตเตอรี่แบบติดตั้งประจำที่" },
      { car: "เครื่องประจุไฟแบตเตอรี่แบบล้อเข็น" },
      { car: "เครื่องมือบริการและตรวจสอบแบตเตอรี่" },
    ],
    "053": [
      { car: "เครื่องล้างทำความสะอาดด้วยน้ำแรงดันสูง" },
      { car: "เครื่องล้างทำความสะอาดชิ้นส่วนด้วยน้ำมัน" },
      { car: "เครื่องทำความสะอาดชิ้นส่วนแบบพ่นทราย" },
    ],
    "054": [

      { car: "เครื่องยกไฮดรอลิคแบบตั้งพื้นมีล้อเลื่อน" },
      { car: "เครื่องยกแบบมีเสาและแขนยื่น" },
      { car: "รอกโซ่พร้อมขาตั้ง" },
      { car: "เครื่องยกในระดับสูงแบบคานเลื่อน" },
      { car: "เครื่องยกไฮดรอลิค แบบ X-LIFT" },
    ],
    "055": [
      { car: "สว่านเจาะแบบตั้งพื้น" },
      { car: "สว่านเจาะแบบตั้งโต๊ะ" },
      { car: "เครื่องเจาะแบบเรเดียล" },
      { car: "สว่านไฟฟ้าแบบมือถือ" },
      { car: "สว่านเจาะมือถือแบบใช้ลม" },
    ],
    "056": [

      { car: "เครื่องมือวัด-ปรับแต่ง ระบบเครื่องยนต์" },
      { car: "เครื่องมือวัดจังหวะการจุดระเบิดแบบใช้แสง" },
      { car: "เครื่องมือวัดอุณหภูมิเครื่องยนต์" },
      { car: "เครื่องมือวิเคราะห์ไอเสีย" },
      { car: "เครื่องมือวัดกำลังอัดเครื่องยนต์" },
    ],
    "057": [

      { car: "เครื่องตัดเหล็กแบบมือถือ" },
      { car: "เครื่องตัดเหล็กแบบตั้งพื้น" },
      { car: "เครื่องตัดเหล็กแบบใช้แผ่นตัด" },
      { car: "ชุดตัดแก๊สพร้อมอุปกรณ์ตัดโลหะ และอโลหะได้ 4 นิ้ว" },
    ],
    "058": [
      { car: "เครื่องกำเนิดไฟฟ้าแบบติดตั้งประจำที่" },
      { car: "เครื่องกำเนิดไฟฟ้าแบบติดตั้งบนรถลากพ่วง" },
      { car: "เครื่องกำเนิดไฟฟ้า แบบติดตั้งบนรถบรรทุก" },
      { car: "เครื่องกำเนิดไฟฟ้า แบบเคลื่อนที่" },
    ],
    "059": [
      { car: "หินเจียรนัยแบบตั้งพื้น" },
      { car: "หินเจียรนัยแบบตั้งโต๊ะ" },
      { car: "หินเจียรนัยแบบมือถือใช้ไฟฟ้าหรือใช้ลม" },
      { car: "เครื่องเจียรนัยลิ้นเครื่องยนต์" },
      { car: "เครื่องเจียรนัยบ่าลิ้นเครื่องยนต์" },
      { car: "เครื่องเจียรและย้ำหมุดผ้าเบรก" },
      { car: "เครื่องเจียรเพลาข้อเหวี่ยง" },
      { car: "เครื่องลับดอกสว่าน" },
      { car: "เครื่องเจียรนัยฝาสูบ" },
    ],
    "060": [
      { car: "เครื่องคว้านกระบอกสูบ/ตั้งศูนย์เสื้อสูบ" },
      { car: "เครื่องขัดกระบอกสูบ" },
      { car: "เครื่องขัดกระบอกสูบแบบมือถือ" },
      { car: "เครื่องมือปรับแต่งก้านสูบและลูกสูบ" },
      { car: "เครื่องมือคว้านรูแบบมือถือ" },
    ],
    "061": [
      { car: "ประแจกระแทกแบบใช้ลม" },
      { car: "ประแจกระแทกแบบใช้ไฮดรอลิค" },
      { car: "ประแจกระแทกแบบใช้ไฟฟ้า" },
      { car: "ไขควงตอก" },
    ],
    "062": [

      { car: "เครื่องตรวจสอบหัวฉีด" },
      { car: "เครื่องตรวจสอบปั๊มหัวฉีด" },
      { car: "เครื่องมือซ่อมปั๊มหัวฉีด" },
    ],
    "063": [

      { car: "แม่แรงไฮดรอลิค" },
      { car: "แม่แรงไฮดรอลิคแบบตะเฆ่" },
      { car: "แม่แรงลม" },
      { car: "แม่แรงแบบกลไก" },
      { car: "แม่แรงไฮดรอลิครองรับกระปุกเกียร์" },
    ],
    "064": [
      { car: "เครื่องกลึง" },
      { car: "เครื่องกลึงอัตโนมัติ" },
      { car: "เครื่องกลึงจานเบรก" },
    ],
    "065": [

      { car: "เครื่องวัดไฟฟ้าแบบเอนกประสงค์" },
      { car: "เครื่องวัดแรงดันไฟฟ้า" },
      { car: "เครื่องวัดกระแสไฟฟ้า" },
      { car: "เครื่องวัดความต้านทานไฟฟ้า" },
      { car: "เครื่องวัดตรวจสอบไฟหน้ารถยนต์" },
      { car: "เครื่องวัดกำลังส่งวิทยุ" },
      { car: "เครื่องโปรแกรมความถี่" },
    ],
    "066": [
      { car: "แท่นอัดไฮดรอลิคแบบตั้งพื้น" },
      { car: "เครื่องอัดหัวต่อสายไฮดรอลิค" },
      { car: "เครื่องอัดแบบกลไก" },
      { car: "เครื่องมือตัดสายไฮดรอลิค" },
    ],
    "067": [

      { car: "รอกโซ่" },
      { car: "รอกโซ่ไฟฟ้า" },
    ],
    "068": [
      { car: "ปากกาจับชิ้นงาน" },
      { car: "ปากกาจับท่อแบบตั้งพื้น" },
    ],
    "069": [
      { car: "เครื่องเชื่อมไฟฟ้าแบบใช้หม้อแปลง" },
      { car: "เครื่องเชื่อมไฟฟ้าแบบใช้เครื่องยนต์ขับ" },
      { car: "เครื่องเชื่อมแก๊สพร้อมอุปกรณ์" },
      { car: "เครื่องเชื่อมแบบย้ำจุด" },
      { car: "เครื่องเชื่อมกึ่งอัตโนมัติแบบใช้ลวด" },
      { car: "เครื่องเชื่อมกึ่งอัตโนมัติแบบใช้แก๊สคลุม" },
      { car: "เครื่องดูดควันงานเชื่อมโลหะ" },
      { car: "เครื่องเชื่อมแบบ TIG" },
      { car: "เครื่องเชื่อมแบบ MIG" },
      { car: "เครื่องเชื่อมแบบพ่นพอกโลหะ" },
      { car: "เครื่องเชื่อมลูกกลิ้ง" },
      { car: "เครื่องเชื่อมโซ่ตีนตะขาบ" },
      { car: "เครื่องบดเศษลวดเชื่อม" },
    ],
    "070": [
      { car: "เครื่องมือส่วนกลาง ประจำห้องเครื่องมือโรงซ่อม" },
      { car: "เครื่องมือเครื่องจักรกลชุดใหญ่" },
      { car: "เครื่องมือประจำช่างซ่อม" },
      { car: "เครื่องมือประจำช่างซ่อมไฟฟ้า" },
      { car: "เครื่องมือประจำชุดซ่อมทางลาดยาง" },
    ],
    "071": [
      { car: "ชุดเครื่องมือทำเกลียว นอก-ใน" },
      { car: "ชุดเครื่องมือทำเกลียวท่อ" },
    ],
    "072": [
      { car: "เลื่อยตัดเหล็กไฟฟ้า" },
      { car: "เลื่อยวงเดือน" },
      { car: "เครื่องตัดเหล็กแบบ AIR PLASMA" },
      { car: "เครื่องตัดเหล็กตามแบบชนิดใช้แก๊ซตัด" },
    ],
    "73": [
      { car: "เครื่องไส" },
    ],
    "074": [
      { car: "เครื่องมือชุดถอด - ประกอบบู๊ช" },
    ],
    "075": [
      { car: "ชุดบริการหล่อลื่นและบำรุงรักษาเครื่องจักรกล" },
    ],
    "076": [
      { car: "เครื่องอัดอากาศ แบบติดตั้งประจำที่" },
      { car: "เครื่องอัดอากาศ แบบติดตั้งบนรถบรรทุก " },
      { car: "เครื่องอัดอากาศ แบบติดตั้งบนรถลากพ่วง" },
    ],
    "077": [

      { car: "เครื่องทำความสะอาดหัวเทียน" },
    ],
    "078": [
      { car: "เครื่องมือชุดตัดและบานท่อ" },
      { car: "เครื่องมือตัดปะเก็น" },
      { car: "เครื่องแกะสลักด้วยไฟฟ้า(ปากกาไฟฟ้า)" },
    ],
    "079": [

      { car: "เครื่องมืออุปกรณ์พ่นสี" },
      { car: "ตู้ดูดละอองสี" },
      { car: "เครื่องขัดสี" },
      { car: "เครื่องพ่นไอร้อน" },
    ],
    "080": [
      { car: "เครื่องมือชุดบัดกรีแบบใช้ไฟเผา" },
      { car: "เครื่องมือชุดบัดกรีแบบใช้ไฟฟ้า" },
    ],
    "081": [
      { car: "เครื่องทดสอบสปริง" },
    ],
    "082": [

      { car: "วิทยุรับ-ส่งแบบ SSB" },
      { car: "วิทยุรับ-ส่ง แบบ VHF/FM แบบติดตั้งประจำสำนักงาน" },
      { car: "วิทยุรับ-ส่ง แบบ VHF/FM แบบติดตั้งประจำรถยนต์" },
      { car: "วิทยุรับ-ส่ง แบบ VHF/FM แบบมือถือ" },
      { car: "วิทยุรับ-ส่ง แบบ TRUNK แบบติดตั้งประจำรถยนต์" },
    ],
    "083": [
      { car: "เครื่องมือชุดดูดอุปกรณ์แบบใช้ไฮดรอลิค" },
      { car: "เครื่องมือชุดดูดอุปกรณ์แบบใช้มือขัน" },
    ],
    "084": [

      { car: "เครื่องมือตรวจสอบอาเมเจอร์" },
      { car: "เครื่องมือตรวจสอบมอเตอร์สตาร์ทและอัลเตอร์เนเตอร์" },
    ],
    "085": [
      { car: "เครื่องมือตรวจสอบและบริการแอร์รถยนต์" },
    ],
    "086": [

      { car: "เครื่องตรวจสอบระบบไฮดรอลิค" },
      { car: "เครื่องกรองน้ำมันไฮดรอลิค" },
    ],
    "087": [
      { car: "เครื่องมือวัดระยะด้านนอก-ใน" },
    ],
    "088": [
      { car: "แท่นติดตั้งซ่อมเครื่องยนต์" },
      { car: "แท่นติดตั้งซ่อมเกียร์" },
      { car: "แท่นติดตั้งซ่อมเฟืองท้าย" },
    ],
    "089": [

      { car: "เครื่องจี้เขย่าคอนกรีต" },
      { car: "เครื่องเจาะดินแบบใช้ไฮดรอลิค" },
      { car: "เครื่องตัดคอนกรีต" },
      { car: "เครื่องขุดเจาะกระแทก แบบมีเครื่องยนต์" },
      { car: "เครื่องขุดเจาะกระแทก แบบใช้ลม" },
      { car: "เครื่องตัดและทำลาย" },
    ],
    "090": [

      { car: "เครื่องมือชุดซ่อมบ่อบาดาล" },
    ],
    "091": [
      { car: "ถังน้ำมันเชื้อเพลิงแบบติดตั้งประจำที่" },
      { car: "ถังน้ำมันหล่อลื่นแบบติดตั้งประจำที่" },
    ],
    "092": [
      { car: "อุปกรณ์การเจาะ แบบ Down The Hole Hammer (DTH)" },
    ],
    "093": [

      { car: "เครื่องมือและอุปกรณ์ชุดซ่อมปะยาง" },
      { car: "เครื่องถ่วงล้อ( ยางรถยนต์ )" },
    ],
    "094": [
      { car: "เครื่องมือทดสอบแรงม้าเครื่องยนต์" },
      { car: "เครื่องมือทดสอบแรงแบบกลไก" },
      { car: "เครื่องทดสอบเบรก" },
    ],

    "095": [

      { car: "เตาอบชุบโลหะด้วยไฟฟ้า" },
    ],
    "096": [

      { car: "เครื่องกัดโลหะควบคุมการทำงานด้วยคอมพิวเตอร์ ( CNC )" },
      { car: "เครื่องกัด เซาะโลหะ" },
    ],
    "097": [

      { car: "เครื่องพับเหล็ก" },
    ],
    "098": [
      { car: "เครื่องดัดท่อ" },
    ],
    "121": [

      { car: "ชุดโคมไฟส่องสว่าง แบบยกหิ้ว" },
      { car: "ชุดโคมไฟส่องสว่าง แบบ Balloon Light" },
    ],
    "122": [

      { car: "เครื่องตัดหญ้าแบบล้อเข็น" },
      { car: "เครื่องตัดหญ้าแบบสะพาย" },
      { car: "เครื่องตัดหญ้าแบบนั่งขับ" },
    ],
    "123": [

      { car: "เครื่องกวาดพื้นดูดฝุ่นอัตโนมัติ แบบนั่งขับ" },
      { car: "เครื่องดูดฝุ่น" },
      { car: "เครื่องดูดควัน/เป่าลม" },
      { car: "เครื่องเป่าลม แบบสะพายหลัง" },
    ],
    "124": [

      { car: "เครื่องวัดความเร็วลม และทิศทางลม" },
      { car: "เครื่องตรวจแก๊สพร้อมอุปกรณ์" },
      { car: "เครื่องตรวจวัดคุณภาพอากาศ (สารอินทรีย์ระเหย)" },
      { car: "เครื่องวิเคราะห์สารเคมีทั้งของแข็งและของเหลว แบบพกพา" },
      { car: "เครื่องค้นหาผู้ประสบภัย แบบมองภาพผ่านกล้อง" },
      { car: "เครื่องค้นหาผู้ประสบภัย" },
    ],
    "125": [

      { car: "ชุดป้องกันความร้อน / กู้ภัย" },
      { car: "ชุดป้องกันสารเคมี ระดับสูง (level. A)" },
      { car: "ชุดป้องกันสารเคมี ระดับกลาง (level. B)" },
      { car: "ชุดป้องกันสารเคมี ระดับตำ่ (level C)" },
      { car: "ชุดดับเพลิงอากาศยาน (Proximity Suit)" },
      { car: "ชุดดำน้ำ พร้อมอุปกรณ์" },
    ],
    "126": [

      { car: "เลื่อยโซ่ยนต์" },
      { car: "เลื่อยยนต์ตัดโลหะ และคอนกรีต" },
      { car: "เลื่อยยนต์ (Power Saw)" },
      { car: "ชุดเครื่องเจาะตัดทำลาย กระแทกโครงสร้างผนัง" },
      { car: "ชุดเครื่องเจาะตัดทำลาย แบบใช้แรงลม" },
    ],
    "127": [

      { car: "เครื่องตัดถ่างไฮดรอลิก แบบใช้เครื่องยนต์" },
      { car: "เครื่องตัดถ่างไฮดรอลิค แบบใช้มือโยก" },
      { car: "เครื่องตัดถ่างไฮดรอลิก แบบใช้ไฟฟ้า" },
      { car: "ชุดอุปกรณ์ค้ำยัน" },
      { car: "เครื่องค้ำยัน" },
    ],
    "128": [

      { car: "เครื่องช่วยหายใจ" },
    ],
    "129": [
      { car: "เครื่องอัดอากาศบริสุทธิ์ แบบประจำที่" },
      { car: "เครื่องอัดอากาศบริสุทธิ์ แบบเคลื่อนที่" },
    ],
    "130": [

      { car: "เครื่องส่งเสียงระยะไกล" },
    ],
    "131": [
      { car: "เครื่องยิงน้ำดับเพลิงแรงดันสูง" },
    ]


  }

}
