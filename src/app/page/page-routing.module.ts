import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageComponent } from "./page.component"
import { StoryMapComponent } from "./story-map/story-map.component"
import { CompareComponent } from "./compare/compare.component"
const routesPage: Routes = [


  {
    path: '',
    component: PageComponent,

    children: [
      { path: '', redirectTo: 'story-map', pathMatch: 'full' },
      {
        path: 'story-map',
        component: StoryMapComponent
      },
      {
        path: 'compare',
        component: CompareComponent
      }]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routesPage)],
  exports: [RouterModule],
})
export class PageRoutingModule { }