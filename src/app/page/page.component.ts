import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PageService } from './page.service';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent {
  page = 0

  titleList = [
    { name: "ระบบการจัดการข้อมูลสาธารณภัยเชิงพื้นที่", nameEN: "Incident Management Tactical and Story Map" },
    { name: "ระบบข้อมูลประกาศแจ้งเตือน", nameEN: "NDWC Routine Process" },
  ]


  constructor(
    private pageService: PageService,

  ) {
    this.pageService.page.subscribe(page => {
      this.page = page
    })
  }

}
