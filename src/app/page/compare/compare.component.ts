import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { MatPaginator, MatPaginatorModule, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ServiceCenter } from 'src/app/@common/service-center/service-center';
import { DateComparePipe } from 'src/app/@pipe/date-compare.pipe';
import { PageService } from '../page.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EarthquakeResModel } from 'src/app/@shares/model/app-model';
import { EarthquakePageEnum } from 'src/app/@shares/enum/app-enum';


//map
import Map from "@arcgis/core/Map.js";
import MapView from '@arcgis/core/views/MapView.js';
import Print from "@arcgis/core/widgets/Print.js";
import PictureMarkerSymbol from "@arcgis/core/symbols/PictureMarkerSymbol.js";
import Graphic from "@arcgis/core/Graphic.js";
import Point from "@arcgis/core/geometry/Point.js";
@Component({
  selector: 'app-compare',
  templateUrl: './compare.component.html',
  styleUrls: ['./compare.component.scss']
})
export class CompareComponent implements OnInit, OnDestroy {

  page = 0


  pageLength = 0;
  pageSize = 10;
  pageIndex = 0;
  pageSizeOptions = [10, 20, 30, 50];
  hidePageSize = false;
  showPageSizeOptions = true;
  showFirstLastButtons = true;
  disabled = false;
  pageEvent: any;
  filter: any
  displayedColumns: string[] = ['select', 'Earthquake_date', 'Earthquake_date_thai', 'Earthquake_mag', 'Earthquake_lat', 'Earthquake_long', 'Earthquake_depth', 'Earthquake_region', 'Earthquake_source', 'status_land', 'status_inth', 'map', 'action'];
  earthquakePageEnum = EarthquakePageEnum
  statusPrint: any = {
    isPrint: false,
    earthquake: null
  }
  timeInterval: any
  compareSelectList: any[] = []

  earthquakeList: EarthquakeResModel[] = []

  compareDialogModal: any;
  @ViewChild("compareDialog") compareDialog: any
  constructor(
    private serviceCenter: ServiceCenter,
    private ngbModal: NgbModal,
    private pageService: PageService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.pageService.page.next(1)
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((param: any) => {
      if (param.c == undefined) {
        this.router.navigate(
          ['/pages/compare'],
          { queryParams: { c: EarthquakePageEnum.earthquake } }
        );
      } else {
        this.page = param.c
      }

    })

    this.timeInterval = setInterval(() => {
      this.getEarthquake()
    }, 60000);


    //test
    const webmapId = "523e292002294392a36d888a9780a731";
    const printUrl = "https://sampleserver6.arcgisonline.com/arcgis/rest/services/Utilities/PrintingTools/GPServer/Export%20Web%20Map%20Task";



    const map = new Map({
      basemap: "topo-vector",
      ground: "world-elevation",
    });

    this.view = new MapView({
      map: map,
      center: [100, 14.027],
      zoom: 7,
      container: "printMap"
    });

    this.print = new Print({
      view: this.view,
      printServiceUrl:
        "https://sampleserver6.arcgisonline.com/arcgis/rest/services/Utilities/PrintingTools/GPServer/Export%20Web%20Map%20Task"
    })

    this.view.ui.add(this.print, {
      position: "top-left"
    });


    this.print.on("complete", (results: any) => {
      this.statusPrint.isPrint = false
      window.open(results.link.url);
    });


  }

  view: any;
  print: any;






  ngAfterViewInit() {
  }

  onSelectCompare() {
    this.compareSelectList = this.earthquakeList.filter((item: any) => item.select == true)
  }


  onCompare() {
    this.compareDialogModal = this.ngbModal.open(this.compareDialog, { centered: true, windowClass: 'custom-modal-size' })
    const box: any = document.querySelectorAll('.custom-modal-size')
    box[0].style.maxWidth = this.compareSelectList.length * 180 + 153 + "px"
    box[0].style.left = (window.innerWidth / 2) - ((this.compareSelectList.length * 180 + 153) / 2) + 'px'
  }

  onCloseCompare() {
    this.compareDialogModal.close()
  }

  handlePageEvent(event: any) {
    this.pageEvent = event;
    this.pageLength = event.length;
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    this.getEarthquake()
  }

  onExportCSV() {
    this.downloadFile(this.earthquakeList, 'data')
  }



  downloadFile(data: any, filename = 'data') {
    let csvData = this.ConvertToCSV(data,
      ['Earthquake_date', 'Earthquake_date_thai', 'Earthquake_mag', 'Earthquake_lat', 'Earthquake_long', 'Earthquake_depth', 'Earthquake_region', 'Earthquake_source', 'status_land', 'status_inth',]
    );

    //
    console.log(csvData)
    let blob = new Blob(['\ufeff' + csvData],
      { type: 'text/csv;charset=utf-8;' });
    let dwldLink = document.createElement("a");
    let url = URL.createObjectURL(blob);
    let isSafariBrowser = navigator.userAgent.indexOf('Safari') !=
      -1 && navigator.userAgent.indexOf('Chrome') == -1;
    if (isSafariBrowser) {

      dwldLink.setAttribute("target", "_blank");
    }
    dwldLink.setAttribute("href", url);
    dwldLink.setAttribute("download", filename + ".csv");
    dwldLink.style.visibility = "hidden";
    document.body.appendChild(dwldLink);
    dwldLink.click();
    document.body.removeChild(dwldLink);
  }

  ConvertToCSV(objArray: any, headerList: any) {
    let array = typeof objArray !=
      'object' ? JSON.parse(objArray) : objArray;
    let str = '';
    let row = 'No,';
    console.log();


    for (let index in headerList) {
      row += headerList[index] + ',';
    }
    row = row.slice(0, -1);
    str += row + '\r\n';
    for (let i = 0; i < array.length; i++) {
      let line = (i + 1) + '';
      for (let index in headerList) {
        let head = headerList[index];

        if (head == 'Earthquake_date_thai') {
          line += ',' + DateComparePipe.prototype.transform(array[i]['Earthquake_date'], "TH");
        } else if (head == 'Earthquake_date') {
          line += ',' + DateComparePipe.prototype.transform(array[i][head], "EN");
        }
        else {
          console.log(head);
          line += ',' + (array[i][head] != null ? array[i][head].toString().replaceAll(',', '') : ' ');
        }

      }
      str += line + '\r\n';
    }
    return str;
  }

  onFilter(filter: any) {
    console.log(filter);
    this.filter = filter
    this.getEarthquake()
  }

  onPrint(earthquake: any) {
    this.statusPrint.isPrint = true
    this.statusPrint.earthquake = earthquake
    var point = new Point({
      x: earthquake.Earthquake_long,
      y: earthquake.Earthquake_lat
    });
    this.removeGraphic("pointLocation")
    var symbol = new PictureMarkerSymbol({
      url: "../assets/image/location-red.svg",
      width: "20px",
      height: "20px"
    });

    let pointAtt = {
      Name: "pointLocation",
      Owner: "TransCanada",
    };
    var graphic = new Graphic({
      geometry: point,
      symbol: symbol,
      attributes: pointAtt
    });
    this.view.graphics.add(graphic);
    this.view.goTo({
      center: [earthquake.Earthquake_long, earthquake.Earthquake_lat],
      zoom: 7
    });

    setTimeout(() => {
      const e: any = document.getElementsByClassName('esri-print__export-button')
      e[0].click()
    }, 1000);

  }

  getEarthquake() {

    console.log(this.filter);
    let startDate = ''
    let endDate = new Date().toISOString()
    if (this.filter.dataDisplay == '1') {
      var datetime = new Date();
      datetime.setDate(datetime.getDate() - 3)
      datetime.setHours(0)
      datetime.setMinutes(0)
      datetime.setSeconds(0)
      startDate = datetime.toISOString()
      console.log(startDate);
      console.log(endDate);
    } else {

    }


    const param = 'page=' + this.pageIndex + '&' +
      'limit=' + this.pageSize +
      '&startDate=' + startDate +
      '&endDate=' + endDate

    this.serviceCenter.getEarthquake(param).then((res: any) => {
      console.log(res);
      this.pageLength = res.pagination.totalPage * res.pagination.limit
      this.earthquakeList = res.data
      console.log(this.earthquakeList);

    }).catch(err => {
      console.log(err);

    })
  }


  onCreateNew() {

  }



  removeGraphic(name: any) {
    if (this.view.graphics.items.length) {
      this.view.graphics.items.forEach((element: any) => {
        if (element.attributes.Name == name) {
          this.view.graphics.remove(element);
        }
      });
    }
  }


  ngOnDestroy(): void {
    clearInterval(this.timeInterval)
  }

}
