import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateCompare'
})
export class DateComparePipe implements PipeTransform {

  transform(value: any, ...args: any): unknown {
    let dateTime = new Date(value)


    if(args[0] == 'EN'){
      dateTime = new Date(dateTime.getTime() +(dateTime.getTimezoneOffset() / 60 *3600000) )
    }


    const date = dateTime.getDate()
    const month = dateTime.getMonth()
    const monthList = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"]
    const year = dateTime.getFullYear() + 543
    const min = dateTime.getMinutes()
    const hr = dateTime.getHours()
    return (date < 10 ? "0" + date : date) + " " + monthList[month] + " " + year + ' ' + (hr < 10 ? "0" + hr : hr) + ":" + (min < 10 ? "0" + min : min);
  }

}
