import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateEvent'
})
export class DateEventPipe implements PipeTransform {

  transform(value: any, ...args: unknown[]): unknown {
    const dateTime = new Date(value)

    const date = dateTime.getDate()
    const month = dateTime.getMonth() + 1
    const year = dateTime.getFullYear() + 543

    return (date < 10 ? "0" + date : date) + "/" + (month < 10 ? "0" + month : month) + "/" + year;
  }

}
