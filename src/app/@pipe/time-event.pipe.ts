import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timeEvent'
})
export class TimeEventPipe implements PipeTransform {

  transform(value: any, ...args: unknown[]): unknown {

    const dateTime = new Date(value)
    const min = dateTime.getMinutes()
    const hr = dateTime.getHours()
    return (hr < 10 ? "0" + hr : hr) + ":" + (min < 10 ? "0" + min : min);
  }

}
