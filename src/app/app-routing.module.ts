import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  // { path: '**', redirectTo: "page" },
  { path: '', redirectTo: "pages", pathMatch: 'full' },
  {
    path: 'pages',
    loadChildren: () => import('./page/page.module').then(m => m.PageModule)
    
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
